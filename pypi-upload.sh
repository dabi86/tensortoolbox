#!/bin/bash

rm -rf dist TensorToolbox.egg TensorToolbox.egg-info
python setup.py sdist bdist_wheel
twine upload dist/*
