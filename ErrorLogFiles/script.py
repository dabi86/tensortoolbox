import pickle as pkl

import numpy as np
import numpy.linalg as npla

import tensor as TT

import matplotlib.pyplot as plt

ff = open('TensorToolbox-working.log')
dd = pkl.load(ff)
ff.close()

A = dd['A']
Is = dd['Is']
Js = dd['Js']

# Plot solution
plt.figure()
plt.imshow(A)
plt.show(block=False)

# Plot evaluation points
idxs = []
dims = A.shape
for k in range(len(Is)-1,-1,-1):
    for i in range(len(Is[k])):
        for j in range(len(Js[k])):
            for kk in range(dims[k]):
                idxs.append( Is[k][i] + (kk,) + Js[k][j] )

last_idxs = np.array(idxs)

plt.figure()
plt.plot(last_idxs[:,0],last_idxs[:,1],'o')
plt.title('Original')
plt.show(block=False)

# Try to approximate with ttcross (Known rank)
rs = [1]
for i in range(len(Js)-1):
    rs.append(len(Js[i]))
rs.append(1)

print "Approximation with last rank and last Js"
TTapprox = TT.TTvec(A,method='ttcross',lr_r=rs,lr_Jinit=Js)

plt.figure()
plt.imshow(TTapprox.to_tensor())
plt.colorbar()
plt.title('TTapprox')
plt.show(block=False)

plt.figure()
plt.imshow( np.abs(A-TTapprox.to_tensor()))
plt.colorbar()
plt.title('Error')
plt.show(block=False)

# Try to approximate from scratch
Jinit = dd['Jinit']
print "Approximation from scratch with same Jinit"
TTapp2 = TT.TTvec(A,method='ttcross',lr_delta=1e-5,lr_r=[1,len(Jinit[0]),1], lr_Jinit=Jinit, mv_eps=1e-10)

print "Approximation with prescribed increasing rank"
TTapp3 = TT.TTvec(A,method='ttcross',lr_r=[1,5,1],lr_delta=1e-5, mv_eps=1e-5)

totit = len(TTapp3.rtol_fiber_lists)
N = min(totit,4)
last_Js = TTapp3.rtol_fiber_lists[(totit-N):]
last_Is = TTapp3.ltor_fiber_lists[(totit-N):]

plt.figure(figsize=(17,12))
XX,YY = np.meshgrid(range(A.shape[0]),range(A.shape[1]))
for i in range(N):
    Js = last_Js[i]
    Is = last_Is[i]
    
    plt.subplot(np.ceil(np.sqrt(2*N)),np.ceil(np.sqrt(2*N)),2*i+1)
    plt.imshow(A)
    for (ix,iy) in Js:
        plt.plot(XX[ix,iy],YY[ix,iy], 'bo')

    plt.subplot(np.ceil(np.sqrt(2*N)),np.ceil(np.sqrt(2*N)),2*i+2)
    plt.imshow(A)
    for (ix,iy) in Is:
        plt.plot(XX[ix,iy],YY[ix,iy], 'ro')

plt.suptitle("Last crosses")
plt.show(block=False)


plt.figure()
plt.imshow( np.abs(A-TTapp3.to_tensor()))
plt.colorbar()
plt.title('Error from scratch prsecribed rank')
plt.show(block=False)
