import sys

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from prettytable import PrettyTable

import scipy.stats as stats

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SparseGrids as SG
import TensorToolbox as DT

import matplotlib.pyplot as plt

unifDist = stats.uniform(-1,2)

##########################################################
# TEST 1
#  Construct quadrature rule of high order throwing points
#  randomly and select them using maxvol 
#  (Convergence toward Gauss-Lobatto)\
#
# TEST 2
#  Start with Gauss Lobatto and then partition.
#  Throw points uniformly when space is partitioned
#
# TEST 3
#  Try to throw l points on a n-points Gauss rule
#  

TESTS = [4]


if 1 in TESTS:
    ##########################################################
    # TEST 1

    xspan = [-1.,1.]
    d = 6
    N = 10000
    X = np.linspace(-1.,1.,N)       
    #X = unifDist.rvs(N)

    P = S1D.Poly1D(S1D.JACOBI,[0.,0.])

    # Compute moments of orthonormal polys
    (x,w) = P.GaussQuadrature(d,normed=True)
    v = P.GradVandermonde1D(x,d,0,norm=True)
    b = np.dot(v.T,w)

    (xG,wG) = P.GaussQuadrature(d)
    (xL,wL) = P.GaussLobattoQuadrature(d,normed=True)
    VL = P.GradVandermonde1D(xL,d,0,norm=True)

    niter = 5
    for i in range(niter):
        V = P.GradVandermonde1D(X,d,0,norm=True)

        # Perturb idx in V to keep it in the maxvol
        # idxs = [100,200,250]
        #idxs = [100,105,110,120,550,16,800,900,1300,1500,1600,1800,2000,2200,2400,2500]
        idxs = []
        Vp = V.copy()
        for idx in idxs:
            Vp[idx,:] *= 100.

        (I,VsqInv,it) = DT.maxvol(Vp,delta=1e-10,maxit=1000)
        I = np.sort(I)
        xMV = X[I]

        if i < niter-1:
            h = np.min(np.diff(X))
            Xnew = []
            for ii in I:
                if X[ii] == -1.:
                    Xnew.append(X[ii]+np.linspace(0.,h,10))
                elif X[ii] == 1.:
                    Xnew.append(X[ii]-np.linspace(0.,h,10))
                else:
                    Xnew.append(X[ii]-np.linspace(h/9.,h,10))
                    Xnew.append(X[ii]+np.linspace(0.,h,10))

            X = np.asarray(Xnew)
            X = X.flatten()

    fixX = list(set(idxs).intersection(set(I)))
    plt.figure()
    plt.plot(xMV,np.zeros(len(I)), 'bo', label='Maxvol')
    plt.plot(X[fixX],np.zeros(len(fixX)),'go')
    plt.plot(xG, np.ones(len(I)), 'ro', label='Gauss')
    plt.plot(xL, 2.*np.ones(len(I)), 'ko', label='Gauss-Lobatto')
    plt.xlim([-1.1,1.1])
    plt.ylim([-0.5,2.5])
    plt.show(block=False)

    print npla.norm(X[np.sort(I)]-xL,2)

    wMV = npla.solve(V[I,:].T,b)
    # (wMV, res, rank, s) = npla.lstsq(V[I,:].T,b)

    # Test orthogonality of submatrix
    I_tmp = np.dot(V[I,:].T,np.dot(np.diag(wMV),V[I,:]))
    IL_tmp = np.dot(VL.T,np.dot(np.diag(wL),VL))
    plt.figure(figsize=(15,7))
    plt.subplot(1,2,1)
    plt.imshow( np.log10(np.abs(IL_tmp)), interpolation='none')
    plt.colorbar()
    plt.subplot(1,2,2)
    plt.imshow( np.log10(np.abs(I_tmp)), interpolation='none')
    plt.colorbar()
    plt.show(block=False)

    # Test cubatures
    for i in range(2*d-1):
        def f(x): return (x/2.)**i
        def intf(x): return (x/2.)**(i+1)/(i+1.)
        If = intf(xspan[1])-intf(xspan[0])

        # Gauss-Lobatto
        fL = f(xL)
        IL = np.dot(fL,wL)

        # Maxvol
        fMV = f(xMV)
        IMV = np.dot(fMV,wMV)

        print "Order %d: GL-err=%e, MV-err=%e" % (i, np.abs(If-IL), np.abs(If-IMV))


if 2 in TESTS:
    ########################################################################
    # TEST 2

    Npart = 2
    xspan = [-1.,1.]
    d = 5
    N = 10000
    X = np.linspace(-1.,1.,N)

    P = S1D.Poly1D(S1D.JACOBI,[0.,0.])

    # Compute moments of orthonormal polys
    dd = d # Degree of interpolatory rule
    (x,w) = P.GaussQuadrature(dd,normed=True)
    v = P.GradVandermonde1D(x,dd,0,norm=True)
    b = np.dot(v.T,w)

    (xL,wL) = P.GaussLobattoQuadrature(d,normed=True)
    VL = P.GradVandermonde1D(xL,d,0,norm=True)

    XW = [(xL,wL)]
    XWpart = [XW]
    Xpart_old = [[np.empty(0)]]

    for i in range(Npart):
        sys.stdout.write("Refinement iteration %d\r" % i)
        sys.stdout.flush()
        XWs = XWpart[i]

        Xs_old = []
        XWs_new = []
        for ii, (x,w) in enumerate(XWs):
            sys.stdout.write("Refinement iteration %d  El  %d\r" % (i,ii))
            sys.stdout.flush()

            # Split domain in half and 
            xLeft = x[np.where(x <= np.spacing(1))[0]]
            xRight = x[np.where(x >= -np.spacing(1))[0]]

            # Rescale points to [-1,1]
            xLeft = xLeft * 2. + 1.
            xRight = xRight * 2. - 1.

            # Drop points
            xLeft = npr.choice(xLeft,len(xLeft)-3)
            xRight = npr.choice(xRight,len(xRight)-3)

            Xs_old.append(xLeft)
            Xs_old.append(xRight)

            # Throw N random new points in xLeft and xRight
            XLeft = np.hstack((xLeft, unifDist.rvs(N)))
            XRight = np.hstack((xRight, unifDist.rvs(N)))

            # Construct Vandermonde matrices
            VLeft = P.GradVandermonde1D(XLeft,dd,0,norm=True)
            VRight = P.GradVandermonde1D(XRight,dd,0,norm=True)
            VLeft_cp = VLeft.copy()
            VRight_cp = VRight.copy()

            # Bias the matrices toward already computed idxs
            VLeft_cp[:len(xLeft),:] *= 10.
            VRight_cp[:len(xRight),:] *= 10.

            # Run Maxvol and find new idxs
            (ILeft,VsqInv,it) = DT.maxvol(VLeft_cp,delta=1e-10,maxit=1000)
            (IRight,VsqInv,it) = DT.maxvol(VRight_cp,delta=1e-10,maxit=1000)

            # Compute sorting indices
            idxLeft  = np.argsort(XLeft[ILeft])
            idxRight = np.argsort(XRight[IRight])

            # Reorder weights
            XLeft = XLeft[ILeft][idxLeft]
            XRight = XRight[IRight][idxRight]

            # Construct interpolation quadrature weights
            WLeft  = npla.solve(VLeft[ILeft,:].T,b)
            WRight = npla.solve(VRight[IRight,:].T,b)

            # Reorder weights
            WLeft = WLeft[idxLeft]
            WRight = WRight[idxRight]

            # Add new quadratures to the list
            XWs_new.append((XLeft,WLeft))
            XWs_new.append((XRight,WRight))

        # Add new list to the list of quadratures
        XWpart.append(XWs_new)
        Xpart_old.append(Xs_old)

    print "Quadrature Refinement tests"
    for i,XWs in enumerate(XWpart):
        print "Quadrature Iteration %d: #els=%i" % (i,len(XWs))

        titles = ["el %d" % i for i in range(len(XWs))]
        titles.insert(0,"Ord.")
        table = PrettyTable(titles)

        # Test cubatures
        for i in range(2*d):

            row_entries = [i]

            def f(x): return (x/2.)**i
            def intf(x): return (x/2.)**(i+1)/(i+1.)
            If = intf(xspan[1])-intf(xspan[0])

            for (x,w) in XWs:
                # Maxvol
                fMV = f(x)
                IMV = np.dot(fMV,w)

                row_entries.append( np.abs(If-IMV) )

            table.add_row( row_entries )

        print table

    # Plot rules
    for i,(XWs,Xs_old) in enumerate(zip(XWpart,Xpart_old)):
        plt.figure()

        for ii,((x,w),xold) in enumerate(zip(XWs,Xs_old)):
            plt.plot(x,ii*np.ones(len(x)),'o')
            plt.plot(xold,ii*np.ones(len(xold)),'s')

        plt.xlim([-1.1,1.1])
        plt.ylim([-0.5, len(XWs)-0.5])
        plt.show(block=False)


if 3 in TESTS:
    ####################################################
    # TEST 3

    xspan = [-1.,1.]
    d = 5
    N = 10000
    X = np.linspace(-1.,1.,N)
    l = 6

    P = S1D.Poly1D(S1D.JACOBI,[0.,0.])

    # Compute moments of orthonormal polys
    npoint = int(np.ceil((2*d+l)/2))
    (x,w) = P.GaussQuadrature(npoint,normed=True)
    v = P.GradVandermonde1D(x,npoint,0,norm=True)
    b = np.dot(v.T,w)

    (xG,wG) = P.GaussQuadrature(d,normed=True)
    VG = P.GradVandermonde1D(xG,d,0,norm=True)

    # Throw additional points
    X  = np.hstack((xG,unifDist.rvs(N)))

    # Construct Vandermonde matrix
    V_cp = P.GradVandermonde1D(X,d+l,0,norm=True)

    # Bias Gauss Points
    V_cp[:len(xG),:] *= 10.

    # Run Maxvol
    (I, VsqInv, it) = DT.maxvol(V_cp, delta=1e-10,maxit=1000)

    # Compute sorting indices
    idxs = np.argsort(X[I])

    # Reorder points
    X = X[I][idxs]

    # Compute Vandermonde matrix
    V = P.GradVandermonde1D(X, npoint,0,norm=True)

    # Construct weights
    (W,res,rank,s) = npla.lstsq(V.T,b)
    # (W,res) = opt.nnls(V.T,b)

    # Test orthogonality of new quadrature rule
    I_tmp = np.dot(V.T,np.dot(np.diag(W),V))
    plt.figure()
    plt.imshow( np.log10(np.abs(I_tmp)), interpolation='none')
    plt.colorbar()
    plt.show(block=False)

if 4 in TESTS:
    ####################################################
    # TEST 4

    xspan = [-1.,1.]
    l = 1
    N = 10000
    X = np.linspace(-1.,1.,N)
    tot_it = 5

    P = S1D.Poly1D(S1D.JACOBI,[0.,0.])

    (xG,wG) = P.GaussQuadrature(l,normed=True)

    x_list = [ xG ]
    for i in range(tot_it):
        # Compute moments of orthonormal polys
        npoint = (l+1)**(i+2) - 1
        (x,w) = P.GaussQuadrature(npoint,normed=True)
        v = P.GradVandermonde1D(x,npoint,0,norm=True)
        b = np.dot(v.T,w)

        # Throw additional points
        X  = np.hstack((x_list[-1],np.linspace(xspan[0],xspan[1],N)))

        # Construct Vandermonde matrix
        V_cp = P.GradVandermonde1D(X,npoint,0,norm=True)

        # Bias Gauss Points
        V_cp[:len(x_list[-1]),:] *= 10.

        # Run Maxvol
        (I, VsqInv, it) = DT.maxvol(V_cp, delta=1e-10,maxit=1000)

        # Compute sorting indices
        idxs = np.argsort(X[I])

        # Reorder points
        X = X[I][idxs]

        # Compute Vandermonde matrix
        V = P.GradVandermonde1D(X, npoint,0,norm=True)

        # Construct weights
        (W,res,rank,s) = npla.lstsq(V.T,b)
        # (W,res) = opt.nnls(V.T,b)

        # Test orthogonality of new quadrature rule
        I_tmp = np.dot(V.T,np.dot(np.diag(W),V))
        plt.figure()
        plt.imshow( np.log10(np.abs(I_tmp)), interpolation='none')
        plt.colorbar()
        plt.title("Iter %d" % it)
        plt.show(block=False)

        x_list.append( X[:] )

    # Plot Points
    plt.figure()
    for i in range(tot_it-1):
        plt.plot(x_list[i+1], i * np.ones(x_list[i+1].shape), 'go')
        plt.plot(x_list[i], i * np.ones(x_list[i].shape), 'ko')
    plt.xlim([-1.1,1.1])
    plt.ylim([-1,tot_it-1])
    plt.show(False)
