import sys

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from prettytable import PrettyTable

import scipy.stats as stats

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SparseGrids as SG
import TensorToolbox as DT

import matplotlib.pyplot as plt

unifDist = stats.uniform(-1,2)

## Non-negative Least square approach for equi-distant points
import scipy.optimize as opt

d = 19
N = 100

X = np.linspace(-1.,1.,N)
P  = S1D.Poly1D(S1D.JACOBI,[0.,0.])

# Compute normalized orthogonal polynomials
V = P.GradVandermonde1D(X,d,0,norm=True)

# Compute moments of orthonormal polys
(x,w) = P.GaussQuadrature(d,normed=True)
v = P.GradVandermonde1D(x,d,0,norm=True)
b = np.dot(v.T,w)

# Compute weights for equidistant grid
(W,res) = opt.nnls(V.T,b)
if res > 1e-10:
    print "Warning: Residual=%e" % resx

plt.figure()
plt.plot(X,W,'o')
plt.title('NNLS')
plt.show(block=False)

X = X[np.where(W>0.)]
W = W[np.where(W>0.)]
V = P.GradVandermonde1D(X,d,0,norm=True)
I_tmp = np.dot(V.T,np.dot(np.diag(W),V))
plt.figure()
plt.imshow( np.log10(np.abs(I_tmp)), interpolation='none' )
plt.colorbar()
plt.show(block=False)

