import sys

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from prettytable import PrettyTable

import scipy.stats as stats

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SparseGrids as SG
import TensorToolbox as DT

import matplotlib.pyplot as plt

unifDist = stats.uniform(-1,2)

maxlev = 10

sgKPUx = []
sgCCx = []
sgFEJx = []

for l in range(1,maxlev):
    sg = SG.SparseGrid(SG.KPU,dim=1,k=l,sym=1)
    (x,w) = sg.sparseGrid()
    sgKPUx.append(x)

    sg = SG.SparseGrid(SG.CC,dim=1,k=l,sym=1)
    (x,w) = sg.sparseGrid()
    sgCCx.append(x)

    sg = SG.SparseGrid(SG.FEJ,dim=1,k=l,sym=1)
    (x,w) = sg.sparseGrid()
    sgFEJx.append(x)


plt.figure(figsize=(15,7))
for l in range(1,maxlev):
    plt.subplot(1,3,1)
    plt.plot(sgKPUx[l-1],l*np.ones(len(sgKPUx[l-1])),'o')
    plt.subplot(1,3,2)
    plt.plot(sgCCx[l-1],l*np.ones(len(sgCCx[l-1])),'o')
    plt.subplot(1,3,3)
    plt.plot(sgFEJx[l-1],l*np.ones(len(sgFEJx[l-1])),'o')

plt.show(block=False)
