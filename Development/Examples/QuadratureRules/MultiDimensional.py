import sys

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from prettytable import PrettyTable

import scipy.stats as stats

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SpectralND as SND
from SpectralToolbox import SparseGrids as SG
from UQToolbox import UncertaintyQuantification as UQ
import TensorToolbox as DT

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

#####################################################
# TEST 1
#  Construct 2D quadrature rule

TESTS = [1]

if 1 in TESTS:
    xspan = [-1.,1.]
    d = 3
    N = 10
    delta = 1e-10

    dists = [stats.uniform(-1.,2.) for i in range(d)]
    polys = [S1D.Poly1D(S1D.JACOBI,[0.,0.]) for i in range(d)]
    distPolys = [stats.uniform(-1.,2.) for i in range(d)]
    quadTypes = [S1D.GAUSSLOBATTO for i in range(d)]
    
    gPC_dic = UQ.gPC_MultiIndex(dists,polys,distPolys,N,NI = N,quadTypes=quadTypes)

    x = gPC_dic['x']
    V = gPC_dic['V']

    (I, VsqInv,it) = DT.maxvol(V,delta,maxit=1000)
    xMV = x[I,:]

    if d == 2:
        plt.figure()
        plt.plot(x[:,0],x[:,1],'bo',label='Tensor')
        plt.plot(xMV[:,0],xMV[:,1],'ro',label='MaxVol')
        plt.legend()
        plt.show(block=False)
    elif d == 3:
        fig = plt.figure(figsize=(12,7))
        ax = fig.add_subplot(121, projection='3d')
        ax.scatter(x[:,0],x[:,1],x[:,2],c='b')
        ax = fig.add_subplot(122, projection='3d')
        ax.scatter(xMV[:,0],xMV[:,1],xMV[:,2],c='r')
        plt.show(block=False)
