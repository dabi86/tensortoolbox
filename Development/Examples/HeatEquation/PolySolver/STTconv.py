import logging
import scipy.stats as stats
import cPickle as pkl
import numpy as np
import os.path
import prettytable
import gc

import TensorToolbox as TT

import setup
import Laplace

PLOTTING = True

###########################################
# Parameters
###########################################
params = setup.setup()

eps_list = [1e-4,1e-6,1e-8]
orders = [0,1,3,7]

exp_eps_list = []
exp_orders = []
# exp_eps_list = [1e-8]
# exp_orders = [0,1,2,3]

# Load KLE to find the dimension
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()
params['KLE'] = KLE
d = KLE.nvars

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)
poly_discr = params['poly_discr']

###########################################
# Recover UQ results
# uq_file = "RS_%s_full_%s_l%.3f_sig%.3f.pkl" % (params['problem'],params['C_type'], params['l'],params['sigma'])
uq_file = "RS_no2.5_%s_full_%s_l%.3f_sig%.3f.pkl" % (params['problem'],params['C_type'], params['l'],params['sigma'])
ff = open( params['EXP_PATH'] + '/' + uq_file ,'rb')
exp_driver = pkl.load(ff)
ff.close()

# Compute QoI for UQ results
res = exp_driver.get_results()
uq_res = np.asarray([ np.dot( poly_discr['interp_mat'], u )[0] for u in res ])
###########################################

# Compute L2 error and number of function evaluations
L2err = np.zeros( (len(eps_list), len(orders)) )
feval = np.zeros( (len(eps_list), len(orders)) )
for ie,eps in enumerate(eps_list):
    for io,order in enumerate(orders):
        # tt_file = "STT_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
        tt_file = "STT_no2.5_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))

        # Recover TT results
        try:
            STTapprox = TT.load( params['EXP_PATH'] + '/' + tt_file, load_data=True )
        except IOError:
            L2err[ie,io] = np.nan
            feval[ie,io] = np.nan
        except EOFError:
            print "File " + tt_file + " corrupted!"
            L2err[ie,io] = np.nan
            feval[ie,io] = np.nan
        else:
            if STTapprox.init:
                err_sq = (STTapprox(np.asarray(exp_driver.get_samples())) - uq_res)**2.
                intf_sq = uq_res**2.

                L2err[ie,io] = np.sqrt( np.mean(err_sq) / np.mean(intf_sq) )
                feval[ie,io] = STTapprox.TW.get_fill_level()
            else:
                L2err[ie,io] = np.nan
                feval[ie,io] = np.nan
            err_sq = None
            intf_sq = None
            STTapprox = None
            gc.collect()
    
    table = prettytable.PrettyTable(["Eps: %.1e" % eps] + [str(order) for order in orders])
    table.add_row(["L2err"] + ["%e" % err for err in L2err[ie,:].tolist()])
    table.add_row(["feval"] + ["%e" % n for n in feval[ie,:].tolist()])
    print table

exp_L2err = np.zeros( (len(exp_eps_list), len(exp_orders)) )
exp_feval = np.zeros( (len(exp_eps_list), len(exp_orders)) )
for ie,eps in enumerate(exp_eps_list):
    for io,order in enumerate(exp_orders):
        # tt_file = "STT_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
        tt_file = "STT_no2.5_%s_%s_l%.3f_sig%.3f_ord-exp%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))

        # Recover TT results
        try:
            STTapprox = TT.load( params['EXP_PATH'] + '/' + tt_file, load_data=True )
        except IOError:
            exp_L2err[ie,io] = np.nan
            exp_feval[ie,io] = np.nan
        except EOFError:
            print "File " + tt_file + " corrupted!"
            exp_L2err[ie,io] = np.nan
            exp_feval[ie,io] = np.nan
        else:
            if STTapprox.init:
                err_sq = (STTapprox(np.asarray(exp_driver.get_samples())) - uq_res)**2.
                intf_sq = uq_res**2.

                exp_L2err[ie,io] = np.sqrt( np.mean(err_sq) / np.mean(intf_sq) )
                exp_feval[ie,io] = STTapprox.TW.get_fill_level()
            else:
                exp_L2err[ie,io] = np.nan
                exp_feval[ie,io] = np.nan
            err_sq = None
            intf_sq = None
            STTapprox = None
            gc.collect()
    
    table = prettytable.PrettyTable(["Exp-Eps: %.1e" % eps] + [str(order) for order in orders])
    table.add_row(["L2err"] + ["%e" % err for err in exp_L2err[ie,:].tolist()])
    table.add_row(["feval"] + ["%e" % n for n in exp_feval[ie,:].tolist()])
    print table
        
if PLOTTING:
    import matplotlib.pyplot as plt

    figsize=(5,4)
    markers = ['-o','--v','-.s','*']
    
    # Full tensor
    full_feval = [ (o+1)**d for o in orders[:-1] ]

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    for ie, eps in enumerate(eps_list):
        mask = list( np.where(np.logical_not(np.isnan(feval[ie,:])) )[0] )
        plt.loglog( feval[ie,mask], L2err[ie,mask], '%sk' % markers[ie], label='Eps = %.0e' % eps )

    for ie, eps in enumerate(exp_eps_list):
        mask = list( np.where(np.logical_not(np.isnan(exp_feval[ie,:])) )[0] )
        plt.loglog( exp_feval[ie,mask], exp_L2err[ie,mask], '%sk' % markers[ie], label='Exp-Eps = %.0e' % eps )

    for n in full_feval:
        ax.loglog( [n]*2, ax.get_ylim(), '--k', linewidth=2 )


    plt.xlabel('#f.eval.')
    plt.ylabel('L2err')
    plt.grid()
    plt.legend(loc='best')
    plt.tight_layout()

    plt.figure(figsize=figsize)
    for ie, eps in enumerate(eps_list):
        mask = list( np.where( np.logical_not(np.isnan(L2err[ie,:])) )[0] )
        plt.semilogy( [ orders[i] for i in mask ], L2err[ie,mask], '%sk' % markers[ie], label='Eps = %.0e' % eps )
    plt.xlabel('Order')
    plt.ylabel('L2err')
    plt.grid()

    plt.legend(loc='best')
    plt.tight_layout()
    
    plt.show(block=False)
