import numpy as np

def setup():
    l = 1./16.
    dstoc = 11
    sigma = 1.
    const = 0.5
    C_type = 'sq'
    N_space = 800
    poly_ord = 40
    params = { 'd': 2,
               'dstoc': dstoc,
               'C_type': C_type,
               'l': l,
               'const': const,
               'sigma': sigma,
               'span': [(0.,1.),(0.,1.)],
               'problem': 'dir',
               'points': [np.array([0.75]),np.array([0.25])],
               'basefolder': '/home/dabi/Documents/universita/phD/BigData/TensorToolbox/HeatEquation/PolySolver',
               'CLUSTER_PATH': '/home/dabi/Documents/universita/phD/BigData/TensorToolbox/HeatEquation/PolySolver',
               'EXP_PATH': '/home/dabi/Documents/universita/phD/BigData/TensorToolbox/HeatEquation/PolySolver',
               'EXP_DIR': 'experiments',
               'kle_file': 'CW_%s-kle-%f.pkl' % (C_type,l),
               'N_space': N_space, # Discretization of the space
               'poly_ord': poly_ord, # Polynomial order
               'PLOTTING': False} 
    return params
