import logging
import scipy.stats as stats
import cPickle as pkl
import numpy as np
import os.path

from SpectralToolbox import Spectral1D as S1D
import TensorToolbox as TT

import setup
import Laplace

maxprocs = 60
store_freq = 1200

###########################################
# Parameters
###########################################
params = setup.setup()

if params['problem'] == 'dir':
    solver = Laplace.poly_solver_dirichlet_point
else:
    solver = Laplace.solver_point

kickrank = 10
eps = 1e-8
mv_eps = 1e-8
mv_maxit = 10000
order = 7

# order = 2

# exp_name = "STT_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
exp_name = "STT_no2.5_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
# exp_name = "STT_no2.5_%s_%s_l%.3f_sig%.3f_ord-exp%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
out_file = exp_name
log_file = exp_name + '.log'

print "Starting: %s" % exp_name
###########################################

logging.basicConfig( filename= params['EXP_PATH'] + '/' + log_file)
logger_TT = logging.getLogger(TT.__name__)
logger_TT.setLevel( logging.DEBUG )

# Load KLE to find the dimension
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()
params['KLE'] = KLE
d = KLE.nvars

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)
poly_discr = params['poly_discr']

# Remove not needed fields in params
params['KLE'].eigvecs = None
params['KLE'].eigvals = None
params['KLE'].eigvecs_fit = None
params['KLE'].order_valI = None
params['KLE'].tensor_valI = None

# Prepare grid
orders = [order] * d
# if order == 1:
#     orders = []
#     for o in range(3): orders.append(3)
#     for o in range(3,d): orders.append(1)

# if order == 2:
#     orders = []
#     for o in range(3): orders.append(7)
#     for o in range(3,7): orders.append(3)
#     for o in range(7,d): orders.append(1)

X = [ (S1D.HERMITEP_PROB, S1D.GAUSS, None, [-np.inf, np.inf]) for i in xrange(d)]

###########################################
# Run tests
###########################################
if os.path.isfile( params['CLUSTER_PATH'] + '/' + out_file + '.pkl' ):
    # Load file
    STTapprox = TT.load( params['CLUSTER_PATH'] + '/' + out_file, load_data=True )

    STTapprox.set_f( solver )
    STTapprox.set_params( params )
    STTapprox.store_freq = store_freq
    STTapprox.store_location = params['EXP_PATH'] + '/' + out_file
    STTapprox.TW.store_location = params['EXP_PATH'] + '/' + out_file
    STTapprox.mv_maxit = mv_maxit
    STTapprox.build( maxprocs )

else:
    STTapprox = TT.SQTT( solver, X, params, 
                         eps=eps, mv_eps=mv_eps, mv_maxit=mv_maxit, range_dim=0, orders=orders, 
                         method='ttdmrg', 
                         kickrank=kickrank,
                         surrogateONOFF=True, surrogate_type=TT.PROJECTION, 
                         store_location = params['EXP_PATH'] + '/' + out_file,
                         store_overwrite=True, store_freq=store_freq )
    STTapprox.build(maxprocs)

print "Ranks: %s" % str(STTapprox.TTapprox[0].ranks())
print "Fill: %d/%d" % (STTapprox.TW.get_fill_level(),STTapprox.TW.get_size())
