import logging
import scipy.stats as stats
import cPickle as pkl
import numpy as np
import os.path

from SpectralToolbox import Spectral1D as S1D
import TensorToolbox as TT

import setup_CW as setup
import Laplace_CW as Laplace

maxprocs = 1
store_freq = 1200

###########################################
# Parameters
###########################################
params = setup.setup()

if params['problem'] == 'dir':
    solver = Laplace.poly_solver_dirichlet_point
else:
    solver = Laplace.solver_point

kickrank = None
eps = 1e-2
mv_eps = 1e-2
mv_maxit = 10000
ISOTROPIC = True
order = 1

if ISOTROPIC:
    exp_name = "STT_CW_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
else:
    exp_name = "STT_CW_%s_%s_l%.3f_sig%.3f_ord-exp%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
out_file = exp_name
log_file = exp_name + '.log'

print "Starting: %s" % exp_name
###########################################

logging.basicConfig( filename= params['EXP_PATH'] + '/' + log_file)
logger_TT = logging.getLogger(TT.__name__)
logger_TT.setLevel( logging.DEBUG )

d = params['dstoc']

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)
poly_discr = params['poly_discr']

# Prepare grid
if ISOTROPIC:
    orders = [order] * d
else:
    # Prepare anisotropic grid
    if order == 0:
        orders = []
        for o in range(d): orders.append(0)

    if order == 1:
        orders = []
        for o in range(d): orders.append(1)

    if order == 2:
        orders = []
        for o in range(3): orders.append(3)
        for o in range(3,d): orders.append(1)

    if order == 3:
        orders = []
        for o in range(3): orders.append(7)
        for o in range(3,7): orders.append(3)
        for o in range(7,d): orders.append(1)

X = [ (S1D.JACOBI, S1D.GAUSS, [0.,0.], [-np.sqrt(3), np.sqrt(3)]) for i in xrange(d)]

###########################################
# Run tests
###########################################
if os.path.isfile( params['CLUSTER_PATH'] + '/' + out_file + '.pkl' ):
    # Load file
    STTapprox = TT.load( params['CLUSTER_PATH'] + '/' + out_file, load_data=True )

    STTapprox.set_f( solver )
    STTapprox.set_params( params )
    STTapprox.store_freq = store_freq
    STTapprox.store_location = params['EXP_PATH'] + '/' + out_file
    STTapprox.TW.store_location = params['EXP_PATH'] + '/' + out_file
    STTapprox.mv_maxit = mv_maxit
    STTapprox.build( maxprocs )

else:
    STTapprox = TT.SQTT( solver, X, params, 
                         eps=eps, mv_eps=mv_eps, mv_maxit=mv_maxit, range_dim=0, orders=orders, 
                         method='ttdmrg', 
                         kickrank=kickrank,
                         surrogateONOFF=True, surrogate_type=TT.PROJECTION, 
                         store_location = params['EXP_PATH'] + '/' + out_file,
                         store_overwrite=True, store_freq=store_freq )
    STTapprox.build(maxprocs)

print "Ranks: %s" % str(STTapprox.TTapprox[0].ranks())
print "Fill: %d/%d" % (STTapprox.TW.get_fill_level(),STTapprox.TW.get_size())
