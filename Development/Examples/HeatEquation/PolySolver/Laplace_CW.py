"""
-Nabla \cdot (kappa Nabla (u)) = f on the unit square.
u(0,y)=u(x,1)=1
du/dn(1,y)=du/dn(x,0)=0
"""

import cPickle as pkl
from scipy import stats
import numpy as np
from numpy import linalg as npla

import setup_CW as setup

def poly_discr_dirichlet_setup(params):
    from scipy import sparse
    from SpectralToolbox import Spectral1D as S1D
    from SpectralToolbox import SpectralND as SND

    P = params['poly_ord']
    
    poly1D = S1D.Poly1D(S1D.JACOBI,[0.0,0.0])
    poly = SND.PolyND([poly1D, poly1D])
    
    (x,w) = poly.GaussLobattoQuadrature([P,P])
    
    xspan = params['span'][0]
    yspan = params['span'][0]
    xScaled = x.copy()
    xScaled[:,0] = (x[:,0]+1.)/2. * (xspan[1]-xspan[0]) + xspan[0]
    xScaled[:,1] = (x[:,1]+1.)/2. * (yspan[1]-yspan[0]) + yspan[0]
    
    coord_x = np.unique(xScaled[:,0])
    coord_y = np.unique(xScaled[:,1])
    
    def f(x,y):
        return np.cos(x) * np.sin(y)
    
    BC_dir = []
    BC_x_neu = []
    BC_y_neu = []

    def BC_dir_f(x,y):
        return 0. * x;
        
    BC_dir_idx = []
    BC_dir_idx += np.where(xScaled[:,0] == xspan[0])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,0] == xspan[1])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,1] == yspan[0])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,1] == yspan[1])[0].tolist()
    BC_dir.append(BC_dir_idx)

    ''' Construct differential operator '''
    (x1D,w1D) = poly1D.GaussLobattoQuadrature(P)
    V1D = poly1D.GradVandermonde1D(x1D,P,0,norm=False)
    Vx1D = poly1D.GradVandermonde1D(x1D,P,1,norm=False)
    D1D = np.linalg.solve(V1D.T,Vx1D.T).T
    D1D = sparse.csc_matrix(D1D)

    # Dx1D = np.kron(D1D,np.eye(P+1))
    # Dy1D = np.kron(np.eye(P+1),D1D)
    Dx1D = sparse.kron(D1D,sparse.eye(P+1),format='bsr')
    Dy1D = sparse.kron(sparse.eye(P+1),D1D,format='bsr')

    # Set Neumann bc
    Dx1D_neu = Dx1D.copy()
    Dx1D_neu = Dx1D_neu.tolil()
    for BC_neu_idx,BC_neu_f in BC_x_neu:
        Dx1D_neu[BC_neu_idx,:] = 0.0 
        Dx1D_neu[BC_neu_idx,BC_neu_idx] = BC_neu_f(xScaled[BC_neu_idx,0],xScaled[BC_neu_idx,1])
    Dx1D_neu = Dx1D_neu.tobsr()

    Dy1D_neu = Dy1D.copy()
    Dy1D_neu = Dy1D_neu.tolil()
    for BC_neu_idx,BC_neu_f in BC_y_neu:
        Dy1D_neu[BC_neu_idx,:] = 0.0 
        Dy1D_neu[BC_neu_idx,BC_neu_idx] = BC_neu_f(xScaled[BC_neu_idx,0],xScaled[BC_neu_idx,1])
    Dy1D_neu = Dy1D_neu.tobsr()

    # Construct F
    F = f(xScaled[:,0],xScaled[:,1]) / poly1D.Gamma(0)**2.

    # Construct interpolating arrays
    baryc_w_x = S1D.BarycentricWeights( coord_x )
    baryc_w_y = S1D.BarycentricWeights( coord_y )
    interp_mat = np.kron( S1D.LagrangeInterpolationMatrix( coord_x, baryc_w_x, params['points'][0] ),
                          S1D.LagrangeInterpolationMatrix( coord_y, baryc_w_y, params['points'][1] ) )
    # V_interp = poly.GradVandermonde( params['points'], [params['poly_ord']]*2, [0]*2 )
    V_interp = np.kron( poly1D.GradVandermonde1D( params['points'][0]*2.-1., params['poly_ord'], 0 ),
                        poly1D.GradVandermonde1D( params['points'][1]*2.-1, params['poly_ord'], 0 ) )
    
    poly_discr = {'x': x,
                  'w': w,
                  'xScaled': xScaled,
                  'coord_x': coord_x,
                  'coord_y': coord_y,
                  'V1D': V1D,
                  'interp_mat': interp_mat,
                  'V_interp': V_interp,
                  'Dx1D': Dx1D,
                  'Dx1D_neu': Dx1D_neu,
                  'Dy1D': Dy1D,
                  'Dy1D_neu': Dy1D_neu,
                  'F': F,
                  'BC_dir': BC_dir,
                  'BC_dir_f_vals': BC_dir_f(xScaled[:,0],xScaled[:,1])}

    params['poly_discr'] = poly_discr

    # KL-expansion
    Lp = max(np.diff(params['span'][0])[0], 2.*params['l'])
    L = params['l']/Lp
    zeta = np.zeros(params['dstoc']-1)
    zetapsi = np.zeros((params['dstoc']-1, (params['poly_ord']+1)**2))
    for n in range(2,params['dstoc']+1):
        zeta[n-2] = np.sqrt(np.sqrt(np.pi) * L) * np.exp( - (np.floor(n/2) * np.pi * L)**2. / 8 )
        if n % 2 == 0:
            zetapsi[n-2,:] = zeta[n-2] * np.kron( np.sin( np.floor(n/2) * np.pi * poly_discr['coord_x'] / Lp ), np.ones(poly_discr['coord_y'].shape) )
        else:
            zetapsi[n-2,:] = zeta[n-2] * np.kron( np.cos( np.floor(n/2) * np.pi * poly_discr['coord_x'] / Lp ), np.ones(poly_discr['coord_y'].shape) )

    KLE = {'Lp': Lp,
           'L': L,
           'zeta': zeta,
           'zetapsi': zetapsi}
    params['KLE'] = KLE

    return params

def poly_solver_dirichlet(X,params):
    import numpy as np
    from scipy import sparse
    from scipy.sparse import linalg as spla
    
    KLE = params['KLE']
    coord_x = params['poly_discr']['coord_x']
    coord_y = params['poly_discr']['coord_y']
    Dx1D = params['poly_discr']['Dx1D']
    Dx1D_neu = params['poly_discr']['Dx1D_neu']
    Dy1D = params['poly_discr']['Dy1D']
    Dy1D_neu = params['poly_discr']['Dy1D_neu']
    F = params['poly_discr']['F']
    BC_dir = params['poly_discr']['BC_dir']
    BC_dir_f_vals = params['poly_discr']['BC_dir_f_vals']
    
    # Define diffusivity field
    kappa = params['const'] * np.exp( 1. + X[0] * np.sqrt(np.sqrt(np.pi) * KLE['L'] / 2.) +
                                      np.dot(X[1:], KLE['zetapsi']) )
    ks = kappa.size
    kappa = kappa.flatten().reshape( (1,ks) )
    V = sparse.dia_matrix( (kappa,[0]), shape=(ks,ks) )
    # V = np.diag(kappa.flatten())

    if params['PLOTTING']:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        X,Y = np.meshgrid(coord_x,coord_y)
        plt.figure()
        plt.contourf(X,Y,kappa.reshape(X.shape),levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)

    # Construct L operator
    L = (Dx1D.dot(V.dot(Dx1D_neu)) + Dy1D.dot(V.dot(Dy1D_neu)))
    L = L.tolil()
    
    # Impose Dirichlet Boundary Conditions
    for BC_dir_idx in BC_dir:
        for idx in BC_dir_idx:
            L[idx,:] = 0
            L[idx,idx] = 1.
        # L[BC_dir_idx,:] = 0.
        # L[BC_dir_idx,BC_dir_idx] = 1.
        F[BC_dir_idx] = BC_dir_f_vals[BC_dir_idx]
    
    # Transform sparse type
    L = L.tocsr()
    
    # Solve
    u = spla.spsolve(L,F)
    # (u,info) = spla.gmres(L,F,x0=params['u0'],tol=1e-8)

    if params['PLOTTING']:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X,Y = np.meshgrid(coord_x,coord_y)
        U = u.reshape(X.shape)
        ax.plot_surface(X,Y,U, rstride=1, cstride=1, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)
        plt.show(block=False)

    return u

def poly_solver_dirichlet_point(X,params):
    import numpy as np
    from scipy import sparse
    from scipy.sparse import linalg as spla
    
    KLE = params['KLE']
    coord_x = params['poly_discr']['coord_x']
    coord_y = params['poly_discr']['coord_y']
    Dx1D = params['poly_discr']['Dx1D']
    Dx1D_neu = params['poly_discr']['Dx1D_neu']
    Dy1D = params['poly_discr']['Dy1D']
    Dy1D_neu = params['poly_discr']['Dy1D_neu']
    F = params['poly_discr']['F']
    BC_dir = params['poly_discr']['BC_dir']
    BC_dir_f_vals = params['poly_discr']['BC_dir_f_vals']
    interp_mat = params['poly_discr']['interp_mat']
    
    # Define diffusivity field
    kappa = params['const'] * np.exp( 1. + X[0] * np.sqrt(np.sqrt(np.pi) * KLE['L'] / 2.) +
                                      np.dot(X[1:], KLE['zetapsi']) )
    ks = kappa.size
    kappa = kappa.flatten().reshape( (1,ks) )
    V = sparse.dia_matrix( (kappa,[0]), shape=(ks,ks) )
    # V = np.diag(kappa.flatten())

    if params['PLOTTING']:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        X,Y = np.meshgrid(coord_x,coord_y)
        plt.figure()
        plt.contourf(X,Y,kappa.reshape(X.shape),levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)

    # Construct L operator
    L = (Dx1D.dot(V.dot(Dx1D_neu)) + Dy1D.dot(V.dot(Dy1D_neu)))
    L = L.tolil()
    
    # Impose Dirichlet Boundary Conditions
    for BC_dir_idx in BC_dir:
        for idx in BC_dir_idx:
            L[idx,:] = 0
            L[idx,idx] = 1.
        # L[BC_dir_idx,:] = 0.
        # L[BC_dir_idx,BC_dir_idx] = 1.
        F[BC_dir_idx] = BC_dir_f_vals[BC_dir_idx]
    
    # Transform sparse type
    L = L.tocsr()
    
    # Solve
    u = spla.spsolve(L,F)
    # (u,info) = spla.gmres(L,F,x0=params['u0'],tol=1e-8)

    if params['PLOTTING']:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X,Y = np.meshgrid(coord_x,coord_y)
        U = u.reshape(X.shape)
        ax.plot_surface(X,Y,U, rstride=1, cstride=1, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)
        plt.show(block=False)

    return np.dot( interp_mat, u )[0]

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import time
    
    params = setup.setup()
    params['PLOTTING'] = True

    # Use the Polynomial solver
    params = poly_discr_dirichlet_setup(params)
    poly_discr = params['poly_discr']
    params['u0'] = None

    # Solve starting point
    xis = np.zeros(params['dstoc'])
    u0 = poly_solver_dirichlet( xis, params )
    params['u0'] = u0

    # Draw random values
    xis = stats.uniform(loc=-np.sqrt(3),scale=2*np.sqrt(3)).rvs(params['dstoc'])

    # Plot KL-coeffs
    plt.figure()
    plt.semilogy(params['KLE']['zeta'])
    plt.show(block=False)

    # Solve
    start = time.clock()
    u_poly = poly_solver_dirichlet( xis, params )
    stop = time.clock()
    print "Time: Solving Dirichlet Poly: " + str( stop - start )

    # Solve
    start = time.clock()
    u_poly_point = poly_solver_dirichlet_point( xis, params )
    stop = time.clock()
    print "Time: Solving Dirichlet Poly Point: " + str( stop - start )
    print "Point Value Poly: " + str(u_poly_point)

    # Plot Fourier coeff decay
    V1D = poly_discr['V1D']
    V = np.kron(V1D,V1D)
    u_h = npla.solve(V,u_poly)
    U_H = u_h.reshape( (params['poly_ord']+1,params['poly_ord']+1) )
    plt.figure()
    plt.imshow( np.log10(np.abs(U_H)), interpolation='none', origin='lower' )
    plt.colorbar()
    plt.show(block=False)
    
    
    # # Check function 1d
    # params['PLOTTING'] = False
    # ns = 10
    # N = 20
    # X = np.linspace(-np.sqrt(3),np.sqrt(3),N)
    # fval = np.zeros(N)
    # for j in range(ns):
    #     xis = np.zeros(params['dstoc'])
    #     for i in range(N):
    #         xis[j] = X[i]
    #         fval[i] = poly_solver_dirichlet_point( xis, params )

    #     plt.figure()
    #     plt.plot(X,fval)
    #     plt.title('n=%d' % j)
    #     plt.show(block=False)
