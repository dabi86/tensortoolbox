import cPickle as pkl
import numpy as np

from UQToolbox import ModelReduction as MR

import setup_CW as setup

#################################
# Parameters
#################################
params = setup.setup()

path = params['basefolder'] + "/" + params['kle_file']

#################################

if params['C_type'] == 'sq':
    def C(x1,x2): return np.exp(- (x1-x2)**2./ (2. * params['l']**2.)) + 1e-12 * (x1==x2)
elif params['C_type'] == 'orn':
    def C(x1,x2): return np.exp(-np.abs(x1-x2)/params['l'])

# Generates
NKL = [400]*params['d']
targetVar = 0.95
KLE = MR.KLExpansion(C,params['d'],params['span'])
KLE.fit(NKL,targetVar)

# stores the KLE expansion
ff = open(path,'wb')
pkl.dump(KLE,ff)
ff.close()
