#!/usr/bin/env python

import sys
import setup
import cPickle as pkl
import Laplace

def main(argv):
    identifier = int(argv[0])
    pp = setup.setup()
    
    # Load parameters
    ff = open( pp['EXP_PATH'] + "/" + pp['EXP_DIR'] + "/" + str(identifier) + ".pkl", 'rb' )
    (X, params) = pkl.load(ff)
    ff.close()
    
    # Load KLE to find the dimension and prefit
    ff = open( params['basefolder'] + "/" + params['kle_exp_file'] ,'rb')
    params['KLE'] = pkl.load(ff)
    ff.close()

    # Load poly_discr
    ff = open( params['basefolder'] + "/" + params['params_discr_exp_file'] ,'rb')
    params['poly_discr'] = pkl.load(ff)
    ff.close()
    
    # Solve the problem
    u = Laplace.poly_solver_dirichlet_point(X,params)
    
    # Store solution
    ff = open( pp['EXP_PATH'] + "/" + pp['EXP_DIR'] + "/" + str(identifier) + "-out.pkl", 'wb' )
    pkl.dump(u, ff)
    ff.close()
            
if __name__ == "__main__":
    main(sys.argv[1:])
