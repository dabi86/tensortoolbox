import scipy.stats as stats
import cPickle as pkl
import numpy as np
import os.path

from UQToolbox import RandomSampling as RS

import setup
import Laplace

ANALYSIS = True
maxprocs = 2
store_freq = 100
N = 2000

###########################################
# Parameters
###########################################
params = setup.setup()

if params['problem'] == 'dir':
    solver = Laplace.poly_solver_dirichlet
else:
    solver = Laplace.solver # Not working now!
# out_file = "RS_%s_full_%s_l%.3f_sig%.3f.pkl" % (params['problem'],params['C_type'], params['l'],params['sigma'])
out_file = "RS_no2.5_%s_full_%s_l%.3f_sig%.3f.pkl" % (params['problem'],params['C_type'], params['l'],params['sigma'])
###########################################

# Load KLE to find the dimension
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()
params['KLE'] = KLE
d = KLE.nvars

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)
poly_discr = params['poly_discr']

# Prepare distributions
dists = [stats.norm() for i in xrange(d)]

###########################################
# Run tests
###########################################
if os.path.isfile( params['EXP_PATH'] + '/' + out_file ):
    # Load file
    ff = open( params['EXP_PATH'] + '/' + out_file ,'rb')
    exp_driver = pkl.load(ff)
    ff.close()
    exp_driver.set_dists( dists )
    exp_driver.set_f(solver)
    
    Nnew = N - (len(exp_driver.get_samples()) + len(exp_driver.get_new_samples()))
    if Nnew > 0 or len(exp_driver.get_new_samples()) > 0:
        exp_driver.sample(Nnew, 'mc')
        exp_driver.run( maxprocs, store_freq )

else:
    exp_driver = RS.Experiments(solver, params, dists, 
                                store_file = params['EXP_PATH'] + '/' + out_file)
    
    exp_driver.sample( N, 'mc' )
    exp_driver.run( maxprocs, store_freq )

if ANALYSIS:
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm

    res = exp_driver.get_results()
    mean = np.mean( np.asarray(res), axis=0 )
    var = np.var( np.asarray(res), axis=0 )

    X,Y = np.meshgrid(poly_discr['coord_x'],poly_discr['coord_y'])
    MEAN = mean.reshape(X.shape)
    VAR = var.reshape(X.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X,Y,MEAN, rstride=1, cstride=1, cmap=cm.coolwarm,
                    linewidth=0, antialiased=False)
    plt.title('Mean')
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X,Y,VAR, rstride=1, cstride=1, cmap=cm.coolwarm,
                    linewidth=0, antialiased=False)
    plt.title('Var')
    plt.show(block=False)
    
    # Check mean and variance convergence for uq_point
    point_res = np.asarray([ np.dot( poly_discr['interp_mat'], u )[0] for u in res ])
    incr_mean = np.cumsum(point_res)/np.arange(1,len(res)+1)
    incr_var = (np.cumsum( (point_res - incr_mean)**2. ))/np.arange(len(res))

    print "Mean: %e, Var: %e, Perc. Std: %.2f" % (incr_mean[-1], incr_var[-1], np.sqrt(incr_var[-1])/incr_mean[-1] * 100.)

    plt.figure()
    plt.plot(np.arange(1,len(res)+1),incr_var)
    plt.title('Variance convergence')

    plt.figure()
    plt.plot(np.arange(1,len(res)+1),incr_mean)
    plt.title('Mean convergence')

    plt.show(block=False)
