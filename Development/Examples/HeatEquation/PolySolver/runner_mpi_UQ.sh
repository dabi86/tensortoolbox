#!/bin/bash
# -- our name ---
# -- request /bin/bash --
#$ -S /bin/bash
# -- run in the current working (submission) directory --
#$ -cwd
#$ -m bea
#$ -pe mpi 20
python UQ.py
