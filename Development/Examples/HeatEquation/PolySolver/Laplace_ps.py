def poly_solver_dirichlet_point(X,params):
    # import time
    # start_global = time.time()
    # start_in = time.clock()

    import subprocess
    import cPickle as pkl
    import os
    import numpy.random as npr
    
    # Open new file and dump in input data
    loop = True
    while loop:
        identifier = npr.randint(0,100000)
        path = params['EXP_PATH'] + "/" + params['EXP_DIR'] + "/" + str(identifier)
        try:
            ff = os.open(path + ".pkl",os.O_CREAT|os.O_EXCL)
            loop = False
        except OSError as exception:
            pass
    
    ff = open( path + ".pkl", 'wb' )
    pkl.dump( (X,params), ff )
    ff.close()
    
    # Run the command
    job_path = "python runner_ps.py"
    # out = open( path + '-run.out', 'w' )
    # err = open( path + '-run.err', 'w' )
    # subprocess.check_call("psqrsh -n " + params['HOST'] + " -p " + str(params['PORT']) + " -i 1.0 " + job_path + " " + str(identifier), stdout=out, stderr=err, shell=True)
    # out.close()
    # err.close()
    subprocess.check_call("psqrsh -n " + params['HOST'] + " -p " + str(params['PORT']) + " -i 1.0 " + job_path + " " + str(identifier), shell=True)
    
    # Read result
    ff = open( path + '-out.pkl', 'rb' )
    val = pkl.load(ff)
    ff.close()
    
    # Delet
    # os.remove( path + '-run.err')
    # os.remove( path + '-run.out')

    # Delete output file
    os.remove( path + '-out.pkl' )
    
    # Delete identifier file
    os.remove( path + '.pkl' )

    # stop_global = time.time()
    # stop_in = time.clock()
    # print "Elapsed time inside: " + str(stop_in-start_in) + " Global: " + str(stop_global-start_global)

    # Return
    return val
