import logging
import scipy.stats as stats
import cPickle as pkl
import numpy as np
import os.path
import subprocess
import shutil

import phantom_scheduler as ps
from SpectralToolbox import Spectral1D as S1D
import TensorToolbox as TT

import setup
import Laplace_ps
import Laplace

maxprocs = 60
slots = 60
store_freq = 1200

###########################################
# Parameters
###########################################
params = setup.setup()

if params['problem'] == 'dir':
    solver = Laplace_ps.poly_solver_dirichlet_point
else:
    solver = Laplace.solver_point # Not working now

kickrank = 10
eps = 1e-8
mv_eps = 1e-8
order = 7

# exp_name = "STT_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
exp_name = "STT_no2.5_%s_%s_l%.3f_sig%.3f_ord%d_eps%d" % (params['problem'],params['C_type'], params['l'], params['sigma'],  order, -int(np.log10(eps)))
out_file = exp_name
log_file = exp_name + '.log'

print "Starting: %s" % exp_name

###########################################
# Setup
###########################################
logging.basicConfig( filename= params['EXP_PATH'] + '/' + log_file)
logger_TT = logging.getLogger(TT.__name__)
logger_TT.setLevel( logging.DEBUG )

# Load KLE to find the dimension and prefit
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()

params['KLE'] = KLE
d = KLE.nvars

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)
poly_discr = params['poly_discr']

# Prepare grid
orders = [order] * d
X = [ (S1D.HERMITEP_PROB, S1D.GAUSS, None, [-np.inf, np.inf]) for i in xrange(d)]


###########################################
# Start scheduler
###########################################
HOST = 'localhost'
PORT = 50000
sched = ps.Scheduler( slots, "qsub -cwd -q long -b y", PORT ) # Cluster
# sched = ps.Scheduler( slots, "", PORT ) # Localhost
PORT = sched.get_input_port()
sched.run()
params['HOST'] = HOST
params['PORT'] = PORT

###########################################
# Store data to be realoaded on the fly
###########################################

# Remove KLE from params and store it once. I will be reloaded at runtime.
# Remove from KLE all the unused information (1d-eigenvectors)
KLE = params.pop('KLE',None)
KLE.eigvecs = None
KLE.eigvals = None
KLE.eigvecs_fit = None
KLE.order_valI = None
KLE.tensor_valI = None
params['kle_exp_file'] = 'kle-' + exp_name + '.pkl'
ff = open( params['basefolder'] + "/" + params['kle_exp_file'] ,'wb')
pkl.dump(KLE,ff)
ff.close()

# Store params and pass only the one needed to reload
# Remove all the params not used
params['params_discr_exp_file'] = 'params-discr-' + exp_name + '.pkl'
ff = open( params['basefolder'] + "/" + params['params_discr_exp_file'] ,'wb')
pkl.dump(poly_discr,ff)
ff.close()
params.pop('poly_discr',None)

###########################################
# Run tests
###########################################
if os.path.isfile( params['CLUSTER_PATH'] + '/' + out_file + '.pkl' ):
    # Load file
    STTapprox = TT.load( params['CLUSTER_PATH'] + '/' + out_file, load_data=True )

    STTapprox.set_f( solver )
    STTapprox.set_params( params )
    STTapprox.store_freq = store_freq
    STTapprox.store_location = params['CLUSTER_PATH'] + '/' + out_file
    STTapprox.TW.store_location = params['CLUSTER_PATH'] + '/' + out_file
    STTapprox.build( maxprocs )

else:
    STTapprox = TT.SQTT( solver, X, params, 
                         eps=eps, mv_eps=mv_eps, range_dim=0, orders=orders, 
                         method='ttdmrg', 
                         kickrank=kickrank,
                         surrogateONOFF=True, surrogate_type=TT.PROJECTION, 
                         store_location = params['CLUSTER_PATH'] + '/' + out_file,
                         store_overwrite=True, store_freq=store_freq )
    STTapprox.build(maxprocs)

# # Copy from cluster path to general path
# shutil.move( params['CLUSTER_PATH'] + '/' + out_file, params['EXP_PATH'] )

print "Ranks: %s" % str(STTapprox.TTapprox[0].ranks())
print "Fill: %d/%d" % (STTapprox.TW.get_fill_level(),STTapprox.TW.get_size())

###########################################
# Stop scheduler
###########################################
subprocess.check_call("psqsub -n %s -p %d EXIT" % (HOST,PORT), shell=True)
