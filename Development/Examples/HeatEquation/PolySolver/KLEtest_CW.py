import cPickle as pkl
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import setup_CW as setup

#################################
# Parameters
#################################
params = setup.setup()

# Use uniform spacing
x = np.linspace(0.,1.,50.)

# Load KLE
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()

# Draw random values
xis = stats.uniform(loc=-np.sqrt(3),scale=2*np.sqrt(3)).rvs(KLE.nvars)
print 'xis: ' + str(xis)

# Generate field
field = KLE.transform([x,x],[xis])

# Make it log-normal
kappa = np.exp(params['const'] + params['sigma'] * field[0])

###############################
# Plotting
###############################
X,Y = np.meshgrid(x,x)
plt.figure()
plt.contourf(X,Y,kappa,levels=np.linspace(np.min(kappa),np.max(kappa),40),origin='lower',cmap=plt.cm.bone)
plt.colorbar()
plt.show(block=False)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X,Y,kappa,rstride=1, cstride=1, cmap=plt.cm.coolwarm, linewidth=0, antialiased=False)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('$\kappa$')
plt.show(block=False)
