import cPickle as pkl
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import setup

#################################
# Parameters
#################################
params = setup.setup()

# Use uniform spacing
x = np.linspace(0.,1.,50.)

# Load KLE
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()

# Draw random values
xis = stats.norm().rvs(KLE.nvars)
print 'xis: ' + str(xis)

# Generate field
field = KLE.transform([x,x],[xis])

# Make it log-normal
kappa = np.exp(-2.5 + field[0])

###############################
# Plotting
###############################
X,Y = np.meshgrid(x,x)
plt.figure()
plt.contourf(X,Y,kappa,levels=np.linspace(0.,np.max(kappa),40),origin='lower')
plt.colorbar()
plt.show(block=False)
