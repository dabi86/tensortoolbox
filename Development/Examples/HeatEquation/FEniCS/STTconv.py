import logging
import scipy.stats as stats
import cPickle as pkl
import matplotlib.pyplot as plt
import numpy as np
import os.path
import dolfin as do

import TensorToolbox as TT

import setup

PLOTTING = True

###########################################
# Parameters
###########################################
params = setup.setup()
params_uq = setup.setup_uq()

eps_list = [1e-4,1e-6,1e-8,1e-10]
orders = [1,3,7,15]

uq_file = "RS_%s_full_%s_l%.3f.pkl" % (params_uq['problem'],params['C_type'], params['l'])
###########################################

# Recover UQ results
ff = open( params['EXP_PATH'] + '/' + uq_file ,'rb')
exp_driver = pkl.load(ff)
ff.close()

# Reconstruct the fenics mesh
NE = params['N_space']
mesh = do.UnitSquareMesh(NE,NE)
V = do.FunctionSpace(mesh, 'Lagrange', 1)

# Compute QoI for UQ results
full_res = exp_driver.get_results()
u_f_list = []
for u in full_res:
    u_f = do.Function(V)
    u_f.vector().set_local(u)
    u_f_list.append(u_f)
uq_res = np.asarray([ u_f(params_uq['point']) for u_f in u_f_list ])


L2err = np.zeros( (len(eps_list), len(orders)) )
feval = np.zeros( (len(eps_list), len(orders)) )
for ie,eps in enumerate(eps_list):
    for io,order in enumerate(orders):
        tt_file = "STT_%s_%s_l%.3f_ord%d_eps%d.pkl" % (params_uq['problem'],params['C_type'], params['l'], order, -int(np.log10(eps)))

        # Recover TT results
        try:
            ff = open( params['EXP_PATH'] + '/' + tt_file ,'rb')
        except IOError:
            L2err[ie,io] = np.nan
            feval[ie,io] = np.nan
        else:
            try:
                STTapprox = pkl.load(ff)
            except EOFError:
                print "File " + tt_file + " corrupted!"
                L2err[ie,io] = np.nan
                feval[ie,io] = np.nan
            else:
                if STTapprox.init:
                    err_sq = (STTapprox(np.asarray(exp_driver.get_samples())) - uq_res)**2.
                    intf_sq = uq_res**2.
                    
                    L2err[ie,io] = np.sqrt( np.mean(err_sq) / np.mean(intf_sq) )
                    feval[ie,io] = STTapprox.TW.get_fill_level()
                else:
                    L2err[ie,io] = np.nan
                    feval[ie,io] = np.nan
            ff.close()

        
if PLOTTING:
    plt.figure()
    for ie, eps in enumerate(eps_list):
        mask = list( np.where(np.logical_not(np.isnan(feval[ie,:])) )[0] )
        plt.loglog( feval[ie,mask], L2err[ie,mask], 'o-', label='Eps = %.0e' % eps )
    plt.xlabel('#f.eval.')
    plt.ylabel('L2err')
    plt.grid()
    plt.legend()

    plt.figure()
    for ie, eps in enumerate(eps_list):
        mask = list( np.where( np.logical_not(np.isnan(L2err[ie,:])) )[0] )
        plt.semilogy( [ orders[i] for i in mask ], L2err[ie,mask], 'o-', label='Eps = %.0e' % eps )
    plt.xlabel('Order')
    plt.ylabel('L2err')
    plt.grid()
    plt.legend()
    
    plt.show(block=False)
