import logging
import scipy.stats as stats
import cPickle as pkl
import matplotlib.pyplot as plt
import numpy as np
import os.path
import dolfin as do

import TensorToolbox as TT

import setup

###########################################
# Parameters
###########################################
params = setup.setup()
params_uq = setup.setup_uq()

eps = 1e-6
mv_eps = 1e-6
order = 7

uq_file = "RS_%s_full_%s_l%.3f.pkl" % (params_uq['problem'],params['C_type'], params['l'])
tt_file = "STT_%s_%s_l%.3f_ord%d_eps%d.pkl" % (params_uq['problem'],params['C_type'], params['l'],  order, -int(np.log10(eps)))
###########################################

# Recover TT results
ff = open( params['EXP_PATH'] + '/' + tt_file ,'rb')
STTapprox = pkl.load(ff)
ff.close()
STTapprox.build()

# Recover UQ results
ff = open( params['EXP_PATH'] + '/' + uq_file ,'rb')
exp_driver = pkl.load(ff)
ff.close()

# Reconstruct the fenics mesh
NE = params['N_space']
mesh = do.UnitSquareMesh(NE,NE)
V = do.FunctionSpace(mesh, 'Lagrange', 1)

# Compute QoI for UQ results
full_res = exp_driver.get_results()
u_f_list = []
for u in full_res:
    u_f = do.Function(V)
    u_f.vector().set_local(u)
    u_f_list.append(u_f)
uq_res = np.asarray([ u_f(params_uq['point']) for u_f in u_f_list ])

# Compute error
err_sq = (STTapprox(np.asarray(exp_driver.get_samples())) - uq_res)**2.
intf_sq = uq_res**2.

L2err = np.sqrt( np.mean(err_sq) / np.mean(intf_sq) )

print 'L2err: %e' % L2err
print 'Fill: %d/%d' % (STTapprox.TW.get_fill_level(), STTapprox.TW.get_size())
print 'Ranks: %s' % str(STTapprox.TTapprox[0].ranks())

# Plotting
d = len(STTapprox.Xs_params[0])
if d == 2:
    xp,yp = STTapprox.Xs_params[0]
    x = np.linspace(xp[0],xp[-1],20)
    y = np.linspace(yp[0],yp[-1],20)
    X,Y = np.meshgrid(x,y)
    app = STTapprox( np.vstack( (X.flatten(),Y.flatten()) ).T )
    app = app.reshape( X.shape )

    plt.figure()
    plt.imshow(app,extent=(x[0],x[-1],y[0],y[-1]),origin='lower')
    plt.colorbar()
    plt.title("Approx")

    # Get filled idxs
    fill_idxs = np.array(STTapprox.TW.get_fill_idxs())
    # Get last used idxs
    last_idxs = STTapprox.generic_approx[0].get_ttdmrg_eval_idxs()

    overlap = [np.any([np.all(i == x) for x in list(last_idxs)]) for i in list(fill_idxs)]
    notover = fill_idxs[np.logical_not(overlap),:]

    notover_coo = np.vstack( (STTapprox.Xs_params[0][0][notover[:,0]],
                              STTapprox.Xs_params[0][1][notover[:,1]]) ).T
    last_coo = np.vstack( (STTapprox.Xs_params[0][0][last_idxs[:,0]],
                           STTapprox.Xs_params[0][1][last_idxs[:,1]]) ).T

    plt.plot(notover_coo[:,0], notover_coo[:,1], 'ob')
    plt.plot(last_coo[:,0], last_coo[:,1], 'or')

    
    plt.figure()
    plt.imshow(np.log10(np.abs(STTapprox.TTfour[0].to_tensor())),interpolation='none',origin='lower')
    plt.colorbar()
    plt.title("Fourier")
    
    plt.show(block=False)
