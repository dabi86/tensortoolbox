import scipy.stats as stats
import cPickle as pkl
import matplotlib.pyplot as plt
import numpy as np
import os.path

from UQToolbox import RandomSampling as RS

import setup
import Laplace

ANALYSIS = True
maxprocs = 3
store_freq = 100
N = 1000

###########################################
# Parameters
###########################################
params = setup.setup()

if params['problem'] == 'dir':
    solver = Laplace.poly_solver_dirichlet
else:
    solver = Laplace.solver # Not working now!
out_file = "RS_%s_full_%s_l%.3f.pkl" % (params['problem'],params['C_type'], params['l'])
###########################################

# Load KLE to find the dimension
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()
params['KLE'] = KLE
d = KLE.nvars

# Prepare discretization parameters
params = Laplace.poly_discr_dirichlet_setup(params)

# Prepare distributions
dists = [stats.norm() for i in xrange(d)]

###########################################
# Run tests
###########################################
if os.path.isfile( params['EXP_PATH'] + '/' + out_file ):
    # Load file
    ff = open( params['EXP_PATH'] + '/' + out_file ,'rb')
    exp_driver = pkl.load(ff)
    ff.close()
    exp_driver.set_dists( dists )
    exp_driver.set_f(solver)
    
    Nnew = N - (len(exp_driver.get_samples()) + len(exp_driver.get_new_samples()))
    if Nnew > 0 or len(exp_driver.get_new_samples()) > 0:
        exp_driver.sample(Nnew, 'mc')
        exp_driver.run( maxprocs, store_freq )

else:
    exp_driver = RS.Experiments(solver, params, dists, 
                                store_file = params['EXP_PATH'] + '/' + out_file)
    
    exp_driver.sample( N, 'mc' )
    exp_driver.run( maxprocs, store_freq )

if ANALYSIS:
    res = exp_driver.get_results()
    mean = np.mean( np.asarray(res), axis=0 )
    var = np.var( np.asarray(res), axis=0 )

    # Reconstruct the fenics mesh
    NE = params['N_space']
    mesh = do.UnitSquareMesh(NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)

    # Define mean and var functions
    mean_f = do.Function(V)
    mean_f.vector().set_local(mean)
    var_f = do.Function(V)
    var_f.vector().set_local(var)
    
    do.plot(mean_f, title='Mean', axes=True)
    do.plot(var_f, title='Variance', axes=True)
    
    # Check mean and variance convergence for uq_point
    u_f_list = []
    for u in res:
        u_f = do.Function(V)
        u_f.vector().set_local(u)
        u_f_list.append(u_f)
    point = params_uq['point']
    point_res = np.asarray([ u_f(point) for u_f in u_f_list ])
    incr_mean = np.cumsum(point_res)/np.arange(1,len(res)+1)
    incr_var = (np.cumsum( (point_res - incr_mean)**2. ))/np.arange(len(res))

    plt.figure()
    plt.plot(np.arange(1,len(res)+1),incr_mean)
    plt.title('Mean convergence')
    plt.figure()
    plt.plot(np.arange(1,len(res)+1),incr_var)
    plt.title('Variance convergence')

    plt.show(block=False)
