import logging
import scipy.stats as stats
import cPickle as pkl
import matplotlib.pyplot as plt
import numpy as np
import os.path
import dolfin as do

from SpectralToolbox import Spectral1D as S1D
import TensorToolbox as TT

import setup
import Laplace

maxprocs = 8
store_freq = 2000

###########################################
# Parameters
###########################################
params = setup.setup()
params_uq = setup.setup_uq()
params['point'] = params_uq['point']

if params_uq['problem'] == 'dir':
    solver = Laplace.solver_dirichlet_point
else:
    solver = Laplace.solver_point

kickrank = 10
eps = 1e-8
mv_eps = 1e-8
order = 7

exp_name = "STT_%s_%s_l%.3f_ord%d_eps%d" % (params_uq['problem'],params['C_type'], params['l'],  order, -int(np.log10(eps)))
out_file = exp_name + '.pkl'
log_file = exp_name + '.log'

print "Starting: %s" % exp_name
###########################################

logging.basicConfig( filename= params['EXP_PATH'] + '/' + log_file)
logger_TT = logging.getLogger(TT.__name__)
logger_TT.setLevel( logging.DEBUG )

# Load KLE to find the dimension
ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
KLE = pkl.load(ff)
ff.close()
params['KLE'] = KLE
d = KLE.nvars

# Prepare grid
orders = [order] * d
X = [ (S1D.HERMITEP_PROB, S1D.GAUSS, None, [-np.inf, np.inf]) for i in xrange(d)]

###########################################
# Run tests
###########################################
if os.path.isfile( params['EXP_PATH'] + '/' + out_file ):
    # Load file
    ff = open( params['EXP_PATH'] + '/' + out_file ,'rb')
    STTapprox = pkl.load(ff)
    ff.close()

    STTapprox.set_f( solver )
    STTapprox.set_params( params )
    STTapprox.store_freq = store_freq
    STTapprox.build( maxprocs )

else:
    STTapprox = TT.SQTT( solver, X, params, 
                         eps=eps, mv_eps=mv_eps, range_dim=0, orders=orders, 
                         method='ttdmrg', 
                         kickrank=kickrank,
                         surrogateONOFF=True, surrogate_type=TT.PROJECTION, 
                         store_location = params['EXP_PATH'] + '/' + out_file,
                         store_overwrite=True, store_freq=store_freq )
    STTapprox.build(maxprocs)

print "Ranks: %s" % str(STTapprox.TTapprox[0].ranks())
print "Fill: %d/%d" % (STTapprox.TW.get_fill_level(),STTapprox.TW.get_size())
