import numpy as np

def setup():
    l = 0.25
    C_type = 'sq'
    N_space = 200
    poly_ord = 40
    params = { 'd': 2,
               'C_type': C_type,
               'l': l,
               'span': [(0.,1.),(0.,1.)],
               'problem': 'dir',
               'points': [np.array([0.75]),np.array([0.25])],
               'basefolder': '/home/dabi/Documents/universita/phD/BigData/TensorToolbox/HeatEquation',
               'EXP_PATH': '/home/dabi/Documents/universita/phD/BigData/TensorToolbox/HeatEquation',
               'kle_file': '%s-kle-%f.pkl' % (C_type,l),
               'N_space': N_space, # Discretization of the space
               'poly_ord': poly_ord, # Polynomial order
               'PLOTTING': False} 
    return params

def setup_uq():
    params = {'problem': 'dir',
              'point': (0.75,0.25)}
    return params
