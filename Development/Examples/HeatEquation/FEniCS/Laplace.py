"""
-Nabla \cdot (kappa Nabla (u)) = f on the unit square.
u(0,y)=u(x,1)=1
du/dn(1,y)=du/dn(x,0)=0
"""

import cPickle as pkl
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as npla
import dolfin as do

import setup

def poly_discr_dirichlet_setup(params):
    from scipy import sparse
    from SpectralToolbox import Spectral1D
    from SpectralToolbox import SpectralND

    P = params['poly_ord']
    
    poly1D = Spectral1D.Poly1D(Spectral1D.JACOBI,[0.0,0.0])
    poly = SpectralND.PolyND([poly1D, poly1D])
    
    (x,w) = poly.GaussLobattoQuadrature([P,P])
    
    xspan = params['span'][0]
    yspan = params['span'][0]
    xScaled = x.copy()
    xScaled[:,0] = (x[:,0]+1.)/2. * (xspan[1]-xspan[0]) + xspan[0]
    xScaled[:,1] = (x[:,1]+1.)/2. * (yspan[1]-yspan[0]) + yspan[0]
    
    coord_x = np.unique(xScaled[:,0])
    coord_y = np.unique(xScaled[:,1])
    
    def f(x,y):
        return -np.ones(x.shape)
    
    BC_dir = []
    BC_x_neu = []
    BC_y_neu = []

    def BC_dir_f(x,y):
        return 0. * x;
        
    BC_dir_idx = []
    BC_dir_idx += np.where(xScaled[:,0] == xspan[0])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,0] == xspan[1])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,1] == yspan[0])[0].tolist()
    BC_dir_idx += np.where(xScaled[:,1] == yspan[1])[0].tolist()
    BC_dir.append(BC_dir_idx)

    ''' Construct differential operator '''
    (x1D,w1D) = poly1D.GaussLobattoQuadrature(P)
    V1D = poly1D.GradVandermonde1D(x1D,P,0,norm=False)
    Vx1D = poly1D.GradVandermonde1D(x1D,P,1,norm=False)
    D1D = np.linalg.solve(V1D.T,Vx1D.T).T
    D1D = sparse.csc_matrix(D1D)

    # Dx1D = np.kron(D1D,np.eye(P+1))
    # Dy1D = np.kron(np.eye(P+1),D1D)
    Dx1D = sparse.kron(D1D,sparse.eye(P+1),format='bsr')
    Dy1D = sparse.kron(sparse.eye(P+1),D1D,format='bsr')

    # Set Neumann bc
    Dx1D_neu = Dx1D.copy()
    Dx1D_neu = Dx1D_neu.tolil()
    for BC_neu_idx,BC_neu_f in BC_x_neu:
        Dx1D_neu[BC_neu_idx,:] = 0.0 
        Dx1D_neu[BC_neu_idx,BC_neu_idx] = BC_neu_f(xScaled[BC_neu_idx,0],xScaled[BC_neu_idx,1])
    Dx1D_neu = Dx1D_neu.tobsr()

    Dy1D_neu = Dy1D.copy()
    Dy1D_neu = Dy1D_neu.tolil()
    for BC_neu_idx,BC_neu_f in BC_y_neu:
        Dy1D_neu[BC_neu_idx,:] = 0.0 
        Dy1D_neu[BC_neu_idx,BC_neu_idx] = BC_neu_f(xScaled[BC_neu_idx,0],xScaled[BC_neu_idx,1])
    Dy1D_neu = Dy1D_neu.tobsr()

    # Construct F
    F = f(xScaled[:,0],xScaled[:,1]) / poly1D.Gamma(0)**2.

    # Construct interpolating arrays
    V_interp = poly.GradVandermonde( params['points'], [params['poly_ord']]*2, [0]*2 )
    
    poly_discr = {'x': x,
                  'w': w,
                  'xScaled': xScaled,
                  'coord_x': coord_x,
                  'coord_y': coord_y,
                  'V1D': V1D,
                  'V_interp': V_interp,
                  'Dx1D': Dx1D,
                  'Dx1D_neu': Dx1D_neu,
                  'Dy1D': Dy1D,
                  'Dy1D_neu': Dy1D_neu,
                  'F': F,
                  'BC_dir': BC_dir,
                  'BC_dir_f_vals': BC_dir_f(xScaled[:,0],xScaled[:,1])}

    params['poly_discr'] = poly_discr

    # Pre-fit KLE
    KLE = params['KLE']
    KLE.fit_grid( [poly_discr['coord_x'],poly_discr['coord_y']] )
    params['KLE'] = KLE
    
    return params

def poly_solver_dirichlet(X,params):
    import numpy as np
    from scipy import sparse
    from scipy.sparse import linalg as spla
    
    KLE = params['KLE']
    coord_x = params['poly_discr']['coord_x']
    coord_y = params['poly_discr']['coord_y']
    Dx1D = params['poly_discr']['Dx1D']
    Dx1D_neu = params['poly_discr']['Dx1D_neu']
    Dy1D = params['poly_discr']['Dy1D']
    Dy1D_neu = params['poly_discr']['Dy1D_neu']
    F = params['poly_discr']['F']
    BC_dir = params['poly_discr']['BC_dir']
    BC_dir_f_vals = params['poly_discr']['BC_dir_f_vals']
    
    # Define diffusivity field
    g = KLE.transform([coord_x,coord_y],[X])
    kappa = np.exp(-2.5 + 0.25 * g[0])
    ks = kappa.size
    kappa = kappa.flatten().reshape( (1,ks) )
    V = sparse.dia_matrix( (kappa,[0]), shape=(ks,ks) )
    # V = np.diag(kappa.flatten())

    if params['PLOTTING']:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        X,Y = np.meshgrid(coord_x,coord_y)
        plt.figure()
        plt.contourf(X,Y,kappa.reshape(X.shape),levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)

    # Construct L operator
    L = (Dx1D.dot(V.dot(Dx1D_neu)) + Dy1D.dot(V.dot(Dy1D_neu)))
    L = L.tolil()
    
    # Impose Dirichlet Boundary Conditions
    for BC_dir_idx in BC_dir:
        for idx in BC_dir_idx:
            L[idx,:] = 0
            L[idx,idx] = 1.
        # L[BC_dir_idx,:] = 0.
        # L[BC_dir_idx,BC_dir_idx] = 1.
        F[BC_dir_idx] = BC_dir_f_vals[BC_dir_idx]
    
    # Transform sparse type
    L = L.tocsr()
    
    # Solve
    u = spla.spsolve(L,F)

    if params['PLOTTING']:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X,Y = np.meshgrid(coord_x,coord_y)
        U = u.reshape(X.shape)
        ax.plot_surface(X,Y,U, rstride=1, cstride=1, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)
        plt.show(block=False)

    return u

def solver_dirichlet(X, params):
    import dolfin as do
    import numpy as np
    # import time
    from petsc4py import PETSc
    from mpi4py import MPI

    do.set_log_level(do.ERROR)

    comm = PETSc.Comm(MPI.COMM_SELF)

    KLE = params['KLE']

    # Create mesh and define function space
    # start = time.clock()
    NE = params['N_space']
    # mesh = do.UnitSquareMesh(NE,NE)
    mesh = do.UnitSquareMesh(comm,NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)
    # stop = time.clock()
    # print "Solver:Timing:Mesh: \t" + str(stop-start)

    # Boundary conditions
    def DirichletBoundary(x,on_boundary):
        return on_boundary
        return (abs(x[0]) < do.DOLFIN_EPS or abs(x[1]-1.) < do.DOLFIN_EPS)

    # Define Dirichlet Boundary Conditions
    f_dir = do.Constant(0.0)
    bc = do.DirichletBC(V, f_dir, DirichletBoundary)

    # Define Neumann Boundary Conditions
    f_neu = do.Constant(0.0)

    # Define diffusivity field
    # start = time.clock()
    coord = mesh.coordinates()
    (u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
    (u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
    g = KLE.transform([u_x,u_y],[X])
    kappa = np.exp(-2.5 + 0.25 * g[0])
    # stop = time.clock()
    # print "Solver:Timing:Diffusivity: \t" + str(stop-start)
    
    if params['PLOTTING']:
        X,Y = np.meshgrid(u_x,u_y)
        plt.figure()
        plt.contourf(X,Y,kappa,levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)
    
    # Construct approximation of kappa in fenics
    # start = time.clock()
    kappa = kappa.flatten()
    kappa_fun_FE = do.Function(V)
    kappa_fun_FE.vector().set_local(kappa)
    # stop = time.clock()
    # print "Solver:Timing:DiffusivityFE: \t" + str(stop-start)
    
    if params['PLOTTING']:
        do.plot(kappa_fun_FE,title="Kappa fenics",axes=True)

    # Define the source function f 
    f = do.Constant(1.0)

    # Define variational problem
    # start = time.clock()
    u = do.TrialFunction(V)
    v = do.TestFunction(V)
    a = kappa_fun_FE * do.inner(do.nabla_grad(u), do.nabla_grad(v))*do.dx
    L = f*v*do.dx - f_neu*v*do.ds # right handside and impose Neumann
    # stop = time.clock()
    # print "Solver:Timing:Problem: \t" + str(stop-start)

    # Compute solution
    # start = time.clock()
    u = do.Function(V)
    do.solve(a == L, u, bc)
    # stop = time.clock()
    # print "Solver:Timing:Solve: \t" + str(stop-start)


    if params['PLOTTING']:
        # Plot solution and mesh
        do.plot(u,axes=True,title='Dirichlet')
        # Hold plot
        do.interactive()

    return u.vector().array() 

def solver_dirichlet_point(X, params):
    import dolfin as do
    import numpy as np
    from petsc4py import PETSc
    from mpi4py import MPI

    do.set_log_level(do.ERROR)

    comm = PETSc.Comm(MPI.COMM_SELF)

    KLE = params['KLE']

    # Create mesh and define function space
    NE = params['N_space']
    # mesh = do.UnitSquareMesh(NE,NE)
    mesh = do.UnitSquareMesh(comm=comm,nx=NE,ny=NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)

    # Boundary conditions
    def DirichletBoundary(x,on_boundary):
        return on_boundary
        return (abs(x[0]) < do.DOLFIN_EPS or abs(x[1]-1.) < do.DOLFIN_EPS)

    # Define Dirichlet Boundary Conditions
    f_dir = do.Constant(0.0)
    bc = do.DirichletBC(V, f_dir, DirichletBoundary)

    # Define Neumann Boundary Conditions
    f_neu = do.Constant(0.0)

    # Define diffusivity field
    coord = mesh.coordinates()
    (u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
    (u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
    g = KLE.transform([u_x,u_y],[X])
    kappa = np.exp(-2.5 + 0.1 * g[0])
    
    if params['PLOTTING']:
        X,Y = np.meshgrid(u_x,u_y)
        plt.figure()
        plt.contourf(X,Y,kappa,levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)
    
    # Construct approximation of kappa in fenics
    kappa = kappa.flatten()
    kappa_fun_FE = do.Function(V)
    kappa_fun_FE.vector().set_local(kappa)
    
    if params['PLOTTING']:
        do.plot(kappa_fun_FE,title="Kappa fenics",axes=True)

    # Define the source function f 
    f = do.Constant(1.0)

    # Define variational problem
    u = do.TrialFunction(V)
    v = do.TestFunction(V)
    a = kappa_fun_FE * do.inner(do.nabla_grad(u), do.nabla_grad(v))*do.dx
    L = f*v*do.dx - f_neu*v*do.ds # right handside and impose Neumann

    # Compute solution
    u = do.Function(V)
    do.solve(a == L, u, bc)

    if params['PLOTTING']:
        # Plot solution and mesh
        do.plot(u,axes=True,title='Dirichlet')
        # Hold plot
        do.interactive()

    return u(params['point'])
    

def solver(X, params):
    import dolfin as do
    import numpy as np
    from petsc4py import PETSc
    from mpi4py import MPI

    do.set_log_level(do.ERROR)

    comm = PETSc.Comm(MPI.COMM_SELF)

    KLE = params['KLE']

    # Create mesh and define function space
    NE = params['N_space']
    # mesh = do.UnitSquareMesh(NE,NE)
    mesh = do.UnitSquareMesh(comm,NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)

    # Boundary conditions
    def DirichletBoundary(x,on_boundary):
        return (abs(x[0]) < do.DOLFIN_EPS or abs(x[1]-1.) < do.DOLFIN_EPS)

    # Define Dirichlet Boundary Conditions
    f_dir = do.Constant(0.0)
    bc = do.DirichletBC(V, f_dir, DirichletBoundary)

    # Define Neumann Boundary Conditions
    f_neu = do.Constant(0.0)

    # Define diffusivity field
    coord = mesh.coordinates()
    (u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
    (u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
    g = KLE.transform([u_x,u_y],[X])
    kappa = np.exp(-2.5 + 0.1 * g[0])
    
    if params['PLOTTING']:
        X,Y = np.meshgrid(u_x,u_y)
        plt.figure()
        plt.contourf(X,Y,kappa,levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)
    
    # Construct approximation of kappa in fenics
    kappa = kappa.flatten()
    kappa_fun_FE = do.Function(V)
    kappa_fun_FE.vector().set_local(kappa)
    
    if params['PLOTTING']:
        do.plot(kappa_fun_FE,title="Kappa fenics",axes=True)

    # Define the source function f 
    f = do.Constant(1.0)

    # Define variational problem
    u = do.TrialFunction(V)
    v = do.TestFunction(V)
    a = kappa_fun_FE * do.inner(do.nabla_grad(u), do.nabla_grad(v))*do.dx
    L = f*v*do.dx - f_neu*v*do.ds # right handside and impose Neumann

    # Compute solution
    u = do.Function(V)
    do.solve(a == L, u, bc)

    if params['PLOTTING']:
        # Plot solution and mesh
        do.plot(u,axes=True,title='Dirichlet')
        # Hold plot
        do.interactive()

    return u.vector().array() 

def solver_point(X, params):
    import dolfin as do
    import numpy as np
    from petsc4py import PETSc
    from mpi4py import MPI

    do.set_log_level(do.ERROR)

    comm = PETSc.Comm(MPI.COMM_SELF)

    KLE = params['KLE']

    # Create mesh and define function space
    NE = params['N_space']
    # mesh = do.UnitSquareMesh(NE,NE)
    mesh = do.UnitSquareMesh(comm,NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)

    # Boundary conditions
    def DirichletBoundary(x,on_boundary):
        return (abs(x[0]) < do.DOLFIN_EPS or abs(x[1]-1.) < do.DOLFIN_EPS)

    # Define Dirichlet Boundary Conditions
    f_dir = do.Constant(0.0)
    bc = do.DirichletBC(V, f_dir, DirichletBoundary)

    # Define Neumann Boundary Conditions
    f_neu = do.Constant(0.0)

    # Define diffusivity field
    coord = mesh.coordinates()
    (u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
    (u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
    g = KLE.transform([u_x,u_y],[X])
    kappa = np.exp(-2.5 + 0.1 * g[0])
    
    if params['PLOTTING']:
        X,Y = np.meshgrid(u_x,u_y)
        plt.figure()
        plt.contourf(X,Y,kappa,levels=np.linspace(0.,np.max(kappa),40),origin='lower')
        plt.colorbar()
        plt.title("Kappa numpy")
        plt.show(block=False)
    
    # Construct approximation of kappa in fenics
    kappa = kappa.flatten()
    kappa_fun_FE = do.Function(V)
    kappa_fun_FE.vector().set_local(kappa)
    
    if params['PLOTTING']:
        do.plot(kappa_fun_FE,title="Kappa fenics",axes=True)

    # Define the source function f 
    f = do.Constant(1.0)

    # Define variational problem
    u = do.TrialFunction(V)
    v = do.TestFunction(V)
    a = kappa_fun_FE * do.inner(do.nabla_grad(u), do.nabla_grad(v))*do.dx
    L = f*v*do.dx - f_neu*v*do.ds # right handside and impose Neumann

    # Compute solution
    u = do.Function(V)
    do.solve(a == L, u, bc)

    if params['PLOTTING']:
        # Plot solution and mesh
        do.plot(u,axes=True,title='Dirichlet')
        # Hold plot
        do.interactive()

    return u(params['point'])

if __name__ == "__main__":
    import time
    
    params = setup.setup()
    params['PLOTTING'] = True
    
    start = time.clock()
    # Load KLE
    ff = open( params['basefolder'] + "/" + params['kle_file'] ,'rb')
    KLE = pkl.load(ff)
    ff.close()
    stop = time.clock()
    print "Time: Loading KLE: " + str( stop - start )
    
    # Pre-fit
    NE = params['N_space']
    mesh = do.UnitSquareMesh(NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)
    coord = mesh.coordinates()
    (u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
    (u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
    KLE.fit_grid([u_x,u_y])

    params['KLE'] = KLE

    # Draw random values
    xis = stats.norm().rvs(KLE.nvars)
    # xis = np.zeros(KLE.nvars)

    # Run the test
    start = time.clock()
    u_fe = solver_dirichlet( xis, params )
    stop = time.clock()
    print "Time: Solving Dirichlet: " + str( stop - start )
    # start = time.clock()
    # solver( xis, params )
    # stop = time.clock()
    # print "Time: Solving Dirichlet-Neumann: " + str( stop - start )

    # Use the Polynomial solver
    params = poly_discr_dirichlet_setup(params)
    poly_discr = params['poly_discr']
    
    # Solve
    start = time.clock()
    u_poly = poly_solver_dirichlet( xis, params )
    stop = time.clock()
    print "Time: Solving Dirichlet Poly: " + str( stop - start )
    
    # Reconstruct FE function
    NE = params['N_space']
    mesh = do.UnitSquareMesh(NE,NE)
    V = do.FunctionSpace(mesh, 'Lagrange', 1)
    u_fun_FE = do.Function(V)
    u_fun_FE.vector().set_local(u_fe)
    
    # Compute FE solution on poly grid
    u_fe_poly = np.zeros(u_poly.shape)
    for i,(x,y) in enumerate(zip( poly_discr['xScaled'][:,0], poly_discr['xScaled'][:,1] ) ):
        u_fe_poly[i] = u_fun_FE((x,y))
    U_FE= u_fe_poly.reshape( (params['poly_ord']+1, params['poly_ord']+1) ).T
    u_fe_poly = U_FE.flatten()

    # Plot FE solution on poly grid
    if params['PLOTTING']:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        X,Y = np.meshgrid(poly_discr['coord_x'],poly_discr['coord_y'])
        U_FE= u_fe_poly.reshape(X.shape)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X,Y,U_FE, rstride=1, cstride=1, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)
        plt.title('FE')
        plt.show(block=False)
    
    # Plot difference
    err = np.abs(u_poly - u_fe_poly)
    if params['PLOTTING']:
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import cm
        X,Y = np.meshgrid(poly_discr['coord_x'],poly_discr['coord_y'])
        ERR = err.reshape(X.shape)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X,Y,ERR, rstride=1, cstride=1, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)
        plt.title('error')
        plt.show(block=False)

    L2err = np.dot(err, poly_discr['w'])
    print "L2 err = %e" % L2err

    # Plot Fourier coeff decay
    V1D = poly_discr['V1D']
    V = np.kron(V1D,V1D)
    u_h = npla.solve(V,u_poly)
    U_H = u_h.reshape( (params['poly_ord']+1,params['poly_ord']+1) )
    plt.figure()
    plt.imshow( np.log10(np.abs(U_H)), interpolation='none', origin='lower' )
    plt.colorbar()
    plt.show(block=False)
