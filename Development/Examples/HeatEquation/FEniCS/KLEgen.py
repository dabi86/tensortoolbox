import cPickle as pkl
import numpy as np
import dolfin as do

from UQToolbox import ModelReduction as MR

import setup

#################################
# Parameters
#################################
params = setup.setup()

path = params['basefolder'] + "/" + params['kle_file']

#################################

if params['C_type'] == 'sq':
    def C(x1,x2): return np.exp(- (x1-x2)**2./ (2. * params['l']**2.)) + 1e-12 * (x1==x2)
elif params['C_type'] == 'orn':
    def C(x1,x2): return np.exp(-np.abs(x1-x2)/params['l'])

# Generates
NKL = [400]*params['d']
targetVar = 0.95
KLE = MR.KLExpansion(C,params['d'],params['span'])
KLE.fit(NKL,targetVar)

# Pre-fit to the grid
NE = params['N_space']
mesh = do.UnitSquareMesh(NE,NE)
V = do.FunctionSpace(mesh, 'Lagrange', 1)
coord = mesh.coordinates()
(u_x, u_x_idx, u_x_inv) = np.unique(coord[:,0], True, True)
(u_y, u_y_idx, u_y_inv) = np.unique(coord[:,1], True, True)
KLE.fit_grid([u_x,u_y])

# stores the KLE expansion
ff = open(path,'wb')
pkl.dump(KLE,ff)
ff.close()
