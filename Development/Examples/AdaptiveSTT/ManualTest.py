#
# This file is part of TensorToolbox.
#
# TensorToolbox is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TensorToolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TensorToolbox.  If not, see <http://www.gnu.org/licenses/>.
#
# DTU UQ Library
# Copyright (C) 2014-2015 The Technical University of Denmark
# Scientific Computing Section
# Department of Applied Mathematics and Computer Science
#
# Author: Daniele Bigoni
#

import logging
import sys
import os
import os.path
import copy
import time as systime
import numpy as np
import numpy.linalg as npla
import scipy.stats as stats
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import itertools

from SpectralToolbox import Spectral1D as S1D
import TensorToolbox as TT
from TensorToolbox import multilinalg as mla
from UQToolbox import RandomSampling as RS

STORE_FIG = True
FORMATS = ['pdf','png','eps']
STORE_LOCATION = '/home/dabi/Documents/universita/phD/phD-DTU/Research/Uncertainty Quantification/ThesisExamples/AdaptiveSTT/figs/'
font = {'family' : 'normal',
        'size'   : 14}
matplotlib.rc('font', **font)

# Test adaptivity strategy
xspan = [0,1]
baseord = 2
d = 6
eps_ad = 1e-5
maxit = 50
err_ord = 2

dist = stats.uniform()
DIST = RS.MultiDimDistribution([dist] * d)
N_LHC = 1000
vol = 1.

# title = 'anisotropic-exp'
# alpha = 0.1
# def ord_inc(o):
#     return 2 * o

# title = 'anisotropic-linear'
# alpha = 0.5
# def ord_inc(o):
#     return o + 2

title = 'isotropic'
alpha = 1.
def ord_inc(o):
    return o + 2

def fun(X,params=None):
    if (X.ndim == 1):
        return 1./(np.sum( X * np.exp(np.arange(X.shape[0])) ) + 1)
    else:
        return 1./(np.sum( X * np.exp(np.arange(X.shape[1]))[np.newaxis,:], axis=1 ) + 1)

# Plot 2D
Xlin = np.linspace(xspan[0],xspan[1],100)
XXlin, YYlin = np.meshgrid(Xlin,Xlin)
XYlin = np.vstack( (XXlin.flatten(), YYlin.flatten()) ).T
fig = plt.figure(figsize=(6,5))
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(XXlin,YYlin, fun(XYlin).reshape((100,100)), rstride=1, cstride=1,
                cmap=plt.cm.coolwarm, linewidth=0, antialiased=False)
plt.show(False)

# Construct STT approximations
eps = 1e-6
surr_type = TT.PROJECTION
X = [ (S1D.JACOBI, S1D.GAUSSLOBATTO, (0.,0.), [0.,1.]) ] * d

ord_low_list = []
ord_hig_list = []
err_dir_list = []
tot_err_list = []
feval_low_list = []
size_low_list = []
ranks_low_list = []
feval_hig_list = []
size_hig_list = []
ranks_hig_list = []
l2err_list = []

it_count = 0
ord_low = [ baseord ] * d
ref_dir = []
tot_err = 10 * eps_ad
while tot_err > eps_ad and it_count < maxit:
    it_count += 1
    
    # Construct lowest order approx
    ord_low = [ (o if i not in ref_dir else ord_inc(o)) for i,o in enumerate(ord_low) ]
    STTapprox_low = TT.SQTT(fun, X, None, eps=eps, orders=ord_low, method='ttdmrg', surrogateONOFF=True, surrogate_type=surr_type)
    STTapprox_low.build()

    # Construct high approx
    ord_hig = [ ord_inc(o) for o in ord_low ]
    STTapprox_hig = TT.SQTT(fun, X, None, eps=eps, orders=ord_hig, method='ttdmrg', surrogateONOFF=True, surrogate_type=surr_type)
    STTapprox_hig.build()

    print "Iteration %d" % (it_count)
    print "LOW  order: %s" % str(ord_low)
    print "HIGH order: %s" % str(ord_hig)

    # Extract TTfour from both
    TTf_low = STTapprox_low.TTfour[0]
    TTf_hig = STTapprox_hig.TTfour[0]

    # Compute single error contributions
    err_dim = []
    subtens_dim = []
    cores = [ ttc[:,slice(0,ord_low[j]+1,None),:] for j,ttc in enumerate(TTf_hig.TT) ]
    subt = TT.TTvec(cores)
    subt.build()
    subtens_dim.append( [subt] )
    err_dim.append( [mla.norm(subt-TTf_low,'fro')] )
    idxs_dim = [ [()] ]
    for i in xrange(1,min(err_ord+1,d+1)):
        iterator = itertools.combinations(xrange(d),i)
        err = []
        subtens = []
        idxs = []
        for it in iterator:
            idxs.append( it )
            # Construct the TTapprox of the subtensor
            cores = [ (ttc[:,slice( ord_low[j]+1, ord_hig[j]+1, None ),:] if (j in it) else ttc[:,slice(0,ord_low[j]+1,None),:]) for j,ttc in enumerate(TTf_hig.TT) ]
            subt = TT.TTvec(cores)
            subt.build()

            subtens.append( subt )
            err.append(mla.norm(subt,'fro'))

        idxs_dim.append(idxs)
        err_dim.append(err)
        subtens_dim.append( subtens )

    # L2 error by LHC
    xx = np.asarray( DIST.lhc(N_LHC) )
    TTvals = STTapprox_hig(xx)
    fvals = fun(xx,None)
    l2err_notnorm = np.sqrt( vol * np.mean( (np.asarray(fvals)-np.asarray(TTvals))**2. ) )
    l2_norm = np.sqrt( vol * np.mean( np.asarray(fvals)**2. ) )
    l2err_list.append( l2err_notnorm / l2_norm )

    # Second order errors
    print "LOW:  %d/%d [used/tot], ranks %s" % (STTapprox_low.TW.get_fill_level(), STTapprox_low.TW.get_global_size(), str(STTapprox_low.TTapprox[0].ranks()))
    print "HIGH: %d/%d [used/tot], ranks %s" % (STTapprox_hig.TW.get_fill_level(), STTapprox_hig.TW.get_global_size(), str(STTapprox_hig.TTapprox[0].ranks()))
    print "Core error: %e" % err_dim[0][0]
    err2nd = np.zeros(d)
    for i in xrange(d):
        for it,err in zip(itertools.chain(*idxs_dim),itertools.chain(*err_dim)):
            if i in it:
                err2nd[i] += err

        print "d=%d, err=%e" % (i,err2nd[i])
    tot_err = sum( itertools.chain(*err_dim) )
    print "Tot. error: %e" % tot_err
    print "L2   error: %e" % l2err_list[-1]
    # Chose refinement directions
    # # Mean refinement
    # lmean = np.mean(np.log10(err2nd))
    # (ref_dir, ) = np.where( np.log10(err2nd) >= lmean - alpha * abs(lmean) )
    # Cut-off
    emx = np.log10(np.max(err2nd))
    emn = np.log10(np.min(err2nd))
    (ref_dir, ) = np.where( np.log10(err2nd) >= emx - alpha * abs(emx-emn) - 10. * np.spacing(1) ) # The last term to ensure isotropicity for alpha=1
    print "Refinement directions: %s" % str(ref_dir)
    print ""

    # STORE
    ord_low_list.append( copy.deepcopy( ord_low ))
    ord_hig_list.append( copy.deepcopy( ord_hig ))
    err_dir_list.append( err2nd.copy() )
    tot_err_list.append( tot_err )
    feval_low_list.append( STTapprox_low.TW.get_fill_level() )
    size_low_list.append( STTapprox_low.TW.get_global_size() )
    ranks_low_list.append( STTapprox_low.TTapprox[0].ranks() )
    feval_hig_list.append( STTapprox_hig.TW.get_fill_level() )
    size_hig_list.append( STTapprox_hig.TW.get_global_size() )
    ranks_hig_list.append( STTapprox_hig.TTapprox[0].ranks() )


# PLOTTING
markers = ['o','v','^','s','*','D']
lines = ['o-','v-','^-','s-','*-','D-']

err_dir_list = np.asarray( err_dir_list )
plt.figure(figsize=(6,5))
for i in xrange(d):
    plt.semilogy( err_dir_list[:,i], 'k'+lines[i], label='d=%d'%(i+1) )
plt.legend(loc='best')
plt.grid()
plt.xlabel('Iteration')
plt.ylabel('Error')
plt.tight_layout()
plt.show(False)
if STORE_FIG:
    for ff in FORMATS:
        plt.savefig(STORE_LOCATION + title + '-ErrorVsIter.'+ff,format=ff)

ord_hig_list = np.asarray( ord_hig_list )
plt.figure(figsize=(6,5))
for i in xrange(d):
    plt.plot( ord_hig_list[:,i], 'k' + markers[i], label='d=%d'%(i+1) )
plt.legend(loc='best')
plt.grid()
plt.xlabel('Iteration')
plt.ylabel('Order')
plt.tight_layout()
plt.show(False)
if STORE_FIG:
    for ff in FORMATS:
        plt.savefig(STORE_LOCATION + title + '-OrderVsIter.'+ff,format=ff)

plt.figure(figsize=(6,5))
# plt.loglog(feval_low_list, tot_err_list, 'ko-', label='Low')
# plt.loglog(feval_hig_list, tot_err_list, 'k^-', label='High')
plt.loglog(feval_hig_list, l2err_list, 'ks-')
plt.legend(loc='best')
plt.grid()
plt.xlabel('#f.eval.')
plt.ylabel('$L^2$ Error')
plt.tight_layout()
plt.show(False)
if STORE_FIG:
    for ff in FORMATS:
        plt.savefig(STORE_LOCATION + title + '-ErrorVsFeval.'+ff,format=ff)
