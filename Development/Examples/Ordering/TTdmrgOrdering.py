#
# This file is part of TensorToolbox.
#
# TensorToolbox is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TensorToolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TensorToolbox.  If not, see <http://www.gnu.org/licenses/>.
#
# DTU UQ Library
# Copyright (C) 2014-2015 The Technical University of Denmark
# Scientific Computing Section
# Department of Applied Mathematics and Computer Science
#
# Author: Daniele Bigoni
#

import sys

import operator
import time
import logging

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from scipy import stats
import scipy.cluster.hierarchy as cluster

import networkx as nx
import openopt as oo

import TensorToolbox as DT
import TensorToolbox.multilinalg as mla

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SpectralND as SND
from SpectralToolbox import Misc
from UQToolbox import UncertaintyQuantification as UQ
from UQToolbox import CutANOVA

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

npr.seed(1)

##########################################################
# TEST 0
#  Corner peak with interleaving dependencies
#
# TEST 1
#  Try function with given covariance (f4 in STT paper)
#  

logging.basicConfig(level=logging.INFO)

TESTS = [0]
IS_PLOTTING = True
STORE_FIG = True
FORMATS = ['pdf','png','eps']

MCestVarLimit = 1e-1
MCestMinIter = 100
MCestMaxIter = 1e4
MCstep = 10000

if 0 in TESTS:
    ##########################################################
    # TEST 0

    import itertools

    maxvoleps = 1e-4
    delta = 1e-4
    eps = 1e-10
    lr_maxit = 50

    xspan = [0.,1.]
    d = 20
    size1D = 7
    vol = (xspan[1]-xspan[0])**d

    def f(X,params):
        groups = params['groups']
        cs = params['cs']

        if isinstance(X,np.ndarray):
            ndim = X.ndim
            if ndim == 1:
                X = np.array([X,])
        else:
            shape = None

        out = 1.
        for g in groups:
            out *= (1. + np.dot( X[:,g], cs[g] ) ) ** (-len(g)+1) # Check correct indexing of cs
        
        if ndim == 1:
            return out[0]
        else:
            return out

    # Construct groups
    dim_list = range(d)
    dim_list_shuffle = dim_list[:]
    npr.shuffle(dim_list_shuffle)
    groups = []
    groups_shuffle = []
    d_left = d
    while d_left > 0:
        partition = npr.randint(1,min(10,d_left+1))
        groups.append( dim_list[d-d_left:d-d_left+partition] )
        groups_shuffle.append( dim_list_shuffle[d-d_left:d-d_left+partition] )
        d_left -= partition

    print "Groups:         %s" % str(groups)    
    print "Groups_shuffle: %s" % str(groups_shuffle)

    # Generate coefficients
    unif_dist = stats.uniform()
    cs_shuffle = unif_dist.rvs(d)
    cs = cs_shuffle[ list(itertools.chain( *groups_shuffle )) ]
    
    # Prepare grid
    X = [ (S1D.JACOBI,S1D.GAUSS,(0.,0.),[0.,1.]) ] * d
    
    # Build function wrapper (shuffled)
    params_shuffle = {'groups': groups_shuffle,
                      'cs'    : cs_shuffle }

    # Build function wrapper (reordered)
    params = {'groups': groups,
              'cs'    : cs }

    ###############################################
    # Test Surrogate construction on Ordered
    STTapprox = DT.SQTT(f, X, params, eps=eps, method='ttdmrg', 
                        surrogateONOFF=True, surrogate_type=DT.PROJECTION, orders=[size1D]*d)
    STTapprox.build()

    print "TTdmrg: grid size %d" % STTapprox.TW.get_size()
    print "TTdmrg: function evaluations %d" % STTapprox.TW.get_fill_level()

    # Check approximation error using MC
    var = 1.
    dist = stats.uniform()
    DIST = UQ.MultiDimDistribution([dist] * d)
    intf = []
    values = []
    while (len(values) < MCestMinIter or var > mean**2. * MCestVarLimit) and len(values) < MCestMaxIter:
        # Monte Carlo
        xx = DIST.rvs(MCstep)
(??)
(??)        VsI = None
(??)        VsI = [P.GradVandermonde1D(xx[:,i]*2.-1.,size1D,0,norm=True) for i in range(d)]
(??)        TTval = TT_four.interpolate(VsI)
(??)
(??)        TTvals = [ TTval[tuple([i]*d)] for i in range(MCstep) ]

        fval = f(xx,params)

        intf.extend( list(fval**2.) )
        values.extend( [(fval[i]-TTvals[i])**2. for i in range(MCstep)])
        mean = vol * np.mean(values)
        var = vol**2. * np.var(values) / len(values)

        sys.stdout.write("L2err estim. iter: %d Var: %e VarLim: %e \r" % (len(values) , var, mean**2. * MCestVarLimit))
        sys.stdout.flush()

    sys.stdout.write("\n")
    sys.stdout.flush()
    L2err = np.sqrt(mean/np.mean(intf))
    print "TTdmrg: L2 err TTapprox: %e" % L2err

    ###############################################
    # Test Surrogate construction on Shuffled
    STTapprox_shuffle = DT.SQTT(f, X, params_shuffle,eps=eps,method='ttdmrg',
                                surrogateONOFF=True, surrogate_type=DT.PROJECTION, orders=[size1D]*d)
    # STTapprox_shuffle.build()

    # print "TTdmrg_shuffle: grid size %d" % TW_shuffle.get_size()
    # print "TTdmrg_shuffle: function evaluations %d" % TW_shuffle.get_fill_level()

    # # Compute Fourier coefficients TT
    # TT_four_shuffle = TTapprox_shuffle.project(V,W)

    # # Check approximation error using MC
    # var = 1.
    # dist = stats.uniform()
    # DIST = UQ.MultiDimDistribution([dist] * d)
    # intf = []
    # values = []
    # while (len(values) < MCestMinIter or var > mean**2. * MCestVarLimit) and len(values) < MCestMaxIter:
    #     # Monte Carlo
    #     xx = DIST.rvs(MCstep)

    #     VsI = None
    #     VsI = [P.GradVandermonde1D(xx[:,i]*2.-1.,size1D,0,norm=True) for i in range(d)]
    #     TTval = TT_four_shuffle.interpolate(VsI)

    #     TTvals = [ TTval[tuple([i]*d)] for i in range(MCstep) ]

    #     fval = f(xx,params_shuffle)

    #     intf.extend( list(fval**2.) )
    #     values.extend( [(fval[i]-TTvals[i])**2. for i in range(MCstep)])
    #     mean = vol * np.mean(values)
    #     var = vol**2. * np.var(values) / len(values)

    #     sys.stdout.write("L2err estim. iter: %d Var: %e VarLim: %e \r" % (len(values) , var, mean**2. * MCestVarLimit))
    #     sys.stdout.flush()

    # sys.stdout.write("\n")
    # sys.stdout.flush()
    # L2err_shuffle = np.sqrt(mean/np.mean(intf))
    # print "TTdmrg_shuffle: L2 err TTapprox: %e" % L2err_shuffle

    
    # #####################################################
    # # Construct Cut-ANOVA-HDMR expansion ORD 2
    # CUT_ORDER = 2
    # Ns = [ 10 ] * d
    # polys = [ S1D.Poly1D(S1D.JACOBI,[0.,0.]) ] * d
    # tol = 2. * Misc.machineEpsilon()
    # X_cut = np.zeros((1,d))
    # cut_HDMR = CutANOVA.CutHDMR(polys,Ns,CUT_ORDER,X_cut,tol)
    # def transformFunc(X):
    #     # from [-1,1] to [0,1]
    #     return (X+1.)/2.
    # def f_hdmr(X):
    #     return f(X,params_shuffle)
    # cut_HDMR.evaluateFun(f_hdmr,transformFunc)
    # cut_HDMR.computeCutHDMR()
    # cut_HDMR.computeANOVA_HDMR()
    # # Fill correlation matrix
    # Sigma = np.zeros((d,d))
    # k = 0
    # minval = np.inf
    # maxval = 0.
    # for i in range(d-1):
    #     for j in range(i+1,d):
    #         Sigma[i,j] = np.dot( cut_HDMR.grids[2][k].ANOVA_HDMR_vals**2., cut_HDMR.grids[2][k].WF )
    #         if minval > Sigma[i,j]: minval = Sigma[i,j]
    #         if maxval < Sigma[i,j]: maxval = Sigma[i,j]
    #         k += 1
    # Sigma += Sigma.T
    # # Plot covariance matrix
    # plt.figure()
    # plt.imshow(Sigma, interpolation='none', origin='lower')
    # plt.colorbar()
    # plt.title('Estimated Covariance')
    # plt.show(block=False)
    # # Plot correlation network
    # import networkx as nx
    # plt.figure()
    # dt = [('weight', float)]
    # A = Sigma.view(dt)
    # G = nx.from_numpy_matrix(A)
    # pos=nx.circular_layout(G)
    # weights = [ G.get_edge_data(G.edges()[i][0],G.edges()[i][1])['weight'] for i in range(len(G.edges())) ]
    # nx.draw(G,pos,edge_color=weights,edge_cmap=cm.gray,edge_vmin=minval, edge_vmax=maxval, width=4)
    # plt.show(block=False) # display

    ##########################################################
    # Construct vicinity matrix
    TW_shuffle = STTapprox_shuffle.TW
    CutIdx = (size1D+1)//2
    
    Sigma = np.zeros((d,d))
    minval = np.inf
    maxval = 0.
    for i in range(d-1):
        for j in range(i+1,d):
            idxs = [ ( CutIdx if ((k != i) and (k != j)) else slice(None,None,None) ) \
                     for k in range(d) ]
            CutMat = TW_shuffle[ tuple(idxs) ]

            # Sigma[i,j] = npla.norm(CutMat,'fro')
            
            (u,s,v) = npla.svd(CutMat)
            eps_svd = 1e-10
            rank = (i for i,e in enumerate(s) if e < eps_svd * s[0]).next()
            Sigma[i,j] = float(rank)

            if minval > Sigma[i,j]: minval = Sigma[i,j]
            if maxval < Sigma[i,j]: maxval = Sigma[i,j]
    Sigma += Sigma.T
    # Plot matrix
    plt.figure(figsize=(6,5))
    plt.imshow(Sigma, interpolation='none', origin='lower')
    plt.colorbar()
    # plt.title('Frobenius norm')
    plt.tight_layout()
    plt.show(block=False)
    if STORE_FIG:
        for ff in FORMATS:
            plt.savefig('figs/VicinityMatrix.'+ff,format=ff)
    # Plot correlation network
    plt.figure(figsize=(6,5))
    dt = [('weight', float)]
    sigtmp = Sigma.copy()
    np.fill_diagonal(sigtmp,np.inf)
    A = ( np.ones(Sigma.shape) / (sigtmp/maxval) ).view(dt)
    G = nx.from_numpy_matrix(A)
    pos=nx.circular_layout(G)
    weights = [ G.get_edge_data(G.edges()[i][0],G.edges()[i][1])['weight'] for i in range(len(G.edges())) ]
    nx.draw(G,pos,edge_color=weights,edge_cmap=cm.gray, width=2)
    plt.show(block=False) # display
    if STORE_FIG:
        for ff in FORMATS:
            plt.savefig('figs/ConnectivityNetwork.'+ff,format=ff)

    # # Solve Traveling Sales Man problem on the network 
    # # (Need to install openopt, FuncDesigner, cvxopt, [glpk, python-glpk])
    # problem = oo.TSP(G, objective='weight')
    # tsp_sol = problem.solve('glpk')

    # tspG = nx.Graph()
    # tspG.add_edges_from(tsp_sol.Edges)
    # tspW = [ tsp_sol.Edges[i][2]['weight'] for i in range(len(tsp_sol.Edges)) ]
    # plt.figure()
    # nx.draw(tspG,pos,edge_color=tspW,edge_cmap=cm.gray, width=2)
    # plt.show(block=False) # display
    # # Determine the reordering: Leave out the weakest connection (lowest frobenious norm correspond to max edge)
    # tspW = [ tsp_sol.Edges[i][2]['weight'] for i in range(len(tsp_sol.Edges)) ]
    # nodes = tsp_sol.nodes
    # max_edge = np.argmax(tspW)
    # shuffle = nodes[max_edge+1:] + nodes[1:max_edge+1]

    # Solve clustering problem
    tmp = np.ones(Sigma.shape) / (sigtmp/maxval)
    cls = cluster.linkage(tmp, method='complete')
    plt.figure(figsize=(6,5))
    cluster.dendrogram(cls)
    plt.tight_layout()
    plt.show(block=False)
    if STORE_FIG:
        for ff in FORMATS:
            plt.savefig('figs/Clustering.'+ff,format=ff)
    cls_groups = cluster.fcluster(cls,0.5*np.max(tmp),'distance')
    cls_idx_groups = [[] for i in range(np.max(cls_groups)) ]
    for i,c in enumerate(cls_groups): cls_idx_groups[c-1].append(i)
    shuffle = list(itertools.chain( *cls_idx_groups ))

    # Print shuffling
    print "groups_shuffle:   %s" % (str(groups_shuffle))
    print "dim_list_shuffle: %s" % (str(dim_list_shuffle))
    print "cluster_groups:   %s" % (str(cls_idx_groups))
    print "shuffle:          %s" % (str(shuffle))
    
    # Define the new Tensor Wrapper for the unshuffled function
    # unsfl_idxs = np.argsort(dim_list_shuffle) # To be changed with the shuffle
    unsfl_idxs = np.argsort(shuffle)
    X_usfl = [ np.arange(size1D+1,dtype=int) for i in range(d) ]
    def f_usfl(X,params):
        usfl = params['unshuffle']
        return f( X[usfl], params_shuffle )
    params_usfl = {'unshuffle': unsfl_idxs}
    TW_usfl = DT.TensorWrapper(f_usfl, X_usfl, params_usfl, twtype='view')
    
    STTapprox_unshuffle = DT.SQTT(f_usfl, X, params_usfl, eps=eps,method='ttdmrg',
                                  surrogateONOFF=True, surrogate_type=DT.PROJECTION, orders=[size1D]*d)
    STTapprox_unshuffle.build()
    
    print "TTdmrg_unshuffle: grid size %d" % TW_shuffle.get_size()
    print "TTdmrg_unshuffle: function evaluations %d" % TW_shuffle.get_fill_level()

    # Check approximation error using MC
    var = 1.
    dist = stats.uniform()
    DIST = UQ.MultiDimDistribution([dist] * d)
    intf = []
    values = []
    while (len(values) < MCestMinIter or var > mean**2. * MCestVarLimit) and len(values) < MCestMaxIter:
        # Monte Carlo
        xx = DIST.rvs(MCstep)

        TTvals = STTapprox_unshuffle(xx[:,shuffle])
        
        fval = f(xx,params_shuffle)

        intf.extend( list(fval**2.) )
        values.extend( [(fval[i]-TTvals[i])**2. for i in range(MCstep)])
        mean = vol * np.mean(values)
        var = vol**2. * np.var(values) / len(values)

        sys.stdout.write("L2err estim. iter: %d Var: %e VarLim: %e \r" % (len(values) , var, mean**2. * MCestVarLimit))
        sys.stdout.flush()

    sys.stdout.write("\n")
    sys.stdout.flush()
    L2err_shuffle = np.sqrt(mean/np.mean(intf))
    print "TTdmrg_unshuffle: L2 err TTapprox: %e" % L2err_shuffle

if 1 in TESTS:
    ##########################################################
    # TEST 1

    import itertools

    maxvoleps = 1e-10
    delta = 1e-10
    eps = 1e-13

    xspan = [-1.,1.]
    d = 6
    size1D = 10
    maxordF = 10
    vol = (xspan[1]-xspan[0])**d

    # Construct Sigma

    # Uniform
    # sigma_dist = stats.uniform()
    # Sigma = sigma_dist.rvs((d,d)) - 0.5

    # Beta 0.1,0.1 to push extreme vals
    # sigma_dist = stats.beta(0.5,0.5)
    # sigma_dist = stats.uniform(loc=-1,scale=2)
    # Sigma = np.zeros((d,d))
    # for i in range(d-1): Sigma[i,i+1:] = sigma_dist.rvs( d-1-i )
    # Sigma += Sigma.T
    # Sigma += np.eye(d) # * float(d)

    # sigma_dist = stats.beta(0.5,0.5)
    # Sigma1 = (sigma_dist.rvs((d,d))) * 2. - 1.
    # Sigma2 = (sigma_dist.rvs((d,d))) * 2. - 1.
    # Sigma = (Sigma1 + Sigma2)/2.
    # Sigma += np.eye(d) * float(d)

    sigma_dist = stats.uniform(loc=-1,scale=2)
    Sigma = sigma_dist.rvs((d,d))
    Sigma += Sigma.T
    Sigma += np.eye(d) * float(d)

    # Normalize maximum eigenvalue (direction of maximum decay)
    (val,vec) = npla.eig(Sigma)
    # w = w * stats.beta(0.5,0.5).rvs(len(w))
    val = (val / np.max(val)) # * stats.beta(0.01,0.01).rvs(len(w))
    Sigma = np.dot(vec,np.dot(np.diag(val),vec.T))

    # Sigma /= 3

    print Sigma
    
    # Plot Covariance Matrix
    sigtmp = Sigma.copy()
    np.fill_diagonal(sigtmp,0.)
    plt.figure()
    plt.imshow(np.abs(sigtmp), interpolation='none')
    plt.colorbar()
    plt.title('Exact Covariance')
    plt.show(block=False)

    # Plot network graph
    import networkx as nx
    import string
    plt.figure()
    dt = [('weight', float)]
    A = np.abs(sigtmp) / np.max(np.abs(sigtmp))
    A = A.view(dt)
    G = nx.from_numpy_matrix(A)
    pos=nx.circular_layout(G)
    weights = [ G.get_edge_data(G.edges()[i][0],G.edges()[i][1])['weight'] for i in range(len(G.edges())) ]
    nx.draw(G,pos,edge_color=weights,edge_cmap=cm.gray,edge_vmin=0., edge_vmax=1., width=4)
    plt.show(block=False) # display

    # Using all idxs \leq maxordF
    ls_tmp = [ range(maxordF+1) for i in range(d) ]
    idxs = list(itertools.product( *ls_tmp ))

    coeffs = np.array([ np.exp(- np.dot( np.array(idx), np.dot(Sigma,np.array(idx))) ) for idx in idxs ])

    names = ['Exp f4']

    P = S1D.Poly1D(S1D.JACOBI,[0.,0.])
    (x,w) = P.Quadrature(size1D)
    VV = [ P.GradVandermonde1D(x,size1D,0,norm=True)] * d
    Gammas = [ np.diag(np.dot(VV[i],VV[i].T)) for i in range(d) ]

    def f(X,params):        
        V = np.array([1.])
        for i in range(d):
            (idx,) = np.where(x == X[i])
            V = np.kron( V, params['VV'][i][idx,:] )
        normFact = np.sum( [np.dot( w, params['VV'][i][:,0]) for i in range(d) ] )
        out = np.dot( params['coeffs'], V.flatten() ) / normFact
        return out

    # Used to compute MC
    def fMC(X,params): 
        if isinstance(X,np.ndarray):
            out = np.zeros(X.shape[0])
        else:
            out = 0.
        for idxs,coeff in zip(params['idxs'],params['coeffs']):
            if coeff > 1e-13:
                tmp = 1.
                for i,ii in enumerate(idxs):
                    tmp *= params['VV'][i][:,ii] 

                out += coeff * tmp / ( d * np.dot( w, VV[0][:,0]))
        return out

    X = [x for i in range(d)]
    W = [w for i in range(d)]
    V_func = [ P.GradVandermonde1D(x,maxordF,0,norm=True)] * d
    params = { 'idxs': list(idxs),
               'coeffs': coeffs,
               'VV': V_func,
               'Gammas': Gammas}
    TW = DT.TensorWrapper( f, X, params )
    
    TTapprox = DT.QTTvec(TW)
    TTapprox.build(eps=eps,method='ttdmrg')

    print "TTdmrg: grid size %d" % TW.get_size()
    print "TTdmrg: function evaluations %d" % TW.get_fill_level()

    # Compute Fourier coefficients TT
    Vs = [P.GradVandermonde1D(X[i],size1D,0,norm=True)] * d
    TT_four = TTapprox.project(Vs,W)

    # Check approximation error using MC
    MCestVarLimit = 1e-1
    MCestMinIter = 100
    MCestMaxIter = 1e4
    MCstep = 10000
    var = 1.
    dist = stats.uniform()
    DIST = UQ.MultiDimDistribution([dist] * d)
    intf = []
    values = []
    while (len(values) < MCestMinIter or var > mean**2. * MCestVarLimit) and len(values) < MCestMaxIter:
        # Monte Carlo
        xx = DIST.rvs(MCstep)

        VsI = None
        VsI = [P.GradVandermonde1D(xx[:,i]*2.-1.,size1D,0,norm=True) for i in range(d)]
        TTval = TT_four.interpolate(VsI)

        TTvals = [ TTval[tuple([i]*d)] for i in range(MCstep) ]

        VsI_func = [P.GradVandermonde1D(xx[:,i]*2.-1.,maxordF,0,norm=True) for i in range(d)]
        paramsMC = { 'idxs': list(idxs),
                     'coeffs': coeffs,
                     'VV': VsI_func,
                     'Gammas': Gammas}
        fval = fMC(xx,paramsMC)

        intf.extend( list(fval**2.) )
        values.extend( [(fval[i]-TTvals[i])**2. for i in range(MCstep)])
        mean = vol * np.mean(values)
        var = vol**2. * np.var(values) / len(values)

        sys.stdout.write("L2err estim. iter: %d Var: %e VarLim: %e \r" % (len(values) , var, mean**2. * MCestVarLimit))
        sys.stdout.flush()

    sys.stdout.write("\n")
    sys.stdout.flush()
    L2err = np.sqrt(mean/np.mean(intf))
    print "TTdmrg: L2 err TTapprox: %e" % L2err

    # Error of the TT Fourier coeff., with respect to their exact value
    idx_slice = []
    for i in range(d):
        if i in nzdim:
            idx_slice.append( slice(None,None,None) )
        else:
            idx_slice.append( 0 )
    idx_slice = tuple(idx_slice)
    four_approx = TT_four[ idx_slice ]
    print "TTdmrg: L2 norm error TT_four: %e" % npla.norm( four_approx.flatten() - np.asarray(coeffs) ,2)
    print "TTdmrg: Inf norm error TT_four: %e" % npla.norm( four_approx.flatten() - np.asarray(coeffs) , np.inf)

    if size1D**d < 1e4:
        # Construct full tensor
        full_tens = TW.copy()[ tuple([slice(None,None,None) for i in range(d)]) ]

        print "TTdmrg: Frobenius err.: %e" % (npla.norm( (TTapprox.to_tensor()-full_tens).flatten()) / npla.norm(full_tens.flatten()))
        
        TTapproxSVD = DT.TTvec(full_tens)

        # Compute Fourier coefficients TT
        Vs = [P.GradVandermonde1D(X[i],size1D,0,norm=True)] * d
        TT_fourSVD = TTapproxSVD.project(Vs,W)

        four_tensSVD = np.zeros( tuple( [size1D+1 for i in range(len(nzdim))] ) )
        import itertools
        ls_tmp = [ range(size1D+1) for i in range(len(nzdim)) ]
        idx_tmp = itertools.product( *ls_tmp )
        for ii  in idx_tmp:
            ii_tt = [0]*d
            for jj, tti in enumerate(nzdim): ii_tt[tti] = ii[jj]
            ii_tt = tuple(ii_tt)
            four_tensSVD[ii] = TT_fourSVD[ii_tt]
        
        print "TT-SVD: ranks: %s" % str( TTapproxSVD.ranks() )
        print "TT-SVD: Frobenius norm TT_four:  %e" % mla.norm(TT_fourSVD,'fro')
        print "TT-SVD: Frobenius norm sub_tens: %e" % npla.norm(four_tensSVD.flatten())

    VMAX = 0.
    VMIN = np.inf
    if size1D**d < 1e4 and IS_PLOTTING and len(nzdim) == 2:
        # Plot 2D Fourier Coeff
        TTsvd_fourier_abs = np.maximum(np.abs(four_tensSVD),1e-20*np.ones(four_tens.shape))
        VMAX = np.max(np.log10(TTsvd_fourier_abs))
        VMIN = np.min(np.log10(TTsvd_fourier_abs))

    if IS_PLOTTING and len(nzdim) == 2:
        # Plot 2D Fourier Coeff
        TTdmrg_fourier_abs = np.maximum(np.abs(four_tens),1e-20*np.ones(four_tens.shape))
        VMAX = max(VMAX, np.max(np.log10(TTdmrg_fourier_abs)))
        VMIN = min(VMIN, np.min(np.log10(TTdmrg_fourier_abs)))

    if IS_PLOTTING and d == 2:
        # Plot 2D Fourier Coeff from TensorToolbox.product
        V2D = np.kron(Vs[0],Vs[1])
        WW = np.kron(W[0],W[1])
        fhat = np.dot(V2D.T,WW*TW.copy()[:,:].flatten()).reshape([size1D+1 for s in range(d)])
        fhat_abs = np.maximum(np.abs(fhat),1e-20*np.ones(fhat.shape))
        VMAX = max(VMAX, np.max(np.log10(fhat_abs)))
        VMIN = min(VMIN, np.min(np.log10(fhat_abs)))

    if size1D**d < 1e4 and IS_PLOTTING and len(nzdim) == 2:
        # Plot 2D Fourier Coeff
        fig = plt.figure(figsize=fsize)
        plt.imshow(np.log10(TTsvd_fourier_abs), interpolation='none', vmin = VMIN, vmax = VMAX, origin='lower')
        plt.colorbar()
        plt.title('TT-SVD Fourier - %s' % names[COEFF])

    if IS_PLOTTING and len(nzdim) == 2:
        # Plot 2D Fourier Coeff
        fig = plt.figure(figsize=fsize)
        plt.imshow(np.log10(TTdmrg_fourier_abs), interpolation='none', vmin = VMIN, vmax = VMAX, origin='lower')
        plt.colorbar()
        plt.title('TT-dmrg Fourier - %s' % names[COEFF])

    if IS_PLOTTING and d == 2:
        # Plot 2D Fourier Coeff from TensorToolbox.product
        fig = plt.figure(figsize=fsize)
        plt.imshow(np.log10(fhat_abs), interpolation='none', vmin = VMIN, vmax = VMAX, origin='lower')
        plt.colorbar()
        plt.title('Full Fourier - %s' % names[COEFF])

    if size1D**d < 1e4 and IS_PLOTTING and len(nzdim) == 2:
        # TT-svd - TT-dmrg
        fig = plt.figure(figsize=fsize)
        plt.imshow(np.log10(np.abs(TTsvd_fourier_abs - TTdmrg_fourier_abs)), interpolation='none', origin='lower')
        plt.colorbar()
        plt.title('(TT-svd - TT-dmrg) Fourier - %s' % names[COEFF])

    if IS_PLOTTING and d == 2:    
        # Full - TT-dmrg
        fig = plt.figure(figsize=fsize)
        plt.imshow(np.log10(np.abs(fhat_abs - TTdmrg_fourier_abs)), interpolation='none', origin='lower')
        plt.colorbar()
        plt.title('(Full - TT-dmrg) Fourier - %s' % names[COEFF])
        
plt.show(block=False)
