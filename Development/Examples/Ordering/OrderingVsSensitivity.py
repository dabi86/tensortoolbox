import numpy as np
import numpy.linalg as npla
import scipy.special as scsp
import scipy.stats as stats

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SpectralND as SND
from SpectralToolbox import Misc

from UQToolbox import CutANOVA
import UQToolbox.RandomSampling as RS

import TensorToolbox as TT

plt.close('all')            

STORE_FIG = False
FORMATS = ['pdf','png','eps']

DIM = 3
def fun(X,params=None):
    if len(X.shape) == 1:
        Y = 10 * X[0]*X[1] + 10 * X[1]*X[2] + 1./(X[0]*X[2]+1.)
    elif len(X.shape) == 2:
        Y = 10 * X[:,0]*X[:,1] + 10 * X[:,1]*X[:,2] + 1./(X[:,0]*X[:,2]+1.)
    return Y

def transformFunc(X):
    # from [-1,1] to [0,1]
    return (X+1.)/2.

pp = S1D.Poly1D(S1D.JACOBI,[0.,0.])
polys = [pp for i in range(DIM)]
Ns = [6 for i in range(DIM)]

#############################################
# CUT HDMR
cut_order = 2
X_cut = np.zeros((1,len(polys)))
tol = 2. * Misc.machineEpsilon()
cut_HDMR = CutANOVA.CutHDMR(polys,Ns,cut_order,X_cut,tol)

print "N. eval = %d" % np.sum( [scsp.binom(DIM,i) * Ns[0]**i for i in range(cut_order+1)] )

# Evaluate f on the cutHDMR grid
cut_HDMR.evaluateFun(fun,transformFunc)

# Compute the cutHDMR
cut_HDMR.computeCutHDMR()

# Compute the ANOVA-HDMR
cut_HDMR.computeANOVA_HDMR()

# Compute an estimate for the total variance (using Monte Carlo)
dists = [stats.uniform(0,1) for i in xrange(DIM)]
exp_lhc = RS.Experiments(fun,None,dists,False)
exp_lhc.sample(2000, method='lhc')
exp_lhc.run()

MC_vals = np.asarray(exp_lhc.get_samples())
MC_exp = np.asarray(exp_lhc.get_results())

MC_mean = np.mean(MC_exp)
MC_var = np.var(MC_exp)

# # Mean and var
# plt.figure()
# plt.subplot(2,1,1)
# plt.plot(np.array([np.mean(MC_exp[:i]) for i in range(len(MC_exp))]))
# plt.subplot(2,1,2)
# inc_var = np.array([np.var(MC_exp[:i]) for i in range(len(MC_exp))])
# plt.plot(inc_var)

# # Check variance convergence
# var_conv = np.abs(np.diff(inc_var))/MC_var
# plt.figure(figsize=(6,5))
# plt.semilogy(var_conv)
# plt.grid()
# plt.show(False)

# # Plot vs. x1
# plt.figure(figsize=(6,5))
# plt.plot(MC_vals[:,0],MC_exp,'k.')
# plt.xlabel('$X_1$')
# plt.ylabel('$f(X)$')
# plt.tight_layout()
# plt.show(False)
# if STORE_FIG:
#     for ff in FORMATS:
#         plt.savefig('figs/Example-TSI-fvs1.'+ff,format=ff)

# # Plot vs. x2
# plt.figure(figsize=(6,5))
# plt.plot(MC_vals[:,1],MC_exp,'k.')
# plt.xlabel('$X_2$')
# plt.ylabel('$f(X)$')
# plt.tight_layout()
# plt.show(False)
# if STORE_FIG:
#     for ff in FORMATS:
#         plt.savefig('figs/Example-TSI-fvs2.'+ff,format=ff)

# # Plot vs. x3
# plt.figure(figsize=(6,5))
# plt.plot(MC_vals[:,2],MC_exp,'k.')
# plt.xlabel('$X_3$')
# plt.ylabel('$f(X)$')
# plt.tight_layout()
# plt.show(False)
# if STORE_FIG:
#     for ff in FORMATS:
#         plt.savefig('figs/Example-TSI-fvs3.'+ff,format=ff)

###
# Compute individual variances
###
D = []
for level_grids in cut_HDMR.grids:
    D_level = []
    for grid in level_grids:
        D_level.append( np.dot(grid.ANOVA_HDMR_vals**2., grid.WF) )
    D.append(D_level)

Var_ANOVA = 0.
for i in range(cut_order):
    Var_ANOVA += np.sum(D[i+1])

print "TotVar/Var_Anova = %f" % (Var_ANOVA/MC_var)

# Compute Total variances per component
TV = np.zeros(DIM)
for idx in range(DIM):
    for level, level_idxs in enumerate(cut_HDMR.idxs):
        for j, idxs in enumerate(level_idxs):
            if idx in idxs:
                TV[idx] += D[level][j]

TS = TV/MC_var

print "N     TV       TS        h_hat"
for i,grid in enumerate(cut_HDMR.grids[1]):
    # cut_hat = npla.solve(grid.V,grid.get_FX())
    # print "%2d    %.4f   %.4f   %s" % (i+1, TV[i], TS[i], np.array_str(cut_hat[1:]))
    print "%2d    %.4f   %.4f " % (i+1, TV[i], TS[i])

print "\nLaTeX TSI: " + " & ".join(["$%.3f$" % TS[i] for i in xrange(DIM)])

# plt.figure(figsize=(6,5))
# plt.pie(TS/np.sum(TS),labels=["$X_%d$"%(i+1) for i in range(DIM)])
# plt.tight_layout()
# plt.show(False)
# if STORE_FIG:
#     for ff in FORMATS:
#         plt.savefig('figs/Example-TSI-Pie.'+ff,format=ff)

import itertools
it = itertools.combinations( range(1,DIM+1), 2)
print "\nTWO-WAYS"
for i,(i1,i2) in enumerate(it):
    print "x%d,x%d: %f" % (i1,i2,np.dot(cut_HDMR.grids[2][i].ANOVA_HDMR_vals**2., cut_HDMR.grids[2][i].WF)/MC_var)

# plt.show(block=False)

#################################################
# TT construction

eps_list = 10.**-np.arange(1,8)
Nexp = len(eps_list)
feval_list = []
ranks_list = []

orders = [7] * DIM
X = [ (S1D.JACOBI, S1D.GAUSSLOBATTO, (0.,0.), [0.,1.]) ] * DIM

for i in range(Nexp):
    STTapprox = TT.SQTT(fun, X, None,eps=eps_list[i], orders=orders, method='ttdmrg',kickrank=1)
    STTapprox.build()
    
    feval_list.append( STTapprox.TW.get_fill_level() )
    ranks_list.append( STTapprox.TTapprox[0].ranks() )

# Re-order
def fun_reord(X, params=None):
    if len(X.shape) == 1:
        return fun(np.asarray([X[0],X[2],X[1]]))
    else:
        return fun( np.vstack( (X[:,0],X[:,2],X[:,1]) ).T )

feval_list_reord = []
ranks_list_reord = []

for i in range(Nexp):
    STTapprox = TT.SQTT(fun_reord, X, None,eps=eps_list[i], orders=orders, method='ttdmrg',kickrank=1)
    STTapprox.build()
    
    feval_list_reord.append( STTapprox.TW.get_fill_level() )
    ranks_list_reord.append( STTapprox.TTapprox[0].ranks() )

# Print LaTeX table
for i in range(Nexp):
    print "$%e$ & $%s$ & $%d$ & $%s$ & $%d$ \\\\" % (eps_list[i],ranks_list[i],feval_list[i],ranks_list_reord[i],feval_list_reord[i])
