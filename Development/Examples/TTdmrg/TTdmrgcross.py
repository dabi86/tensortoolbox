# This test case breaks. Figure out why...
# Somehow the problem is disappeared...
import logging
import sys
import operator
import time

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import numpy.linalg as npla
import numpy.random as npr
import itertools

from scipy import stats

import TensorToolbox as DT
import TensorToolbox.multilinalg as mla

# logging.basicConfig(level=logging.INFO)

PLOTTING = True

####
# Sin(sum(x)) TTcross Approximation
####
maxvoleps = 1e-5
delta = 1e-5
eps = 1e-10

d = 3
size = [20] * d

# Build up the tensor wrapper
# def f(X,params): return np.sin( X[0] ) * np.sin(X[1])
def f(X,params): return np.sin( np.sum(X) )
# def f(X,params): return 1./( np.sum(X) + 1 )
X = [np.linspace(0,2*np.pi,size[0])] * d
TW = DT.TensorWrapper(f,X)

TTapprox = DT.TTvec(TW, method='ttdmrgcross',eps=eps,mv_eps=maxvoleps)
fill = TW.get_fill_level()
crossRanks = TTapprox.ranks()
PassedRanks = all( map( operator.eq, crossRanks[1:-1], TTapprox.ranks()[1:-1] ) )

A = TW.copy()[tuple( [slice(None,None,None) for i in range(len(size))] ) ]
FroErr = mla.norm( TTapprox.to_tensor() - TW.copy()[tuple( [slice(None,None,None) for i in range(len(size))] ) ], 'fro')
kappa = np.max(A)/np.min(A) # This is slightly off with respect to the analysis
r = np.max(TTapprox.ranks())
epsTarget = (2.*r + kappa * r + 1.)**(np.log2(d)) * (r+1.) * eps
if FroErr < epsTarget:
    print '[OK] sin(sum(x)) Low Rank Approx (FroErr=%e, Fill=%.2f%%)' % (FroErr,100.*np.float(fill)/np.float(TW.get_size()))
else:
    print '[FAIL] sin(sum(x)) Low Rank Approx (FroErr=%e, Fill=%.2f%%)' % (FroErr,100.*np.float(fill)/np.float(TW.get_size()))

if PLOTTING and d == 3:
    # Get filled idxs
    fill_idxs = np.array(TW.get_fill_idxs())
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(fill_idxs[:,0],fill_idxs[:,1],fill_idxs[:,2])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # Get last used idxs
    last_idxs = TTapprox.get_ttdmrg_eval_idxs()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(last_idxs[:,0],last_idxs[:,1],last_idxs[:,2],c='r')

    plt.show(block=False)

