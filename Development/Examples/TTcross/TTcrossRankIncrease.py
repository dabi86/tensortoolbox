#
# This file is part of TensorToolbox.
#
# TensorToolbox is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TensorToolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TensorToolbox.  If not, see <http://www.gnu.org/licenses/>.
#
# DTU UQ Library
# Copyright (C) 2014-2015 The Technical University of Denmark
# Scientific Computing Section
# Department of Applied Mathematics and Computer Science
#
# Author: Daniele Bigoni
#

import operator
import time

import random

import numpy as np
import numpy.linalg as npla
import numpy.random as npr

from scipy import stats

import TensorToolbox as DT
import TensorToolbox.multilinalg as mla

from SpectralToolbox import Spectral1D as S1D
from SpectralToolbox import SpectralND as SND
from UQToolbox import UncertaintyQuantification as UQ

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# npr.seed(1)

IS_PLOTTING = True
N_Exp = 100

#########################################
# Genz functions
#########################################
maxvoleps = 1e-10
delta = 1e-10

size1D = 40
d = 2
size = [size1D for i in range(d)]
r = 2

ws = (npr.random(d)*2.)-1.
#ws = np.array([-0.5, -0.5])
cs = npr.random(d)

# # Oscillatory
# def f(X,params): return 10. * np.cos( 2.*np.pi * np.sum(X) )
# # def f(X,params): return np.cos(2.*np.pi*params['ws'][0] + np.sum( params['cs'] * X ))

# # Product peak
# def f(X,params): return np.prod( ( params['cs']**-2. + (X - params['ws'])**2. )**-1. )

# # Corner peak
# def f(X,params): return (1.+ np.sum(params['cs'] * (X+1.)/2.))**(-d-1.)

# # Gaussian
# # def f(X,params): return np.exp( - np.sum( 2. * X )**2. )
# def f(X,params): return np.exp( - np.sum( params['cs']**2. * (X - params['ws'])**2. ) )

# Continuous
def f(X,params): return np.exp( - np.sum( params['cs']**2. * np.abs(X - params['ws']) ) )

# # Discontinuous (not C^0)
# def f(X,params):
#     if np.sum(X + params['ws']) > 0.: return 0.
#     else: return 1.

# # C^0 function (not C^1)
# def f(X,params): return np.abs(np.sum(X+params['ws'][0]))

# # C^1 function (not C^2)
# def f(X,params):
#     if np.sum(X + params['ws']) > 0:
#         return np.sum(X+params['ws'])**2.
#     else:
#         return -np.sum(X+params['ws'])**2.

# # C^2 function (not C^3)
# def f(X,params):
#     if np.sum(X + params['ws']) > 0:
#         return np.sum(X+params['ws'])**3.
#     else:
#         return -np.sum(X+params['ws'])**3.

# # C^3 function (not C^4)
# def f(X,params):
#     if np.sum(X + params['ws']) > 0:
#         return np.sum(X+params['ws'])**4.
#     else:
#         return -np.sum(X+params['ws'])**4.

# # C^inf poly function
# def f(X,params): return np.sum(X+params['ws'])**6.

# Build up the 2d tensor wrapper
P = S1D.Poly1D(S1D.JACOBI,(0.,0.))
X = []
W = []
for i in range(d):
    [x,w] = P.GaussLobattoQuadrature(size[i],normed=True)
    X.append(x)
    W.append(w)

dims = [len(Xi) for Xi in X]
params = {'ws':ws,'cs':cs}
TW_tmp = DT.TensorWrapper(f,X,params)

if d <= 3:
    fr_norm = npla.norm(TW_tmp[tuple([ slice(None,None,None) for i in range(d)])].flatten(),2)
    def f1(X,params): return f(X,params)/fr_norm
    TW = DT.TensorWrapper(f1,X,params)
else:
    TW = TW_tmp


# Compute max possible ranks
MAXRANK = [1]
for i in range(d-1): MAXRANK.append( min( (min(size[i],size[i+1])+1)**(i+1),(min(size[i],size[i+1])+1)**(d-(i+1)) ) )
MAXRANK.append(1)

TTapprox = DT.TTvec(TW, method='ttcross',lr_delta=1e-5,mv_eps=1e-5)

# Compute TT cross starting already with the right rank
TW1 = DT.TensorWrapper(f,X,params)
TTapprox_direct = DT.TTvec(TW1, method='ttcross',lr_r=TTapprox.ranks())
fill_lev_direct = TW1.get_fill_level()
PassedRanks = True
roundRanks = TTapprox_direct.rounding(eps=delta).ranks()

print "Fill: Direct %d, Adaptive %d" % (TW1.get_fill_level(),TW.get_fill_level())
print "Total size: %d" % (size1D**d)
print "Rank: Direct %s, Adaptive %s" % (str(TTapprox_direct.ranks()), str(TTapprox.ranks()))

# # Compute Fourier coefficients TT
# TThat = []
# Vs = []
# for i in range(d):
#     V1D = P.GradVandermonde1D(X[i],size[i],0,norm=True)
#     Vs.append(V1D)
#     TTi = TTapprox.TT[i]
#     tth = np.zeros(TTi.shape)
#     for itt in range(TTi.shape[0]):
#         for jtt in range(TTi.shape[2]):
#             #tth[itt,:,jtt] = npla.solve(V1D,TTi[itt,:,jtt])
#             tth[itt,:,jtt] = np.dot(V1D.T,W[i]*TTi[itt,:,jtt])

#     TThat.append(tth)

# TT_four = DT.TTvec(TThat)

Vs = [P.GradVandermonde1D(X[i],size[i],0,norm=True)] * d
TT_four = TTapprox.project(Vs,W)

if TW.size <= 1e6 and TW.size > 0:
    full = TW.copy()[tuple([ slice(None,None,None) for i in range(d)])]
    print "Frobenious (Full - TT): %e" % npla.norm( (full-TTapprox.to_tensor()).flatten(),2)

if IS_PLOTTING and d == 2:
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    [XX,YY] = np.meshgrid(X[0],X[1])
    ax.plot_surface(XX,YY,full,rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    fig = plt.figure(figsize=(15,12))
    plt.subplot(2,2,1)
    plt.title("TTapprox")
    plt.imshow(TTapprox.to_tensor())
    plt.colorbar()
    plt.subplot(2,2,2)
    plt.title("Original")
    plt.imshow(full)
    plt.colorbar()
    plt.subplot(2,2,3)
    plt.title("Error")
    plt.imshow( full-TTapprox.to_tensor())
    plt.colorbar()

    fig = plt.figure(figsize=(6,4.5))
    plt.title("TTapprox")
    plt.imshow(TTapprox.to_tensor())
    plt.colorbar()
    fig = plt.figure(figsize=(6,4.5))
    plt.title("Original")
    plt.imshow(full)
    plt.colorbar()
    
    # Evaluation points
    fill_idxs = np.array(TW.get_fill_idxs())
    Is = TTapprox.ttcross_Is
    Js = TTapprox.ttcross_Js
    plt.figure(figsize=(6,4.5))
    plt.title("Evaluation points")
    plt.plot(fill_idxs[:,0],fill_idxs[:,1],'ob')
    idxs = []
    for k in range(len(Is)-1,-1,-1):
        for i in range(len(Is[k])):
            for j in range(len(Js[k])):
                for kk in range(dims[k]):
                    idxs.append( Is[k][i] + (kk,) + Js[k][j] )

    last_idxs = np.array(idxs)
    plt.plot(last_idxs[:,0],last_idxs[:,1],'or')

    # Plot 2D Fourier Coeff from tensor product
    V2D = np.kron(Vs[0],Vs[1])
    WW = np.kron(W[0],W[1])
    fhat = np.dot(V2D.T,WW*TW.copy()[:,:].flatten()).reshape([s+1 for s in size])

    # Plot 2D Fourier Coeff
    TT_fourier_abs = np.maximum(np.abs(TT_four.to_tensor()),1e-20*np.ones(TT_four.shape()))
    fhat_abs = np.maximum(np.abs(fhat),1e-20*np.ones(fhat.shape))
    # TT_fourier_abs *= fhat_abs[0,0]/TT_fourier_abs[0,0]
    VMAX = max( np.max(np.log10(TT_fourier_abs)), np.max(np.log10(fhat_abs)))
    VMIN = min( np.min(np.log10(TT_fourier_abs)), np.min(np.log10(fhat_abs)))
    fig = plt.figure(figsize=(15,12))
    plt.subplot(2,2,1)
    plt.title("TT Fourier")
    plt.imshow(np.log10(TT_fourier_abs), interpolation='none', vmin = VMIN, vmax = VMAX)
    plt.colorbar()
    plt.subplot(2,2,2)
    plt.title("Exact Fourier")
    plt.imshow(np.log10(fhat_abs), interpolation='none', vmin = VMIN, vmax = VMAX)
    plt.colorbar()
    plt.subplot(2,2,3)
    plt.title("Error")
    plt.imshow( np.log10( np.abs(TT_fourier_abs - fhat_abs)), interpolation = 'none')
    plt.colorbar()

plt.show(block=False)
