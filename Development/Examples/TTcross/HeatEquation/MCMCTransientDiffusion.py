# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"
Using a Spectral Tensor Train surrogate
"""

import sys
from os import getenv
from os import path
import numpy as np
import numpy.linalg as npla
from scipy import sparse
import scipy.sparse.linalg as spla
import scipy.linalg as scila
from scipy import integrate
from scipy import stats    

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from dolfin import *

import ExtPython

import DeterministicTransientDiffusion as DTD

import STT_TransientDiffusion as STT_TD

from UQToolbox import mcmc,RandomSampling

plt.close('all')

# Computation of posterior probability for unknown source location

print "Problem 6: Evaluation of posterior densities"
print "           STT expansion of source position"

(NE, mesh, V) = DTD.fenicsDiscretization()

# Storing parameters
BASE_filename = "%s" % ("./Heat_STT_data")
BASE_data_load = "%s" % ("./data-full")
BASE_ext = ".pkl"

(STTapprox, f, XX, YY) = STT_TD.STT_TD(BASE_filename, BASE_data_load)

# Compute perturbed solution
d_sig = 0.4
pert_dist = stats.norm(0,d_sig)

s_l = 10.0
sig_l = 0.1
x0 = 0.25
y0 = 0.75
T_l = 0.05

(NE, mesh, V) = DTD.fenicsDiscretization()
det_file = "Deterministic.pkl"
if not path.isfile(det_file):
    sol_dictionary = DTD.solve(s_l, sig_l, x0, y0, T_l, plotting=False)
    ExtPython.storeVariables(sol_dictionary, det_file)
sol_dictionary = ExtPython.loadVariables(det_file)
det_sol = np.asarray(sol_dictionary['solution_history'])
det_time = np.asarray(sol_dictionary['time_history'])

# Perturbing the solution with N(0,sigma^2). Note: here we use all the time and location observations
perturbations = pert_dist.rvs(np.prod(det_sol.shape)).reshape(det_sol.shape)
pert_sol = det_sol + perturbations

def interpolateCoeff(V,u,xy0):
    # Reshape list of interpolating points as a 2xN array
    if len(xy0.shape) == 1:
        xy0 = xy0.reshape((2,1))
    if xy0.shape[0] == 2:
        N_xy0 = xy0.shape[1]
    else:
        print "InterpolateCoeff: wrong shape passed"

    # Check number of layers in u
    if len(u.shape) == 1:
        u = u.reshape((1,u.shape[0]))
    if len(u.shape) == 2:
        N_u = u.shape[0]
        DOF = u.shape[1]
    else:
        print "InterpolateCoeff: wrong u shape passed"

    resp_surf_u_hat = np.zeros((N_u,N_xy0))
    tmp_vec_FE = Vector(DOF)
    # Iterate over coefficient levels
    for i in range(N_u):
        tmp_vec_FE.zero()
        tmp_vec_FE.add(u[i,:], np.arange(DOF,dtype='intc'))
        tmp_fun_FE = Function(V,tmp_vec_FE)
        # Interpolate over xy0 positions
        for j in range(N_xy0):
            resp_surf_u_hat[i,j] = tmp_fun_FE(xy0[:,j])
    return resp_surf_u_hat

# Define the likelihood function
def likelihood(t0,x0,y0,obs_idxs=None, obs_X=None, obs_Y=None):
    # x0, y0, obs_X, obs_Y 1d-array
    # 1) Generate surrogate solution at (t0=Tfinal,x0,y0) using STT
    surr = STTapprox(np.vstack((x0,y0)).T).flatten()
    if obs_idxs != None:
        surr = surr[:,obs_idxs]
    else:
        surr = interpolateCoeff(V,surr,np.vstack((obs_X,obs_Y)))

    # Find observed data at time t0
    obs_idx = np.where(np.abs(np.asarray(det_time) - t0) <= np.spacing(1))
    if len(obs_idx) == 0:
        print >> sys.stderr, "The requested time is not available for the deterministic solution"
        sys.exit()
    obs_sol = pert_sol[obs_idx[0][0]]
    # Interpolate observed data on sensor position or filter obs_idxs
    if obs_idxs != None:
        obs_sol = obs_sol[obs_idxs]
    else:
        obs_sol = interpolateCoeff(V,obs_sol,np.vstack((obs_X,obs_Y)))

    # 2) Compute likelihood
    ll = np.prod( pert_dist.pdf( np.tile(obs_sol,(x0.shape[0],1)) - surr), axis=1)

    return ll

# Define the logLikelihood function
def logLikelihood(t0,x0,y0,obs_idxs=None, obs_X=None, obs_Y=None):
    # x0, y0, obs_X, obs_Y 1d-array
    # 1) Generate surrogate solution at (t0,x0,y0) using gPC expansion
    surr = STTapprox(np.vstack((x0,y0)).T,verbose=False)
    if surr.ndim == 3:
        surr = surr.reshape((surr.shape[0]*surr.shape[1],surr.shape[2])).T
    elif surr.ndim == 2:
        surr = surr.reshape((1,surr.shape[0]*surr.shape[1]))

    if obs_idxs != None:
        surr = surr[:,obs_idxs]
    else:
        surr = interpolateCoeff(V,surr,np.vstack((obs_X,obs_Y)))

    # Find observed data at time t0
    obs_idx = np.where(np.abs(np.asarray(det_time) - t0) <= np.spacing(1))
    if len(obs_idx) == 0:
        print >> sys.stderr, "The requested time is not available for the deterministic solution"
        sys.exit()
    obs_sol = pert_sol[obs_idx[0][0]]
    # Interpolate observed data on sensor position or filter obs_idxs
    if obs_idxs != None:
        obs_sol = obs_sol[obs_idxs]
    else:
        obs_sol = interpolateCoeff(V,obs_sol,np.vstack((obs_X,obs_Y)))

    # 2) Compute logLikelihood
    # ll = np.sum( np.log( pert_dist.pdf(np.tile(obs_sol,(x0.shape[0],1))-gPC_surrogate) ), axis=1 )
    # ll = np.sum( np.log( pert_dist.pdf( np.tile(obs_sol[obs_idxs],(x0.shape[0],1)) - gPC_surrogate[:,obs_idxs]) ), axis=1)
    if type(x0) == np.float64 and type(y0) == np.float64:
        ll = np.sum( np.log( pert_dist.pdf( obs_sol - surr) ), axis=1)
    else:
        ll = np.sum( np.log( pert_dist.pdf( np.tile(obs_sol,(x0.shape[0],1)) - surr) ), axis=1)

    return ll

# Compute posterior at equidistant grid points (using boundary values)
X,Y = np.meshgrid(np.linspace(0.,1.,50), np.linspace(0.,1.,50))
# Get boundary values
COORD = mesh.coordinates()
obs_idxs = np.any( np.vstack( [np.abs(COORD[:,0])    <= np.spacing(1),
                               np.abs(COORD[:,0]-1.) <= np.spacing(1),
                               np.abs(COORD[:,1])    <= np.spacing(1),
                               np.abs(COORD[:,1]-1.) <= np.spacing(1)] ), 
                   axis=0 )

ll_t0 = 0.15
print "Computing LogLikelihood"
ll = logLikelihood(ll_t0,X.flatten(),Y.flatten(),obs_idxs=obs_idxs)
LL = ll.reshape(X.shape)

# Plot contour plot
plt.figure()
CS = plt.contour(X,Y,LL)
plt.clabel(CS, inline=1, fontsize=10)
plt.title("LogLikelihood - t = %.2f - obs: boundary" % ll_t0)
plt.show(block=False)

# Compute posterior at 3x3 grid at times t=0.05, t=0.15
X,Y = np.meshgrid(np.linspace(0.1,0.5,100), np.linspace(0.6,1.,100))
obs_X, obs_Y = np.meshgrid(np.linspace(0.,1.,3), np.linspace(0.,1.,3))

# # Likelihood
# L = 1.
# L_t0 = 0.05
# L = L * likelihood(L_t0, X.flatten(), Y.flatten(), obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())
# L_t0 = 0.15
# L = L * likelihood(L_t0, X.flatten(), Y.flatten(), obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())
# L = L.reshape(X.shape)
# Loglikelihood
L = 0.
# L_t0 = 0.05
# L = L + logLikelihood(L_t0, X.flatten(), Y.flatten(), obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())
L_t0 = 0.15
L = L + logLikelihood(L_t0, X.flatten(), Y.flatten(), obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())
L = L.reshape(X.shape)

# Plot contour plot
plt.figure()
CS = plt.contour(X,Y,L)
plt.clabel(CS, inline=1, fontsize=10)
plt.title("LogLikelihood - t = %.2f - obs: grid" % L_t0)
plt.show(block=False)

obs_X, obs_Y = np.meshgrid(np.linspace(0.,1.,3), np.linspace(0.,1.,3))

def target(P):
    # P is the set of parameters (in this case (x,y))
    ll = 0.0
    # t0 = 0.05
    # ll += logLikelihood(t0,P[0],P[1],obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())[0]
    t0 = 0.15
    ll += logLikelihood(t0,P[0],P[1],obs_X = obs_X.flatten(), obs_Y = obs_Y.flatten())[0]
    return ll

from scipy import stats
def q(xBase, xNew, sigma_q):
    return stats.norm.logpdf(xNew,loc=xBase,scale=sigma_q)

def rg(xBase, sigma_q, N):
    out = stats.norm(xBase,sigma_q).rvs(N)
    while out < 0. or out > 1.:
        out = stats.norm(xBase,sigma_q).rvs(N)
    return out

dists = [stats.uniform(loc=0.,scale=1.) for i in range(2)]
MultiDist = RandomSampling.MultiDimDistribution(dists)
lMC = 5000               # Length of Markov Chains
nMC = 2                 # Two Markov Chains for the two directions
sigma_q = 0.4           # Taken from the article
def propRG(xBase,N): return rg(xBase, sigma_q, N)          # Proposal random number generator
def propPDF(xBase,xNew): return q(xBase,xNew,sigma_q)      # Proposal PDF
X0 = MultiDist.rvs(1)
# MCMC_X = mcmc.MetropolisHastingsWithinGibbs(lMC,nMC,X0,target,propRG,propPDF)
MCMC_X = mcmc.MetropolisHastings(lMC,nMC,X0,target,propRG,propPDF)

plt.figure()
plt.plot(MCMC_X[:,0],MCMC_X[:,1],'-o')
plt.plot(MCMC_X[0,0],MCMC_X[0,1],'ro')
plt.xlim(0.,1.)
plt.ylim(0.,1.)
plt.show(block=False)
