# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

LHC analysis.
"""

from os import path
from os import getenv
import itertools
import numpy as np
from scipy import stats

from dolfin import *

from UQToolbox import UncertaintyQuantification as UQ

import ExtPython

import DeterministicTransientDiffusion as DTD

(NE,mesh,V) = DTD.fenicsDiscretization()

# Storing parameters
HOME_folder = getenv("HOME")
BASE_filename = "%s%s" % (HOME_folder,"/Documents/universita/phD/BigData/ProbabilisticInverseProblems/TransientDiffusion/LHC/results")
BASE_ext = ".pkl"

# Load list of solutions
listOfListSols = []
i = 0
while path.isfile("%s-%d%s" % (BASE_filename, i, BASE_ext)):
    tmpSols = ExtPython.loadVariables("%s-%d%s" % (BASE_filename, i, BASE_ext))
    listOfListSols.append(tmpSols)
    i += 1

listSols = list(itertools.chain(*listOfListSols))


# Plot one realization at specific time
def plotRealization(Nreal,t0,title=""):
    sol = listSols[Nreal]
    sol_time = sol['time_history']
    sol_sol = sol['solution_history']
    idx = np.where(np.abs(np.asarray(sol_time) - t0) <= np.spacing(1))[0][0]
    DTD.plotFE(sol_sol[idx],V,title=title)

Nreal = 15
t0 = 0.05
plotRealization(Nreal,t0,title="Nreal=%d,t0=%f" % (Nreal,t0))

# Compute mean and variance at specific time
def computeMeanVar(t0):
    solSum  = None
    solSum2 = None
    for sol in listSols:
        sol_time = sol['time_history']
        sol_sol = sol['solution_history']
        idx = np.where(np.abs(np.asarray(sol_time) - t0) <= np.spacing(1))[0][0]
        if solSum == None:
            solSum  = sol_sol[idx].copy()
            solSum2 = sol_sol[idx].copy()**2.
        else:
            solSum  += sol_sol[idx]
            solSum2 += sol_sol[idx]**2.
    
    meanSol = solSum/len(listSols)
    varSol  = solSum2/len(listSols) - meanSol**2.
    return (meanSol,varSol)

t0 = 0.05
(meanSol,varSol) = computeMeanVar(t0)
DTD.plotFE(meanSol,V,title="Mean")
DTD.plotFE(varSol,V,title="Variance")

# Compute histogram of distribution at specific time and point in space
def interpolateCoeff(V,u,xy0):
    DOF = interpolate(Expression("0.0"), V).vector().array().shape[0]
    tmp_vec_FE = Vector(DOF)
    tmp_vec_FE.zero()
    tmp_vec_FE.add(u, np.arange(DOF,dtype='intc'))
    tmp_fun_FE = Function(V,tmp_vec_FE)
    u_val = tmp_fun_FE(xy0)
    return u_val

def computeHist(t0,x0,y0):
    import matplotlib.pyplot as plt
    # Iterate over solutions at time t0 to get interpolated values
    u_vals = np.zeros(len(listSols))
    for i_sol, sol in enumerate(listSols):
        sol_time = sol['time_history']
        sol_sol = sol['solution_history']
        idx = np.where(np.abs(np.asarray(sol_time) - t0) <= np.spacing(1))[0][0]
        u_vals[i_sol] = interpolateCoeff(V,sol_sol[idx],np.array([x0,y0],dtype='float_'))
    plt.figure()
    plt.hist(u_vals,bins=20,normed=True)
    plt.show()
    return u_vals

t0 = 0.05
x0 = 0.0
y0 = 0.0
u_vals = computeHist(t0,x0,y0)
t0 = 0.15
u_vals = computeHist(t0,x0,y0)
