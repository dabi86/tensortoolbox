# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

TTcross surrogate generation
"""

from os import path
from os import getenv
import numpy as np
import numpy.linalg as npla
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import pickle as pkl

from dolfin import *

from SpectralToolbox import Spectral1D as S1D
from UQToolbox import UncertaintyQuantification as UQ

import TensorToolbox as TT

import ExtPython

import DeterministicTransientDiffusion as DTD

LOAD_DATA = True
LOAD_INIT = False

(NE,mesh,V) = DTD.fenicsDiscretization()

# Fixed parameters
s_l = 10.
sig_l = 0.1
T_l = 0.05

# Stochastic parameters
class Parameters:
    x0 = 0.5
    y0 = 0.5

params = Parameters()
dists = [stats.uniform() for i in range(2)]

# Storing parameters
HOME_folder = getenv("HOME")
BASE_filename = "%s%s" % (HOME_folder,"/Documents/universita/phD/LocalBigData/ProbabilisticInverseProblems/TransientDiffusion/TTcross/results")
BASE_data_load = "%s%s" % (HOME_folder,"/Documents/universita/phD/LocalBigData/ProbabilisticInverseProblems/TransientDiffusion/TTcross/data-full")
BASE_data_store = "%s%s" % (HOME_folder,"/Documents/universita/phD/LocalBigData/ProbabilisticInverseProblems/TransientDiffusion/TTcross/data")
BASE_ext = ".pkl"

if LOAD_DATA and path.isfile("%s%s" % (BASE_data_load,BASE_ext)):
    ff = open("%s%s" % (BASE_data_load,BASE_ext))
    dd = pkl.load(ff)
    ff.close()
    
    if LOAD_INIT: Jinit = dd['Jinit']
    else: Jinit = None
    X = dd['X']
    params = dd['params']
    data = dd['data']

    Nspace = len(X[0])-1
    Nparams = len(X[2])-1

    XX, YY = np.meshgrid(X[0],X[1])

    def f(X,params):
        # X contains the two dimensional coordinates and the two parameters for the source position
        dd = DTD.solve(s_l,sig_l,X[0],X[1], T_l, plotting=False)
        sol = dd['solution_history'][-1]
        # Interpolate solution on the spatial grid
        return interpolate(V,sol,XX,YY)

    tmpTW = TT.TensorWrapper(f, X, params, data=data, range_dim=2)

    def g(X2,params):
        return tmpTW[:,:,int(X2[0]),int(X2[1])]

    XspaceTmp = np.arange(Nspace+1,dtype=int)
    XparamsTmp = np.arange(Nparams+1,dtype=int)
    Xtmp = [XspaceTmp,XspaceTmp,XparamsTmp,XparamsTmp]
    TW = TT.TensorWrapper(g, Xtmp, params, range_dim=2)

else:
    # Discretization parameters
    Nspace = 20
    Nparams = 10

    # Construct Tensor Wrapper
    X = []
    W = []

    Pspace = S1D.Poly1D(S1D.JACOBI,(0.,0.))
    [x,w] = Pspace.GaussLobattoQuadrature(Nspace,normed=True)
    x = (x+1.)/2.
    for i in range(2):
        X.append(x)
        W.append(w)

    Pparams = S1D.Poly1D(S1D.JACOBI,(0.,0.))
    [x,w] = Pparams.GaussLobattoQuadrature(Nparams,normed=True)
    x = (x+1.)/2.
    for i in range(2):
        X.append(x)
        W.append(w)

    Jinit = None
    data = None
    params = None


    XX, YY = np.meshgrid(X[0],X[1])

    def interpolate(V,u,XX,YY):
        sol = np.zeros((Nspace+1,Nspace+1))
        # Load vector
        vec_FE = Vector(u.shape[0])
        vec_FE.add(u, np.arange(u.shape[0],dtype='intc'))
        # Construct FE function
        fun_FE = Function(V,vec_FE)

        for i in range(Nspace+1):
            for j in range(Nspace+1):
                sol[i,j] = fun_FE(np.array([XX[i,j],YY[i,j]]))

        return sol


    def f(X,params):
        # X contains the two dimensional coordinates and the two parameters for the source position
        dd = DTD.solve(s_l,sig_l,X[0],X[1], T_l, plotting=False)
        sol = dd['solution_history'][-1]
        # Interpolate solution on the spatial grid
        return interpolate(V,sol,XX,YY)

    params = None
    TW = TT.TensorWrapper(f, X, params, data=data, range_dim=2)

TTapprox = TT.TTvec(TW,method='ttcross', lr_delta=1e-2, lr_Jinit = Jinit, lr_store_location="%s%s" % (BASE_data_store,BASE_ext))

idx = [0,0]
TTsol = np.zeros((Nspace+1,Nspace+1))
for i in range(Nspace+1):
    for j in range(Nspace+1):
        TTsol[i,j] = TTapprox[i,j,idx[0],idx[1]]

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(XX, YY, TTsol, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
plt.title("TT")
plt.show(block=False)

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(XX, YY, TW[:,:,idx[0],idx[1]], rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
plt.title("TW")
plt.show(block=False)


FroErr = npla.norm(TTapprox.to_tensor().flatten()- TW[:,:,:,:].flatten(),2)/npla.norm(TW[:,:,:,:].flatten(),2)
print "Frobenious Error = %e" % FroErr
print "TT ranks: %s" % str(TTapprox.ranks())
print "Filling: %d/%d" % (TW.get_fill_level(),np.prod(TW.get_shape()[2:]))
