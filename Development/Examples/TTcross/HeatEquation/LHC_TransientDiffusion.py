# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

LHC driver to generate realizations.
"""

from os import path
from os import getenv
from numpy import * 
from scipy import stats

from mpi4py import MPI

from UQToolbox import UncertaintyQuantification as UQ

import ExtPython

import DeterministicTransientDiffusion as runner

# Fixed parameters
s_l = 10.
sig_l = 0.1
T_l = 0.05

# Stochastic parameters
class Parameters:
    x0 = 0.5
    y0 = 0.5

params = Parameters()
dists = [stats.uniform() for i in range(2)]

# Storing parameters
HOME_folder = getenv("HOME")
BASE_filename = "%s%s" % (HOME_folder,"/Documents/universita/phD/BigData/ProbabilisticInverseProblems/TransientDiffusion/LHC/results")
BASE_ext = ".pkl"

# LHC parameters
totN = 100

def paramUpdate(params,samples):
    params.x0 = samples[0]
    params.y0 = samples[1]
    return params

def computeRealization(params):
    return runner.solve(s_l,sig_l,params.x0, params.y0, T_l, plotting=False)

def action(sols,newSol):
    
    if not path.isfile("%s-%d%s" % (BASE_filename, i_file, BASE_ext)):
        listSol = []
    else:
        listSol = ExtPython.loadVariables("%s-%d%s" % (BASE_filename, i_file, BASE_ext))

    listSol.append(newSol)

    ExtPython.storeVariables(listSol,"%s-%d%s" % (BASE_filename, i_file, BASE_ext))
    
    return sols

# Aquire the lock for creating the file
with ExtPython.FileLock("MC.lck", timeout=2) as lock:
    i_file = 0
    while ( path.isfile("%s-%d%s" % (BASE_filename,i_file,BASE_ext)) ):
        i_file = i_file + 1
    # Create output file
    ExtPython.storeVariables([], "%s-%d%s" % (BASE_filename,i_file,BASE_ext))

(samples,sols) = UQ.LatinHyperCube(dists,totN,computeRealization,params,paramUpdate,action)
