# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

TTcross surrogate generation
"""

from os import path
from os import getenv
import numpy as np
import numpy.linalg as npla
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import pickle as pkl
import itertools

from dolfin import *

from SpectralToolbox import Spectral1D as S1D
from UQToolbox import UncertaintyQuantification as UQ

import TensorToolbox as TT

import ExtPython

import DeterministicTransientDiffusion as DTD

(NE,mesh,V) = DTD.fenicsDiscretization()

# Fixed parameters
s_l = 10.
sig_l = 0.1
T_l = 0.05

# Stochastic parameters
class Parameters:
    x0 = 0.5
    y0 = 0.5

params = Parameters()
dists = [stats.uniform() for i in range(2)]

# Storing parameters
HOME_folder = getenv("HOME")
BASE_filename = "%s%s" % (HOME_folder,"/Documents/universita/phD/LocalBigData/ProbabilisticInverseProblems/TransientDiffusion/TTcross/results_flat")
BASE_data_load = "%s%s" % (HOME_folder,"/Documents/universita/phD/LocalBigData/ProbabilisticInverseProblems/TransientDiffusion/TTcross/data-full")
BASE_ext = ".pkl"


ff = open("%s%s" % (BASE_data_load,BASE_ext))
dd = pkl.load(ff)
ff.close()

Jinit = None
X = dd['X']
params = dd['params']
data = dd['data']

Nspace = len(X[0])-1
Nparams = len(X[2])-1

XX, YY = np.meshgrid(X[0],X[1])

def f(X,params):
    pass

tmpTW = TT.TensorWrapper(f, X, params, data=data, range_dim=2)

def g(X2,params):
    return tmpTW[:,:,int(X2[0]),int(X2[1])].flatten()

XspaceTmp = np.arange( (Nspace+1)**2 ,dtype=int)
XparamsTmp = np.arange(Nparams+1,dtype=int)
Xtmp = [XspaceTmp,XparamsTmp,XparamsTmp]
TW = TT.TensorWrapper(g, Xtmp, params, range_dim=1)

TTapprox = TT.TTvec(TW,method='ttcross', lr_delta=1e-2, lr_Jinit = Jinit)

TWcopy = TW.copy()

idx = [0,0]
TTsol = np.zeros((Nspace+1)**2)
for i in range((Nspace+1)**2):
        TTsol[i] = TTapprox[i,idx[0],idx[1]]
TTsol = TTsol.reshape((Nspace+1,Nspace+1))

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(XX, YY, TTsol, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
plt.title("TT")
plt.show(block=False)

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(XX, YY, TWcopy[:,idx[0],idx[1]].reshape((Nspace+1,Nspace+1)), rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
plt.title("TW")
plt.show(block=False)


FroErr = npla.norm(TTapprox.to_tensor().flatten()- TWcopy[:,:,:].flatten(),2)/npla.norm(TWcopy[:,:,:].flatten(),2)
print "Frobenious Error = %e" % FroErr
print "TT ranks: %s" % str(TTapprox.ranks())
print "Filling: %d/%d" % (TW.get_fill_level(),np.prod(TW.get_shape()[1:]))
