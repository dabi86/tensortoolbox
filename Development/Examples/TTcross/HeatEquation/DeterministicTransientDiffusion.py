# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

Deterministic solver. To be called by sampling methods in order to get statistics to compare to.
"""

import sys
import numpy as np
import numpy.linalg as npla
from scipy import sparse
import scipy.sparse.linalg as spla
import scipy.linalg as scila
from scipy import integrate

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from dolfin import *

plt.close('all')

def plotFE(u,V,title=""):
    # Prepare vectors (back to FENICS)
    u_FE = Vector(u.shape[0])
    u_FE.add(u, np.arange(u.shape[0],dtype='intc'))
    # Prepare plotting
    u_fun_FE = Function(V,u_FE)
    plot(u_fun_FE,interactive=True,title=title)

def fenicsDiscretization():
    NE = 40
    mesh = UnitSquareMesh(NE,NE)
    V = FunctionSpace(mesh, "Lagrange", 1)    # Function space
    return (NE,mesh,V)

def solve(s_l, sig_l, x0, y0, T_l, plotting=False):
    ###
    # Deterministic time-dep problem with external time-stepping solver (Theta method)
    ###
    
    print "Deterministic time-dep problem with external adaptive time-stepping solver (Theta method)"

    parameters.linear_algebra_backend = "uBLAS"

    # Meshing, Function Space and Marking boudaries
    (NE,mesh,V) = fenicsDiscretization()
    boundary_parts = MeshFunction("uint", mesh, mesh.topology().dim()-1)

    class DirichletBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and abs(x[1]) < DOLFIN_EPS

    class NeumannBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary
            # return on_boundary and ( abs(x[0]) < DOLFIN_EPS or abs(x[1]-1.) < DOLFIN_EPS or abs(x[0]-1.) < DOLFIN_EPS)

    Gamma_D = DirichletBoundary()
    Gamma_D.mark(boundary_parts,0)
    Gamma_N = NeumannBoundary()
    Gamma_N.mark(boundary_parts,1)
    
    # Define Dirichlet Boundary Conditions
    u0 = Expression("0.0")
    bcs = []
    # bcs = [DirichletBC(V, u0, boundary_parts, 0)]
    
    # Initial condition
    u0 = Expression("0.0")
    u_1 = project(u0,V)
    
    # Define variational problem
    class ForcingExpression(Expression):
        t = None
        s_l = None
        sig_l = None
        x0 = None
        y0 = None
        T_l = None
        def __init__(self,t,s_l,sig_l,x0,y0,T_l):
            self.t = t
            self.s_l = s_l
            self.sig_l = sig_l
            self.x0 = x0
            self.y0 = y0
            self.T_l = T_l
        def eval(self, values, x):
            if self.t <= self.T_l:
                values[0] = self.s_l/(2.*np.pi*self.sig_l**2.) * exp( - 0.5 * (self.x0-x[0])**2./self.sig_l**2. - 0.5 * (self.y0-x[1])**2./self.sig_l**2.)
            else:
                values[0] = 0.0

    # Set up parts of the variational form
    u = TrialFunction(V)
    v = TestFunction(V)
    f0 = ForcingExpression(t=0.0,s_l=s_l,sig_l=sig_l,x0=x0,y0=y0,T_l=T_l)
    a_k = inner(nabla_grad(u), nabla_grad(v))*dx
    a_m = u*v*dx
    g = Expression("0.0")
    g_m = - g*v*ds(1)
    
    # Assemble once
    K_FE = assemble(a_k, exterior_facet_domains=boundary_parts)
    M_FE = assemble(a_m, exterior_facet_domains=boundary_parts)
    G_FE = assemble(g_m, exterior_facet_domains=boundary_parts)

    # Impose Boundary Conditions
    for bc in bcs: 
        bc.apply(M_FE)
        bc.zero(K_FE)

    # Get matrices
    K_idx, K_ptr, K_vals = K_FE.data()
    K = sparse.csr_matrix((K_vals,K_ptr,K_idx))
    M_idx, M_ptr, M_vals = M_FE.data()
    M = sparse.csr_matrix((M_vals,M_ptr,M_idx))
    
    # Set initial value
    u_init = u_1.vector().array()
    
    # Compute Solution
    T = 0.15
    dt = 0.001
    u = Function(V)

    # Use theta method to advance the solution
    # M is the mass matrix
    # K is the stiffness matrix    
    t = dt
    plt_dt = 10*dt
    plt_t = plt_dt
    theta = 0.5
    u = u_init

    # Prepare vectors (back to FENICS)
    u_FE = Vector(u.shape[0])
    u_FE.add(u, np.arange(u.shape[0],dtype='intc'))
    
    # Prepare rhs of variational problem
    f_k = interpolate(f0,V)
    F_k = f_k.vector()
    b_FE = M_FE*F_k + G_FE
    for bc in bcs: bc.apply(b_FE)
    b_old = b_FE.array()

    if plotting:
        # Prepare plotting
        u_fun_FE = Function(V,u_FE)
        plot(u_fun_FE,interactive=True)
    
    # Solve with variable time stepping for error control
    # The embedded method is the forward euler method
    SS_p = 1.
    SS_PI_kP  = 0.4/(SS_p+1.)
    SS_PI_kI  = 0.3/(SS_p+1.)
    SS_eps = 0.8
    SS_minStep = 1e-8
    r_new = 1.0
    abs_tol = 1e-3
    rel_tol = 1e-3
    t_inner = 0.0
    t_inner_old = t_inner
    dt_inner = dt
    dt_inner_old = dt_inner
    stepsize_history = []
    error_history = []
    time_history = [0.0]       # Taken at fixed time intervals
    solution_history = [u]     # Taken at fixed time intervals
    for t in np.arange(dt,T+dt,dt):
        
        while t_inner - t <= np.spacing(1):
            # Update u_old
            u_old = u.copy()

            # Update matrices
            R = M + dt_inner * theta * K
            S = M - dt_inner * (1-theta) * K

            # Prepare new b
            f0.t = t_inner+dt_inner
            f_k = interpolate(f0,V)
            F_k = f_k.vector()
            b_FE = M_FE*F_k + G_FE
            for bc in bcs: bc.apply(b_FE)
            b_new = b_FE.array()

            # Solve time step with Trapezoidal Method (theta = 0.5)
            rhs = S.dot(u_old) + dt_inner * theta * b_new + dt_inner * (1-theta) * b_old
            (u_trap,info) = spla.gmres(R,rhs)
            
            # Solve time step with Forward Euler method (theta = 0.0)
            rhs = S.dot(u_old) + dt_inner * b_old
            (u_fwd,info) = spla.gmres(M,rhs)

            # Compute estimated error
            r_old = r_new
            r_new = np.sqrt( np.sum( 1./u_fwd.shape[0] * ((u_trap - u_fwd)/(abs_tol + np.abs(u_fwd) * rel_tol))**2. ) )
            err_2 = npla.norm(u_trap-u_fwd,2)

            # Step size controller
            if r_new <= 1.:
                if r_new <= np.spacing(1):
                    print >> sys.stderr, "Zero error estimated. Relax tolerances."
                    exit()
                stepsize_history.append(dt_inner)
                error_history.append(err_2)
                # PI error controller (accepted)
                t_inner_old = t_inner
                t_inner += dt_inner
                dt_inner_old = dt_inner
                dt_inner *= (SS_eps/r_new)**SS_PI_kI * (r_old/r_new)**SS_PI_kP
                # Store old solution u and old forcing term b
                u = u_trap.copy()
                b_old = b_new.copy()
            else:
                # Asymptotic step size controller (rejected)
                dt_inner *= (SS_eps/r_new)**(SS_p+1.)
                if dt_inner < SS_minStep:
                    print >> sys.stderr, "Minimums step size limit reached"
                    exit()
        
        # Extrapolate (linear interpolation) result at time t from u_old and u
        u_extrap = (u-u_old)/(t_inner - t_inner_old) * (t - t_inner_old) + u_old

        # Update history lists
        time_history.append(t)
        solution_history.append(u_extrap.copy())
        
        # Update FEniCS vector
        u_FE.zero()
        u_FE.add(u_extrap, np.arange(u_extrap.shape[0],dtype='intc'))
        
        sys.stdout.write("Time: %f\r" % t)
        sys.stdout.flush()
        if plotting:
            if t >= plt_t:
                plot(u_fun_FE,interactive=True)
                plt_t += plt_dt

    sys.stdout.write("\n" % t)
    sys.stdout.flush()

    if plotting:
        plot(u_fun_FE,interactive=True)

    dictionary = {'time_history': time_history,
                  'solution_history': solution_history}
    return dictionary
