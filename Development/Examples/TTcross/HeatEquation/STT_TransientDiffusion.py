# -*- coding: utf-8 -*-
"""
Created on Mon May 13 09:39:53 2013

@author: dabi

Example from: Marzouk et al. "Stochastic spectral methods for efficient Bayesian solution of inverse problems"

TTcross surrogate generation
"""

import time as systime
import os.path
from os import getenv
import numpy as np
import numpy.linalg as npla
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import pickle as pkl
import itertools

from dolfin import *

from SpectralToolbox import Spectral1D as S1D
from UQToolbox import UncertaintyQuantification as UQ
import TensorToolbox as TT

import ExtPython

import DeterministicTransientDiffusion as DTD

def STT_TD(BASE_filename, BASE_data_load):
    LOAD_DATA = False
    LOAD_FULL = True
    # surr_type = TT.PROJECTION
    # surr_type = TT.LINEAR_INTERPOLATION
    surr_type = TT.LAGRANGE_INTERPOLATION

    (NE,mesh,V) = DTD.fenicsDiscretization()

    # Fixed parameters
    s_l = 10.
    sig_l = 0.1
    T_l = 0.05

    # Stochastic parameters
    class Parameters:
        x0 = 0.5
        y0 = 0.5

    params = Parameters()
    dists = [stats.uniform() for i in range(2)]

    # Storing parameters
    BASE_ext = ".pkl"

    if LOAD_DATA and os.path.isfile("%s%s" % (BASE_filename,BASE_ext)):
        # Load precomputed (even partial) STT
        ff = open("%s%s" % (BASE_filename,BASE_ext))
        STTapprox = pkl.load(ff)
        ff.close()

        # Load the TW data
        ff = open("%s%s" % (BASE_data_load,BASE_ext))
        dd = pkl.load(ff)
        ff.close()
        X = dd['X']
        params = dd['params']
        data = dd['data']
        
        Nspace = len(X[0])-1
        Nparams = len(X[2])-1
        
        XX, YY = np.meshgrid(X[0],X[1])
        
        tmpTW = TT.TensorWrapper(None, X, params, data=data, range_dim=2)
        
        def g(X2,params):
            return params['TW'][:,:,int(X2[0]),int(X2[1])]

        STTapprox.set_f(g,params)
        STTapprox.stt_store_freq = 20

        # Restart construction
        STTapprox.build()

        # Redefine the function
        X = [ STTapprox.Xs_space[0], STTapprox.Xs_space[1] ]
        Nspace = len(X[0])-1
        XX, YY = np.meshgrid(X[0],X[1])
        def interpolate(V,u,XX,YY):
            sol = np.zeros((Nspace+1,Nspace+1))
            # Load vector
            vec_FE = Vector(u.shape[0])
            vec_FE.add(u, np.arange(u.shape[0],dtype='intc'))
            # Construct FE function
            fun_FE = Function(V,vec_FE)

            for i in range(Nspace+1):
                for j in range(Nspace+1):
                    sol[i,j] = fun_FE(np.array([XX[i,j],YY[i,j]]))

            return sol

        def f(X,params):
            # X contains the two dimensional coordinates and the two parameters for the source position
            dd = DTD.solve(s_l,sig_l,X[0],X[1], T_l, plotting=False)
            sol = dd['solution_history'][-1]
            # Interpolate solution on the spatial grid
            return interpolate(V,sol,XX,YY)

        # Reset the function
        STTapprox.set_f(f)

        # Overwrite grids
        X = []
        # Space dimensions
        Pspace = S1D.Poly1D(S1D.JACOBI,(0.,0.))
        [x,w] = Pspace.GaussLobattoQuadrature(Nspace,normed=True)
        x = (x+1.)/2.
        for i in range(2):
            X.append(x)
        # Parameter dimensions
        orders = []
        for i in range(2):
            X.append( (S1D.JACOBI, S1D.GAUSSLOBATTO, (0.,0.), [0.,1.]) )
            orders.append( len(STTapprox.Xs_params[0,0][i])-1 )
        STTapprox.set_grids(X,orders)

        TT.object_store("%s%s" % (BASE_filename,BASE_ext),STTapprox)

        STTapprox.surr_type = surr_type

        # Ask for surrogate
        STTapprox.surrogateONOFF = True

        STTapprox.prepare_surrogate(force_redo=True)

        # Store STTapprox
        TT.object_store("%s%s" % (BASE_filename,BASE_ext),STTapprox)

    else:
        if LOAD_FULL and os.path.isfile("%s%s" % (BASE_data_load,BASE_ext)):
            # Load the TW data
            ff = open("%s%s" % (BASE_data_load,BASE_ext))
            dd = pkl.load(ff)
            ff.close()
            X = dd['X']
            params = dd['params']
            data = dd['data']

            Nspace = len(X[0])-1
            Nparams = len(X[2])-1

            XX, YY = np.meshgrid(X[0],X[1])

            def f(X,params):
                # X contains the two dimensional coordinates and the two parameters for the source position
                dd = DTD.solve(s_l,sig_l,X[0],X[1], T_l, plotting=False)
                sol = dd['solution_history'][-1]
                # Interpolate solution on the spatial grid
                return interpolate(V,sol,XX,YY)

            tmpTW = TT.TensorWrapper(f, X, params, data=data, range_dim=2)

            def g(X2,params):
                return tmpTW[:,:,int(X2[0]),int(X2[1])]

            XspaceTmp = np.arange(Nspace+1,dtype=int)
            XparamsTmp = np.arange(Nparams+1,dtype=int)
            Xtmp = [XspaceTmp,XspaceTmp,XparamsTmp,XparamsTmp]

            params = None
            STTapprox = TT.STT(g, Xtmp, params, range_dim=2, surrogateONOFF=False, stt_store_location="%s%s" % (BASE_filename,BASE_ext), stt_store_overwrite=True, stt_store_freq=0)

            # Overwrite grids
            X = []
            # Space dimensions
            Pspace = S1D.Poly1D(S1D.JACOBI,(0.,0.))
            [x,w] = Pspace.GaussLobattoQuadrature(Nspace,normed=True)
            x = (x+1.)/2.
            for i in range(2):
                X.append(x)
            # Parameter dimensions
            orders = []
            for i in range(2):
                X.append( (S1D.JACOBI, S1D.GAUSSLOBATTO, (0.,0.), [0.,1.]) )
                orders.append(Nparams)
            STTapprox.set_grids(X,orders)

            # Ask for surrogate
            STTapprox.surrogateONOFF = True
            STTapprox.surr_type = surr_type

            # Build surrogate
            STTapprox.build_projection_surrogate()

            # Store STTapprox
            TT.object_store("%s%s" % (BASE_filename,BASE_ext),STTapprox)
        else:
            # Recompute everything from scratch
            # Discretization parameters
            Nspace = 20
            Nparams = 10

            # Construct Tensor Wrapper
            X = []

            # Space dimensions
            Pspace = S1D.Poly1D(S1D.JACOBI,(0.,0.))
            [x,w] = Pspace.GaussLobattoQuadrature(Nspace,normed=True)
            x = (x+1.)/2.
            for i in range(2):
                X.append(x)

            # Parameter dimensions
            orders = []
            for i in range(2):
                X.append( (S1D.JACOBI, S1D.GAUSSLOBATTO, (0.,0.), [0.,1.]) )
                orders.append(Nparams)

            # Define the function
            XX, YY = np.meshgrid(X[0],X[1])
            def interpolate(V,u,XX,YY):
                sol = np.zeros((Nspace+1,Nspace+1))
                # Load vector
                vec_FE = Vector(u.shape[0])
                vec_FE.add(u, np.arange(u.shape[0],dtype='intc'))
                # Construct FE function
                fun_FE = Function(V,vec_FE)

                for i in range(Nspace+1):
                    for j in range(Nspace+1):
                        sol[i,j] = fun_FE(np.array([XX[i,j],YY[i,j]]))

                return sol

            def f(X,params):
                # X contains the two dimensional coordinates and the two parameters for the source position
                dd = DTD.solve(s_l,sig_l,X[0],X[1], T_l, plotting=False)
                sol = dd['solution_history'][-1]
                # Interpolate solution on the spatial grid
                return interpolate(V,sol,XX,YY)

            # Start construction
            params = None
            STTapprox = TT.STT(f, X, params, range_dim=2, orders=orders, stt_store_location="%s%s" % (BASE_filename,BASE_ext), stt_store_overwrite=True)
    
    return (STTapprox, f, XX, YY)

if __name__ == "__main__":
    BASE_filename = "%s" % ("./Heat_STT_data")
    BASE_data_load = "%s" % ("./data-full")
    (STTapprox, f, XX, YY) = STT_TD(BASE_filename, BASE_data_load)
    
    start_eval = systime.clock()
    val = STTapprox(np.array([0.2,0.2]))
    end_eval = systime.clock()
    print "Evaluation time: " + str(end_eval-start_eval)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(XX,YY,val)
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show(block=False)

    def plot_point(STTapprox,x,y,exact=False):
        start_eval = systime.clock()
        val = STTapprox(np.array([x,y]))
        end_eval = systime.clock()
        print "Surrogate evaluation time: " + str(end_eval-start_eval)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf = ax.plot_surface(XX,YY,val)
        # fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.title("Surrogate")
        plt.show(block=False)

        if exact:
            # Plot exact function
            start_eval = systime.clock()
            exact = f([x,y],None)
            end_eval = systime.clock()
            print "Exact evaluation time: " + str(end_eval-start_eval)
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            surf = ax.plot_surface(XX,YY,exact)
            # fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.title("Exact")
            plt.show(block=False)

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            surf = ax.plot_surface(XX,YY,np.abs(exact-val))
            # fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.title("Error")
            plt.show(block=False)

        return (val,exact)


    # Compute the integral
    start_eval = systime.clock()
    integral = STTapprox.integrate()
    end_eval = systime.clock()
    print "Integration time: " + str(end_eval-start_eval)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(XX,YY,integral)
    plt.title("Mean")
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show(block=False)

    # Compute variance
    start_eval = systime.clock()
    variance = (STTapprox**2).integrate() - (STTapprox.integrate() ** 2)
    end_eval = systime.clock()
    print "Variance time: " + str(end_eval-start_eval)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(XX,YY,variance)
    plt.title("Variance")
    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show(block=False)

