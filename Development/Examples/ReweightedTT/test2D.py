import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npla

import SpectralToolbox.Spectral1D as S1D
import TensorToolbox as TT

# Create a 2d discretization based on Gauss points for the Hermite polynomials
# and run the Quantics TTdmrg to approximate the constant function f=1
# w.r.t. the measure

STORE_FIG = True
FORMATS = ['pdf','png','eps']

N = 31 # Power of 2 for now
P = S1D.Poly1D(S1D.HERMITEP_PROB,None)
(x,w) = P.Quadrature(N)

W = np.outer(w,w)
def f(x): return 1.
# def f(x): return 1. + 1. * np.exp(-npla.norm(x-2.)/(2*1.**2.) )
# def f(x): return (x[0]-5.)**2. + (x[1]-5.)**2.


def fW(X,params): 
    return params['f'](X) * params['w'][np.where(x==X[0])] * params['w'][np.where(x==X[1])]

params = {'f': f,
          'x': x,
          'w': w}

grids = [(S1D.HERMITEP_PROB, S1D.GAUSS, None, (-np.inf,np.inf))] * 2
WTT = TT.SQTT(fW, grids, params, marshal_f=False,
              eps=1e-8,
              surrogateONOFF=True, surrogate_type=TT.PROJECTION, orders=[N]*2)
WTT.build()

FTT = TT.SQTT(fW, grids, params, marshal_f=False,
              eps=1e-8,
              surrogateONOFF=True, surrogate_type=TT.PROJECTION, orders=[N]*2)
FTT.build()

print "Fill level fW: %d/%d" % (WTT.TW.get_fill_level(), (N+1)**2)
print "Fill level f: %d/%d" % (FTT.TW.get_fill_level(), (N+1)**2)

fill_idxs = np.array(WTT.TW.get_fill_idxs())
plt.figure()
plt.scatter(x[fill_idxs[:,0]],x[fill_idxs[:,1]])
plt.title("Filled idxs - fW")
plt.show(block=False)

used_idxs = WTT.generic_approx[0].get_ttdmrg_eval_idxs()
plt.figure()
plt.scatter(x[used_idxs[:,0]],x[used_idxs[:,1]],c='r')
plt.title("Final idxs - fW")
plt.show(block=False)

fill_idxs = np.array(FTT.TW.get_fill_idxs())
plt.figure()
plt.scatter(x[fill_idxs[:,0]],x[fill_idxs[:,1]])
plt.title("Filled idxs - f")
plt.show(block=False)

used_idxs = FTT.generic_approx[0].get_ttdmrg_eval_idxs()
plt.figure()
plt.scatter(x[used_idxs[:,0]],x[used_idxs[:,1]],c='r')
plt.title("Final idxs - f")
plt.show(block=False)

F = np.zeros((N+1,N+1))
for i in range(N+1):
    for j in range(N+1):
        F[i,j] = f(np.array([x[i],x[j]]))
FW = np.zeros((N+1,N+1))
for i in range(N+1):
    for j in range(N+1):
        FW[i,j] = fW(np.array([x[i],x[j]]),params)

X,Y = np.meshgrid(x,x)

plt.figure()
plt.contourf(X,Y,F,30)
plt.title("f")
plt.colorbar()
plt.show(block=False)

plt.figure()
plt.contourf(X,Y,FW,30)
plt.title("fW")
plt.colorbar()
plt.show(block=False)

# plt.figure()
# plt.contourf(X,Y,np.log10(F))
# plt.title("log10(f)")
# plt.colorbar()
# plt.show(block=False)

# plt.figure()
# plt.contourf(X,Y,np.log10(FW))
# plt.title("log10(fW)")
# plt.colorbar()
# plt.show(block=False)
