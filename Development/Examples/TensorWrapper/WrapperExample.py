#
# This file is part of TensorToolbox.
#
# TensorToolbox is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TensorToolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TensorToolbox.  If not, see <http://www.gnu.org/licenses/>.
#
# DTU UQ Library
# Copyright (C) 2014-2015 The Technical University of Denmark
# Scientific Computing Section
# Department of Applied Mathematics and Computer Science
#
# Author: Daniele Bigoni
#

#
# Construct two nested grids to exemplify the capabilities of the
# TensorWrapper.
#

import numpy as np
import itertools
import matplotlib.pyplot as plt
import TensorToolbox as TT

d = 2
x_fine = np.linspace(-1,1,7)
params = {'k': 1.}

def f(X,params):
    return np.max(X) * params['k']

TW = TT.TensorWrapper( f, [ x_fine ]*d, params, dtype=float )

x_coarse = np.linspace(-1,1,4)
TW.set_view( 'coarse', [x_coarse]*d )

TW.set_active_view('full')
TW[1,:]
TW[:,2]
TW.set_active_view('coarse')
TW[2,:]

grid_coarse = np.array( list( itertools.product( *([x_coarse]*d) ) ) )
grid_fine = np.array( list( itertools.product( *([x_fine]*d) ) ) )

fill_idxs = np.array(TW.get_fill_idxs())
grid_fine_fill = np.vstack( [x_fine[fill_idxs[:,0]], x_fine[fill_idxs[:,1]]] ).T
grid_coarse_fill = np.array( [ grid_fine_fill[i,:] for i in xrange(TW.get_fill_level()) if (grid_fine_fill[i,0] in x_coarse and grid_fine_fill[i,1] in x_coarse) ] )

fig = plt.figure(figsize=(16,5))
ax = fig.add_subplot(131)
ax.plot( grid_fine[:,0], grid_fine[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='w', label='global' )
ax.plot( grid_fine_fill[:,0], grid_fine_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
ax = fig.add_subplot(132)
ax.plot( grid_fine[:,0], grid_fine[:,1], 'o', markeredgecolor='b', markeredgewidth=1.5, markerfacecolor='w', label='full')
ax.plot( grid_fine_fill[:,0], grid_fine_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
ax = fig.add_subplot(133)
ax.plot( grid_coarse[:,0], grid_coarse[:,1], 'o', markeredgecolor='r', markeredgewidth=1.5, markerfacecolor='w', label='coarse')
ax.plot( grid_coarse_fill[:,0], grid_coarse_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
plt.tight_layout()
plt.show(False)

#
# Refine
#
x_ffine = np.linspace(-1,1,13)
TW.refine([x_ffine]*d)

grid_coarse = np.array( list( itertools.product( *([x_coarse]*d) ) ) )
grid_fine = np.array( list( itertools.product( *([x_ffine]*d) ) ) )

fill_idxs = np.array(TW.get_fill_idxs())
grid_fine_fill = np.vstack( [x_ffine[fill_idxs[:,0]], x_ffine[fill_idxs[:,1]]] ).T
grid_coarse_fill = np.array( [ grid_fine_fill[i,:] for i in xrange(TW.get_fill_level()) if (grid_fine_fill[i,0] in x_coarse and grid_fine_fill[i,1] in x_coarse) ] )

fig = plt.figure(figsize=(16,5))
ax = fig.add_subplot(131)
ax.plot( grid_fine[:,0], grid_fine[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='w', label='global' )
ax.plot( grid_fine_fill[:,0], grid_fine_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
ax = fig.add_subplot(132)
ax.plot( grid_fine[:,0], grid_fine[:,1], 'o', markeredgecolor='b', markeredgewidth=1.5, markerfacecolor='w', label='full')
ax.plot( grid_fine_fill[:,0], grid_fine_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
ax = fig.add_subplot(133)
ax.plot( grid_coarse[:,0], grid_coarse[:,1], 'o', markeredgecolor='r', markeredgewidth=1.5, markerfacecolor='w', label='coarse')
ax.plot( grid_coarse_fill[:,0], grid_coarse_fill[:,1], 'o', markeredgecolor='k', markeredgewidth=1.5, markerfacecolor='k', label='eval' )
plt.xlim([-1.1,1.1])
plt.ylim([-1.1,1.1])
plt.legend()
plt.tight_layout()
plt.show(False)


#
# Quantics extension of the full view
#
TW.set_active_view('full')

plt.figure(figsize=(12,5))
plt.subplot(1,2,1)
plt.imshow(TW[:,:],interpolation='none')
plt.title('No extension')

TW.set_active_view('full')
TW.set_Q(2)

plt.subplot(1,2,2)
plt.imshow(TW[:,:],interpolation='none')
plt.title('Quantics extension')
plt.tight_layout()
plt.show(False)

#
# Reshape
#
TW.set_active_view('full')
TW.reshape((8,32))
plt.figure(figsize=(12,4))
plt.imshow(TW[:,:], interpolation='none')
plt.title('Reshaped')
plt.tight_layout()
plt.show(False)

import math
TW.reshape( [2] * int(round(math.log(TW.size,2))) )
TW.reset_shape()

#
# Storage
#
TW.set_store_location('tensorwrapper')
TW.store(force=True)

TW = TT.load('tensorwrapper')

#
# Timing storage
#
import time
TW.data = {}
TW[1,2]
TW.set_store_freq( 5 )
time.sleep(6.0)
TW[3,5]
