import numpy as np
import numpy.random as npr
import time

def test(f,N=100):
    ts = np.zeros(N)
    for i in range(N):
        a = npr.rand(900).reshape((30,30))
        b = npr.rand(900).reshape((30,30))
        st = time.clock()
        c = f(a,b)
        end = time.clock()
        ts[i] = end-st
    
    print "Mean time elapsed: %f" % (np.mean(ts))

test(np.kron,N=1000)
