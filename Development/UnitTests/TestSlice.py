import numpy as np

import TensorToolbox as TT


Xs = [ np.linspace(0.,1.,10) ] * 3
def f(X,params): return np.sin(np.sum(X))

TW = TT.TensorWrapper(f,Xs,None)

TTapp = TT.TTvec(TW,method='ttcross')

print TTapp[:,:,0]
