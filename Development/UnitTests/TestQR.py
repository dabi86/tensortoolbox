import numpy as np
import numpy.linalg as npla
import numpy.random as npr
from scipy import linalg as scla

size = (1000,100)
N = 100

for i in range(N):
    A = npr.rand(np.prod(size)).reshape(size)
    (Q,R) = scla.qr(A,mode='economic')
    (R,Q) = scla.rq(A,mode='economic')
