import sys
from os import getenv
from os import path
import numpy as np
import numpy.linalg as npla
from scipy import sparse
import scipy.sparse.linalg as spla
import scipy.linalg as scila
from scipy import integrate
from scipy import stats

import TensorToolbox as DT
import TensorToolbox.multilinalg as mla

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from SpectralToolbox import Spectral1D

import ExtPython

plt.close('all')

LEFT = 0
RIGHT = 1

DEBUG = True

################################################
# Stochastic Poisson with uncertain field
################################################

PROBLEM = 1
if PROBLEM == 2:
    KL_a = 1.
    KL_targ_var = 0.95
PLOTTING = True

############################
# Deterministic Settings
############################
# Meshing and function space
NE = 8
h = 1/float(NE-1)
xspan = [0.0, 1.0]
yspan = [0.0, 1.0]
x = np.linspace(xspan[0],xspan[1],NE)
(X,Y) = np.meshgrid(x,x)
xx = np.hstack([X.reshape((NE**2,1)),Y.reshape((NE**2,1))])

def f(x,y):
    # return -np.exp(-( (x-0.5)**2.+(y-0.5)**2. )/0.01)
    return -np.ones(x.shape)

BC_dir = []
BC_x_neu = []
BC_y_neu = []

def BC_dir_f(x,y):
    return 0. * x;

def BC_neu_x_f(x,y):
    return 0. * np.ones(x.shape);

def BC_neu_y_f(x,y):
    return 0. * np.ones(y.shape);

BC_dir_idx = []
BC_dir_idx += np.where(xx[:,0] == 0.)[0].tolist()
# BC_dir_idx += np.where(xx[:,1] == 0.)[0].tolist()
# BC_dir_idx += np.where(xx[:,0] == 1.)[0].tolist()
# BC_dir_idx += np.where(xx[:,1] == 1.)[0].tolist()
BC_dir.append((BC_dir_idx,BC_dir_f))

BC_neu_x_idx_left = []
# BC_neu_x_idx_left += np.where(xx[:,0] == 0.)[0].tolist()
# BC_x_neu.append((BC_neu_x_idx_left,BC_neu_x_f,LEFT));
BC_neu_x_idx_right = []
BC_neu_x_idx_right += np.where(xx[:,0] == 1.)[0].tolist()
BC_x_neu.append((BC_neu_x_idx_right,BC_neu_x_f,RIGHT));

BC_neu_y_idx_left = []
BC_neu_y_idx_left += np.where(xx[:,1] == 0.)[0].tolist()
BC_y_neu.append((BC_neu_y_idx_left,BC_neu_y_f,LEFT))
BC_neu_y_idx_right = []
BC_neu_y_idx_right += np.where(xx[:,1] == 1.)[0].tolist()
BC_y_neu.append((BC_neu_y_idx_right,BC_neu_y_f,RIGHT));

D1D = 1./h**2. * ( np.diag(np.ones((NE-1)),-1) + np.diag(np.ones((NE-1)),1) + np.diag(-2.*np.ones((NE)),0) )

Dxx1D = np.kron(np.eye(NE),D1D)
Dyy1D = np.kron(D1D,np.eye(NE))

F = f(X.flatten(),Y.flatten())

# Set Neumann bc
for BC_neu_idx,BC_neu_f,side in BC_x_neu:
    for row_idx in BC_neu_idx:
        Dxx1D[row_idx,:] = 0.
        if side == LEFT:
            cols = np.arange(row_idx,row_idx+2,1)
            Dxx1D[row_idx,cols] = 1./h * np.array([-1, 1.])
            # cols = np.arange(row_idx,row_idx+3,1)
            # Dxx1D[row_idx,cols] = 1./h * np.array([3./2., -2, 1./2.])
        elif side == RIGHT:
            cols = np.arange(row_idx-1,row_idx+1,1)
            Dxx1D[row_idx,cols] = 1./h * np.array([1., -1.])
            # cols = np.arange(row_idx-2,row_idx+1,1)
            # Dxx1D[row_idx,cols] = 1./h * np.array([1./2., -2, 3./2.])
        else: raise NameError("NBC error!")
        F[row_idx] = BC_neu_f(xx[row_idx,0],xx[row_idx,1]) + h/2. * f(xx[row_idx,0],xx[row_idx,1])
        # F[row_idx] = BC_neu_f(xx[row_idx,0],xx[row_idx,1])
        
for BC_neu_idx,BC_neu_f,side in BC_y_neu:
    for row_idx in BC_neu_idx:
        Dyy1D[row_idx,:] = 0.
        if side == LEFT:
            cols = np.arange(row_idx,row_idx+(NE*1)+1,NE)
            Dyy1D[row_idx,cols] = 1./h * np.array([-1, 1.])
            # cols = np.arange(row_idx,row_idx+(NE*2)+1,NE)
            # Dyy1D[row_idx,cols] = 1./h * np.array([3./2., -2, 1./2.])
        elif side == RIGHT:
            cols = np.arange(row_idx-(NE*1),row_idx+1,NE)
            Dyy1D[row_idx,cols] = 1./h * np.array([1., -1.])
            # cols = np.arange(row_idx-(NE*2),row_idx+1,NE)
            # Dyy1D[row_idx,cols] = 1./h * np.array([1./2., -2, 3./2.])
        else: raise NameError("NBC error!")
        F[row_idx] = BC_neu_f(xx[row_idx,0],xx[row_idx,1]) + h/2. * f(xx[row_idx,0],xx[row_idx,1])
        # F[row_idx] = BC_neu_f(xx[row_idx,0],xx[row_idx,1])

L = Dxx1D + Dyy1D

for BC_dir_idx,BC_dir_f, in BC_dir:
    L[BC_dir_idx,:] = 0.
    L[BC_dir_idx,BC_dir_idx] = 1.
    F[BC_dir_idx] = BC_dir_f(xx[BC_dir_idx,0],xx[BC_dir_idx,1])

plt.figure()
plt.spy(Dxx1D)
plt.show(block=False)
plt.figure()
plt.spy(Dyy1D)
plt.show(block=False)
plt.figure()
plt.spy(L)
plt.show(block=False)

if DEBUG:
    # Solve deteriministic system
    U = npla.solve(L,F)
    
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_surface(X,Y,U.reshape((NE,NE)),rstride=1, cstride=1, cmap=cm.coolwarm,
                    linewidth=0, antialiased=False)
    plt.show(block=False)

#################################################
# Stochastic definition for the problem
#################################################

class PoissonParameters:
    epsilon = 1.

params = PoissonParameters()

# Positive uniform distribution
# def betaFunc(xi):
#     return xi

dists = [ stats.uniform() ]

def paramUpdate(params,sample):
    params.epsilon = sample
    return params

def kappa(x,y,params):
    return params.epsilon * np.ones(x.shape)

gPC_polysType = [Spectral1D.JACOBI]
gPC_polysParams = [[0.,0.]]
gPC_distsPoly = [ stats.uniform(-1,2) ]
gPC_polys = [Spectral1D.Poly1D(gPC_polysType[i],gPC_polysParams[i]) for i in range(len(gPC_polysType))]
PCord = 10


# Log-normal distribution
# beta_bar = 1.
# C = np.sqrt(10)
# mu_beta = np.log(beta_bar)
# sigma_beta = np.log(C)/2.85

# def betaFunc(xi):
#     return np.exp(mu_beta + sigma_beta * xi)

# dists = [ stats.lognorm(sigma_beta, loc=0., scale=np.exp(mu_beta)) ]

# def paramUpdate(params,sample):
#     params.epsilon = sample
#     return params

# def kappa(x,y,params):
#     return params.epsilon * np.ones(x.shape)

# gPC_polysType = [Spectral1D.HERMITEP_PROB]
# gPC_polysParams = [None]
# gPC_distsPoly = [ stats.norm(0.,1.) ]
# gPC_polys = [Spectral1D.Poly1D(gPC_polysType[i],gPC_polysParams[i]) for i in range(len(gPC_polysType))]
# PCord = 10

#######################################
# Construct TT-tensor from full tensor

L_rsh = L.copy()
L_rsh = np.reshape(L_rsh,(NE,NE,NE,NE))
L_rsh = np.transpose(L_rsh,axes=(0,2,1,3))
L_rsh = np.reshape(L_rsh,(NE**2,NE**2))
TT_L = DT.TTmat(L_rsh,nrows=NE,ncols=NE,eps=1e-13,is_sparse=True)

F_rsh = F.copy()
F_rsh = np.reshape(F_rsh,(NE,NE))
TT_F = DT.TTvec(F_rsh,eps=1e-13)

if DEBUG:
    # Solve deteriministic system
    (TT_U,conv,info) = mla.gmres(TT_L,TT_F,maxit=1000,ext_info=True)
    
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_surface(X,Y,TT_U.to_tensor(),rstride=1, cstride=1, cmap=cm.coolwarm,
                    linewidth=0, antialiased=False)
    plt.show(block=False)


##################################
# Construct CP uncertainty tensor
CPtmp = []
d = len(gPC_polysType)
for i in range(d):
    CPi = np.empty((d,(PCord+1)**2))
    for j in range(d):
        if i==j:
            (xtmp,wtmp) = gPC_polys[j].Quadrature(PCord)
            xtmp = dists[j].ppf(gPC_distsPoly[0].cdf(xtmp))
            CPi[j,:] = np.diag(xtmp).flatten()
        else:
            CPi[j,:] = np.eye(PCord+1)

    CPtmp.append(CPi)

CP_coll = DT.Candecomp(CPtmp)
TT_coll = DT.TTmat(CP_coll,nrows=PCord+1,ncols=PCord+1)
TT_coll.rounding(eps=1e-13)

# Tensor product Unc. tensor x Lap
TT_L.kron(TT_coll,eps=1e-13)

####################################
# Construct CP right hand side
CPtmp = []
d = len(gPC_polysType)
for i in range(d):
    CPi = np.ones((1,PCord+1))
    CPtmp.append(CPi)

CP_ones = DT.Candecomp(CPtmp)
TT_ones = DT.TTvec(CP_ones)
TT_ones.rounding(1e-13)

# Tensor product rhs x ones
TT_F.kron(TT_ones,eps=1e-13)

# Solve deteriministic system
(TT_U,conv,info) = mla.gmres(TT_L,TT_F,maxit=10000,restart=400,ext_info=True)

fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
ax.plot_surface(X,Y,TT_U.to_tensor()[:,:,8],rstride=1, cstride=1, cmap=cm.coolwarm,
                linewidth=0, antialiased=False)
plt.show(block=False)

